@extends('site.layouts.master')


@section('content')
<section class="main-content-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!-- BSTORE-BREADCRUMB START -->
                <div class="bstore-breadcrumb">
                    <a href="index.html">HOMe</a>
                    <span><i class="fa fa-caret-right"></i></span>
                    <span>Đăng nhập | Đăng ký</span>
                </div>
                <!-- BSTORE-BREADCRUMB END -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="page-title">Đăng nhập | Đăng ký</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <!-- CREATE-NEW-ACCOUNT START -->
                <div class="create-new-account">
                    <form class="new-account-box primari-box" id="create-new-account" method="post" action="#">
                        <h3 class="box-subheading">Tạo một tài khoản</h3>
                        <div class="form-content">
                            <div class="submit-button">
                            <a href="{{route('register')}}" id="SubmitCreate" class="btn main-btn">
                                    <span>
                                        <i class="fa fa-user submit-icon"></i>
                                        Tạo một tài khoản
                                    </span>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- CREATE-NEW-ACCOUNT END -->
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <!-- REGISTERED-ACCOUNT START -->
                <div class="primari-box registered-account">
                    <form class="new-account-box" id="accountLogin" method="post" action="">
                        @csrf
                            @if (Session::has('status'))
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                {{Session::get('status')}}
                            </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        <span class="sr-only" style="font-size: 35px">Close</span>
                                    </button>
                                </div>
                            @endif
                        <h3 class="box-subheading">Đăng Nhập</h3>
                        <div class="form-content">
                            <div class="form-group primary-form-group">
                                <label for="loginemail">Email</label>
                                <input type="text" name="email" id="email" class="form-control input-feild">
                            </div>
                            <div class="form-group primary-form-group">
                                <label for="password">Password</label>
                                <input type="password" name="password" id="password" class="form-control input-feild">
                            </div>
                            <div class="submit-button">
                                <button type="submit" id="signinCreate" class="btn main-btn" name="action" value="0">
                                <span>
                                    <i class="fa fa-lock submit-icon"></i>
                                     Đăng nhập
                                </span>   
                                </button>

                            </div>
                            <hr>
                           <a href="{{url('password/reset')}}" class="btn btn-google btn-block">
                           <i class="	fa fa-unlock"></i> Quên mật khẩu
                            </a>
                            <a href="{{route('login.google',['google'])}}" class="btn btn-google btn-block">
                                <i class="fa fa-google fa-fw"></i> Login with Google
                            </a>
                            <a href="{{route('login.facebook',['facebook'])}}" class="btn btn-facebook btn-block">
                                <i class="fa fa-facebook-f fa-fw"></i> Login with Facebook
                            </a>
                        </div>
                    </form>
                </div>
                <!-- REGISTERED-ACCOUNT END -->
            </div>
        </div>
    </div>
</section>
@endsection
