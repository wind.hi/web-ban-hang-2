@extends('site.layouts.master')


@section('content')
<div class="container-fluid" id="container-wrapper">
    <!--Row-->

    <!-- Documentation Link -->

    <section class="main-content-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- BSTORE-BREADCRUMB START -->
                    <div class="bstore-breadcrumb">
                        <a href="index.html">home</a>
                        <span><i class="fa fa-caret-right	"></i></span>
                        <span>Đăng ký</span>
                    </div>
                    <!-- BSTORE-BREADCRUMB END -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2 class="page-title">Tạo tài khoản</h2>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- PERSONAL-INFOMATION START -->
                    <div class="personal-infomation">
                        <form class="primari-box personal-info-box" id="personalinfo" method="post" action="{{route('register')}}">
                            @csrf
                            @if (Session::has('status'))
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                {{Session::get('status')}}
                            </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        <span class="sr-only" style="font-size: 35px">Close</span>
                                    </button>
                                </div>
                            @endif
                            <h3 class="box-subheading">Đăng ký tài khoản</h3>
                            <div class="personal-info-content">
                                <div class="form-group primary-form-group p-info-group">
                                    <label for="firstname">Tên hiển thị  <sup>*</sup></label>
                                    <input type="text" value="" name="name" id="name" class="form-control input-feild">
                                </div>
                                <div class="form-group primary-form-group p-info-group">
                                    <label for="email">Địa chỉ Email<sup>*</sup></label>
                                    <input type="email" value="" name="email" id="email" class="form-control input-feild">
                                </div>
                                <div class="form-group primary-form-group p-info-group">
                                    <label for="password">Password <sup>*</sup></label>
                                    <input type="password" value="" name="password" id="password" class="form-control input-feild">
                                    <span class="min-pass">(Tối thiểu 5 kí tự)</span>
                                </div>
                                <div class="form-group primary-form-group p-info-group">
                                    <label for="password">Re_Password <sup>*</sup></label>
                                    <input type="password" value="" name="password_confirmation" id="password_confirmation" class="form-control input-feild">
                                </div>
                                <div class="form-group primary-form-group p-info-group">
                                    <label for="firstname">Địa chỉ  <sup>*</sup></label>
                                    <input type="text" value="" name="address" id="address" class="form-control input-feild">
                                </div>
                                <div class="form-group primary-form-group p-info-group">
                                    <label for="firstname">Số điện thoại  <sup>*</sup></label>
                                    <input type="text" value="" name="phone" id="phone" class="form-control input-feild">
                                </div>
                                <div class="submit-button p-info-submit-button">
                                    <button type="submit" id="SubmitCreate" class="btn main-btn">
                                        <span>
                                            Đăng ký
                                            <i class="fa fa-chevron-right"></i>
                                        </span>
                                    </button>
                                    <span class="required-field"><sup>*</sup>Phẩn bắt buộc phải nhập</span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- PERSONAL-INFOMATION END -->
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
