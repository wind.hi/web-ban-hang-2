@extends('site.layouts.master')
@section('content')

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<!-- BSTORE-BREADCRUMB START -->
		<div class="bstore-breadcrumb">
			<a href="index.html">HOME <span><i class="fa fa-caret-right"></i> </span> </a>
			<a href="my-account.html"> My Account <span><i class="fa fa-caret-right"></i></span></a>
			<span> My Wish Lists</span>
		</div>
		<!-- BSTORE-BREADCRUMB END -->
	</div>
</div>
<div class="row">
	<h2 class="page-title">My wishlists</h2>
	<!-- WISHLISTS-CHART START -->
	<div class="wishlists-chart table-responsive">
		<table class="table table-bordered">
            <thead>
                <tr>
                    <th class="wish-name">Name</th>
                    <th class="wish-name">Image</th>
                    <th class="wish-create">Created</th>
                    <th class="wish-link">Direct Link</th>
                    <th class="wish-delete">Action</th>
                </tr>
            </thead>
            <tbody id="dataWishlist">
				@forelse ($wish_lists as $wish_list)
                    <tr id="row-{{ $wish_list->id }}">
                        <td>
                            <a href="{{ route('product.detail', $wish_list->product_id) }}">{{ $wish_list->name}}</a>
                        </td>
                        <td>
                            <img width="100px" src="{{ asset('frontend/img/product/'.$wish_list->image) }}" alt="">
                        </td>
                        <td>
                            <span>{{ $wish_list->created_at}}</span>
                        </td>
                        <td>
                            <a href="{{ route('product.detail', $wish_list->product_id) }}">View</a>
                        </td>
                        <td>
                            <a class="delete-wish-list" href="java:noscript" onclick="removeWishlist({{ $wish_list->id }})"><i class="fa fa-close"></i> Remove</a>
                        </td>
                    </tr>

				@empty
					<tr>
						<td colspan="5">Wish list hiện đang trống</td>
					</tr>
				@endforelse
            </tbody>
		</table>
	</div>
	<!-- WISHLISTS-CHART END -->
</div>

@endsection

@section('scripts')
    <script>
        function removeWishlist(id) {
            const Toast = Swal.mixin({
                toast: true,
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })
            $.ajax({
                url: 'delete-wish-list/'+id,
                method: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id,
                },
                success: function(data) {
                    $('#row-'+id)
						.empty()
						.append(
							$("<td colspan='5'></td>").text("Đã xóa khỏi wishlists"),
							//$("<td></td>").html("<a class='delete-wish-list'><i class='fa fa-refresh'></i> Undo </a>"),
						)

                    Toast.fire({
                        icon: 'success',
                        title: data.success,
                        position: 'top-end'
                    })
                },
                error: function(error) {}
            }, );
        }
    </script>

@stop
