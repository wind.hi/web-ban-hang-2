@extends('site.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!-- BSTORE-BREADCRUMB START -->
                <div class="bstore-breadcrumb">
                    <a href="{{route('index')}}">HOMe</a>
                    <span><i class="fa fa-caret-right	"></i></span>
                    <span>Contact</span>
                </div>
                <!-- BSTORE-BREADCRUMB END -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="page-title contant-page-title">Customer service - Contact us</h2>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!-- CONTACT-US-FORM START -->
                <div class="contact-us-form">
                    <div class="contact-form-center">
                        <h3 class="contact-subheading">send a message</h3>
                        <!-- CONTACT-FORM START -->
                        <form class="contact-form" id="contactForm" method="post" action="">
                            @csrf
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                    <div class="form-group primary-form-group">
                                        <label>Title Heading</label>
                                        <input type="text" class="form-control input-feild" id="contactEmail" name="title" value="">
                                        @if($errors->has('title'))
                                            <span class="error-text">
                                                {{$errors->first('title')}}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group primary-form-group">
                                        <label>Email address</label>
                                        <input type="text" class="form-control input-feild" id="contactEmail" name="email" value="">
                                        @if($errors->has('email'))
                                            <span class="error-text">
                                                {{$errors->first('email')}}
                                            </span>
                                        @endif
                                    </div>
                                    <div style="margin-bottom: 50px" class="form-group primary-form-group">
                                        <label>Phone</label>
                                        <input type="text" class="form-control input-feild" id="contactEmail" name="phone" value="">
                                        @if($errors->has('phone'))
                                            <span class="error-text">
                                                {{$errors->first('phone')}}
                                            </span>
                                        @endif
                                    </div>
                                    <button type="submit" name="submit" id="sendMessage" class="send-message main-btn">Send <i class="fa fa-chevron-right"></i></button>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                                    <div class="type-of-text">
                                        <div class="form-group primary-form-group">
                                            <label>Message</label>
                                            <textarea style="height: 194px;" class="contact-text" name="message"></textarea>
                                            @if($errors->has('message'))
                                                <span class="error-text">
                                                {{$errors->first('message')}}
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- CONTACT-FORM END -->
                    </div>
                </div>
                <!-- CONTACT-US-FORM END -->
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script type="text/javascript">
        const Toast = Swal.mixin({
            toast: true,
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })
        @if(Session::has('message'))
        Toast.fire({
            icon: 'success',
            title: "{{ Session::get('message') }}",
            position: 'top'
        })
        {{ Session::put('message', null) }}
        @endif
    </script>
@stop
