@extends('site.layouts.master')
@section('title', 'Danh mục sản phẩm')
@section('content')
    <section class="main-content-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- BSTORE-BREADCRUMB START -->
                    <div class="bstore-breadcrumb">
                        <a href="{{route('index')}}">HOMe</a>
                        <span><i class="fa fa-caret-right"></i></span>
                        <span>Contact</span>
                    </div>
                    <!-- BSTORE-BREADCRUMB END -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2 class="page-title contant-page-title">
                        Dịch vụ khách hàng - Liên hệ với chúng tôi</h2>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- CONTACT-US-FORM START -->
                    <div class="contact-us-form">
                        <div class="contact-form-center">
                            <h3 class="contact-subheading">Gửi tin nhắn</h3>
                            <!-- CONTACT-FORM START -->
                            <form class="contact-form" id="contactForm" method="post" action="">
                                @csrf
                                @if (Session::has('status'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        {{Session::get('status')}}
                                    </div>
                                @endif
                                @if ($errors->any())
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            <span class="sr-only" style="font-size: 35px">Close</span>
                                        </button>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                        <div class="form-group primary-form-group">
                                            <label>Tiêu đề</label>
                                            <input type="text" class="form-control input-feild" id="contactEmail" name="title">
                                            @if($errors->has('title'))
                                                <span class="error-text">
                                                    {{$errors->first('title')}}
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group primary-form-group">
                                            <label>Địa Chỉ Email</label>
                                            <input type="text" class="form-control input-feild" id="contactEmail" name="email">
                                            @if($errors->has('email'))
                                                <span class="error-text">
                                                    {{$errors->first('email')}}
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group primary-form-group">
                                            <label>Số Điện thoại</label>
                                            <input type="number" class="form-control input-feild" id="contactEmail" name="phone">
                                            @if($errors->has('phone'))
                                                <span class="error-text">
                                                    {{$errors->first('phone')}}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                                        <div class="type-of-text">
                                            <div class="form-group primary-form-group">
                                                <label>Message</label>
                                                <textarea class="contact-text" name="message"></textarea>
                                            </div>
                                            @if($errors->has('message'))
                                                <span class="error-text">
                                                    {{$errors->first('message')}}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <button type="submit" name="submit" id="sendMessage" class="send-message main-btn"> Gửi  <i class="fa fa-chevron-right"></i></button>
                                </div>
                            </form>
                            <!-- CONTACT-FORM END -->
                        </div>
                    </div>
                    <!-- CONTACT-US-FORM END -->
                </div>
            </div>
        </div>
    </section>
@endsection
