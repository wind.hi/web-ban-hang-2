<div class="header-top">
	<div class="container">
		<div class="row">
			<!-- HEADER-LEFT-MENU START -->
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="header-left-menu">
					<div class="welcome-info">
						Welcome <span>Web-ban-hang</span>
					</div>
					<div class="currenty-converter">
						<form method="post" action="#" id="currency-set">
							<div class="current-currency">
								<span class="cur-label">Currency : </span><strong>Vnd (vnd)</strong>
							</div>
{{--							<ul class="currency-list currency-toogle">--}}
{{--								<li>--}}
{{--									<a title="Dollar (USD)" href="#">Dollar (USD)</a>--}}
{{--								</li>--}}
{{--								<li>--}}
{{--									<a title="Euro (EUR)" href="#">Euro (EUR)</a>--}}
{{--								</li>--}}
{{--								<li>--}}
{{--									<a title="Vnd (Vnd)" href="#">Vnd (vnd)</a>--}}
{{--								</li>--}}
{{--							</ul>--}}
						</form>
					</div>
					<div class="selected-language">
						<div class="current-lang">
							<span class="current-lang-label">Language : </span><strong>VietNamese</strong>
						</div>
{{--						<ul class="languages-choose language-toogle">--}}
{{--							<li>--}}
{{--								<a href="#" title="English">--}}
{{--									<span>VietNamese</span>--}}
{{--								</a>--}}
{{--							</li>--}}
{{--							<li>--}}
{{--								<a href="#" title="VietNam">--}}
{{--									<span>VietNamese</span>--}}
{{--								</a>--}}
{{--							</li>--}}
{{--						</ul>--}}
					</div>
				</div>
			</div>
			<!-- HEADER-LEFT-MENU END -->
			<!-- HEADER-RIGHT-MENU START -->
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="header-right-menu">
					<nav>
						<ul class="list-inline">
							<li><a href="{{route('my.cart')}}">My Cart</a></li>
							@if(Auth::check())
							<li><a style="color: #e74c3c;">{{Auth::user()->name}}</a></li>
                                <li><a href="{{ route('wishlist.list') }}">Wishlist</a></li>
							<li><a href="{{route('My.Account')}}">My Account</a></li>
							<li><a href="{{route('logoutSite')}}">Đăng Xuất</a></li>
							@else
							<li><a href="{{route('login')}}">Đăng nhập</a></li>
							<li><a href="{{route('register')}}">Đăng ký</a></li>
							@endif
						</ul>
					</nav>
				</div>
			</div>
			<!-- HEADER-RIGHT-MENU END -->
		</div>
	</div>
</div>
