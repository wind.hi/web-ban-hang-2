<?php
use Illuminate\Database\Eloquent\Model;
use App\Entity\Category;
$categories = Category::where('parent', '=', 0)->get();
?>
<section class="header-middle">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<!-- LOGO START -->
				<div class="logo">
{{--                    <a href="{{ route('index') }}"><img src="{{asset('frontend/img/info/'.$info->logo)}}" alt="bstore logo"/></a>--}}
				</div>
				<!-- LOGO END -->
				<!-- HEADER-RIGHT-CALLUS START -->
				<div class="header-right-callus">
					<h3>call us free</h3>
					<span>{{ isset($info->phone) ? $info->phone : '' }}</span>
				</div>
				<!-- HEADER-RIGHT-CALLUS END -->
				<!-- CATEGORYS-PRODUCT-SEARCH START -->
				<div class="categorys-product-search">
					<form action="{{ route('search') }}" method="get" class="search-form-cat">
						<div class="search-product form-group">
							<select name="catsearch" class="cat-search">
								<option value="">Categories</option>
								@foreach($categories as $category)
									<option value="{{ $category->id }}">{{ $category->title }}</option>
								@endforeach
							</select>
							<input type="text" class="form-control search-form" name="keysearch" placeholder="Enter your search key ... ">
							<button class="search-button" value="Search" name="s" type="submit">
								<i class="fa fa-search"></i>
							</button>
						</div>
					</form>
				</div>
				<!-- CATEGORYS-PRODUCT-SEARCH END -->
			</div>
		</div>
	</div>
</section>
