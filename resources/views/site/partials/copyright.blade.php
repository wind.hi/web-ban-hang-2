<footer class="copyright-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="copy-right">
					<address>Bản Quyền © 2020 <a href="#">Chưa nghĩ ra tên</a> Tất cả các quyền</address>
				</div>
				<div class="scroll-to-top">
					<a href="#" class="bstore-scrollertop"><i class="fa fa-angle-double-up"></i></a>
				</div>
			</div>
		</div>
	</div>
</footer>
