<!-- FONTS -->
<link href='http://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Bitter:400,700,400italic&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>

<!-- animate CSS -->
<link rel="stylesheet" href="{{asset('frontend')}}/css/animate.css">

<!-- FANCYBOX CSS -->
<link rel="stylesheet" href="{{asset('frontend')}}/css/jquery.fancybox.css">

<!-- BXSLIDER CSS -->
<link rel="stylesheet" href="{{asset('frontend')}}/css/jquery.bxslider.css">

<!-- MEANMENU CSS -->
<link rel="stylesheet" href="{{asset('frontend')}}/css/meanmenu.min.css">

<!-- JQUERY-UI-SLIDER CSS -->
<link rel="stylesheet" href="{{asset('frontend')}}/css/jquery-ui-slider.css">

<!-- NIVO SLIDER CSS -->
<link rel="stylesheet" href="{{asset('frontend')}}/css/nivo-slider.css">

<!-- OWL CAROUSEL CSS -->
<link rel="stylesheet" href="{{asset('frontend')}}/css/owl.carousel.css">

<!-- OWL CAROUSEL THEME CSS -->
<link rel="stylesheet" href="{{asset('frontend')}}/css/owl.theme.css">

<!-- BOOTSTRAP CSS -->
<link rel="stylesheet" href="{{asset('frontend')}}/css/bootstrap.min.css">

<!-- FONT AWESOME CSS -->
<link rel="stylesheet" href="{{asset('frontend')}}/css/font-awesome.min.css">

<!-- NORMALIZE CSS -->
<link rel="stylesheet" href="{{asset('frontend')}}/css/normalize.css">

<!-- MAIN CSS -->
<link rel="stylesheet" href="{{asset('frontend')}}/css/main.css">

<!-- STYLE CSS -->
<link rel="stylesheet" href="{{asset('frontend')}}/style.css">

<!-- RESPONSIVE CSS -->
<link rel="stylesheet" href="{{asset('frontend')}}/css/responsive.css">

<!-- IE CSS -->
<link rel="stylesheet" href="{{asset('frontend')}}/css/ie.css">
<style>
    .error-text{
        color: red;
    }
</style>
<!-- MODERNIZR JS -->
<script src="{{asset('frontend')}}/js/vendor/modernizr-2.6.2.min.js"></script>
