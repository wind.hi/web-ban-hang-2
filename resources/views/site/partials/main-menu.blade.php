<?php
use Illuminate\Database\Eloquent\Model;
use App\Entity\Category;

$categories = Category::where('status', '=', 1)->get();
?>
<header class="main-menu-area">
	<div class="container">
		<div class="row">
			<!-- SHOPPING-CART START -->
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-right shopingcartarea">
				<div class="shopping-cart-out pull-right">
                    @if(\Gloudemans\Shoppingcart\Facades\Cart::count() > 0)
					<div class="shopping-cart">
						<a class="shop-link" href="cart.html" title="View my shopping cart">
							<i class="fa fa-shopping-cart cart-icon"></i>
							<b>My Cart</b>
							<span class="ajax-cart-quantity">{{count(\Gloudemans\Shoppingcart\Facades\Cart::content())}}</span>
						</a>
                        <?php
                        $total = 0;
                        ?>
						<div class="shipping-cart-overly">
                            @foreach(\Gloudemans\Shoppingcart\Facades\Cart::content() as $item)
							<div class="shipping-item">
								<span class="cross-icon"><i class="fa fa-times-circle"></i></span>
								<div class="shipping-item-image">
									<a href="#"><img src="{{asset('frontend/img/product/'.$item->options->image)}}" alt="shopping image" width="80px" height="80px"/></a>
								</div>
								<div class="shipping-item-text">
									<span>{{$item->qty}} <span class="pro-quan-x">x</span> <a href="#" class="pro-cat">{{$item->name}}</a></span>
                                    @if(count((array)$item->options->attribute) > 0)

                                        <?php $string = ''; ?>
                                        @foreach($item->options->attribute as $value)
                                            @php
                                                $string .= $value.', ';
                                            @endphp
                                        @endforeach
                                            <span class="pro-quality">
                                                <a href="#">{{rtrim($string, ', ')}}</a>
                                            </span>
                                    @endif
									<p>{{number_format($item->price * $item->qty,'0','.',',')}} d</p>
								</div>
							</div>
                                <?php
                                $total += $item->price * $item->qty;
                                ?>
                            @endforeach
							<div class="shipping-total-bill">
								<div class="cart-prices">
									<span class="shipping-cost">0</span>
									<span>Shipping</span>
								</div>
								<div class="total-shipping-prices">
									<span class="shipping-total">{{number_format($total,'0','.',',')}} d</span>
									<span>Total</span>
								</div>
							</div>
							<div class="shipping-checkout-btn">
								<a href="checkout.html">Check out <i class="fa fa-chevron-right"></i></a>
							</div>
						</div>
					</div>
                        @else
                        <div class="shopping-cart">
                            <a class="shop-link" href="cart.html" title="View my shopping cart">
                                <i class="fa fa-shopping-cart cart-icon"></i>
                                <b>My Cart</b>
                                <span class="ajax-cart-quantity">0</span>
                            </a>
                            <div class="shipping-cart-overly">

                                <div class="shipping-total-bill">
                                    <div class="cart-prices">
                                        <span class="shipping-cost">0</span>
                                        <span>Shipping</span>
                                    </div>
                                    <div class="total-shipping-prices">
                                        <span class="shipping-total">0 d</span>
                                        <span>Total</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                        @endif
				</div>
			</div>
			<!-- SHOPPING-CART END -->
			<!-- MAINMENU START -->
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 no-padding-right menuarea">
				<div class="mainmenu">
					<nav>
						<ul class="list-inline mega-menu">
							<li class="active"><a href="{{ route('index') }}">Home</a>

							</li>
							@foreach($categories as $category)
							@if($category->parent == 0)
							<li>
								<a>{{ $category->title}}</a>
								<!-- DRODOWN-MEGA-MENU START -->
								<div class="drodown-mega-menu">
									<div class="left-mega col-xs-12">
										<div class="mega-menu-list">
											<ul>
												@foreach($categories as $category_child)
												@if($category->id == $category_child->parent)
												<li><a href="{{ Route('product.type',$category_child->id)}}">{{ $category_child->title }}</a></li>
												@endif
												@endforeach
											</ul>
										</div>
									</div>
								</div>
							</li>
							@endif
							@endforeach


							<li><a href="7">Delivery</a></li>
							<li><a href="about-us.html">About us</a></li>
						</ul>
					</nav>
				</div>
			</div>
			<!-- MAINMENU END -->
		</div>
{{--		<div class="row">--}}
{{--			<!-- MOBILE MENU START -->--}}
{{--			<div class="col-sm-12 mobile-menu-area">--}}
{{--				<div class="mobile-menu hidden-md hidden-lg" id="mob-menu">--}}
{{--					<span class="mobile-menu-title">MENU</span>--}}
{{--					<nav>--}}
{{--						<ul>--}}
{{--							<li><a href="{{ route('index') }}">Home</a></li>--}}
{{--							@foreach($categories as $category)--}}
{{--							@if($category->parent == 0)--}}
{{--							<li><a href="#{{ $category->id }}">{{ $category->title }}</a>--}}
{{--								<ul>--}}
{{--									@foreach($categories as $category_child)--}}
{{--									@if($category_child->parent == $category->id)--}}
{{--									<li><a href="#{{ $category_child->id }}">{{ $category_child->title }}</a></li>--}}
{{--									@endif--}}
{{--									@endforeach--}}
{{--								</ul>--}}
{{--							</li>--}}
{{--							@endif--}}
{{--							@endforeach--}}
{{--							<li><a href="#">Delivery</a></li>--}}
{{--							<li><a href="about-us.html">About us</a></li>--}}
{{--						</ul>--}}
{{--					</nav>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--			<!-- MOBILE MENU END -->--}}
{{--		</div>--}}
	</div>
</header>
