@extends('site.layouts.master')
@section('content')
    <section class="main-content-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- BSTORE-BREADCRUMB START -->
                    <div class="bstore-breadcrumb">
                        <a href="index.html">HOMe</a>
                        <span><i class="fa fa-caret-right	"></i></span>
                        <span>Authentication</span>
                    </div>
                    <!-- BSTORE-BREADCRUMB END -->
                </div>
            </div>
            <div class="row">
                @if(\App\Entity\Vendor::where('user_id',\Illuminate\Support\Facades\Auth::id())->exists())
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2 class="page-title">Bạn đã đăng ký. Vui lòng chờ đợi là hạnh phúc - akira phan</h2>
                    </div>
                    @else
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2 class="page-title">Đăng ký làm nhà cung cấp</h2>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <!-- PERSONAL-INFOMATION START -->
                        <div class="personal-infomation">
                            <form class="primari-box personal-info-box" id="personalinfo" method="" >
                                @csrf
                                <h3 class="box-subheading">Thông tin cửa hàng của bạn</h3>
                                <div class="personal-info-content">

                                    <div class="form-group primary-form-group p-info-group">
                                        <label for="firstname">Tên cửa hàng<sup>*</sup></label>
                                        <input type="text" value="" name="name_store" id="firstname" class="form-control input-feild">
                                    </div>

                                    <div class="form-group primary-form-group p-info-group">
                                        <label for="lastname">Tên đại điện<sup>*</sup></label>
                                        <input type="text" value="" name="name_representative" id="lastname" class="form-control input-feild">
                                    </div>


                                    <div class="form-group primary-form-group p-info-group">
                                        <label for="email">Email Liện hệ<sup>*</sup></label>
                                        <input type="email" value="" name="email" id="email" class="form-control input-feild">
                                    </div>

                                    <div class="form-group primary-form-group p-info-group">
                                        <label for="email">Số điện thoại contact<sup>*</sup></label>
                                        <input type="" value="" name="phone" id="" class="form-control input-feild">
                                    </div>

                                    <div class="form-group primary-form-group p-info-group">
                                        <label for="email">Địa chỉ cửa hàng<sup>*</sup></label>
                                        <input type="" value="" name="address" id="" class="form-control input-feild">
                                    </div>




                                    <div class="submit-button p-info-submit-button">
                                        <button type="button" class="btn main-btn" onclick="return RegisterVendor(this);">
                                        <span>Register
                                        </span></button>
                                        {{--                                    <a href="" id="SubmitCreate" class="btn main-btn">--}}
                                        {{--											<span>--}}
                                        {{--												Register--}}
                                        {{--												<i class="fa fa-chevron-right"></i>--}}
                                        {{--											</span>--}}
                                        {{--                                    </a>--}}
                                        <span class="required-field"><sup>*</sup>Required field</span>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- PERSONAL-INFOMATION END -->
                    </div>
                    @endif

            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Đăng ký thành công</h4>
                </div>
                <div class="modal-body">
                    <h3 style="font-family:none;">Cảm ơn bạn đã đăng ký tham gia chuỗi bán hàng đa cấp. Chúng tôi sẽ xác nhận lại. Vui lòng kiểm tra email ! </h3>
                </div>
                <div class="modal-footer">
                    <button type="button" id="confirmRegister" class="btn btn-primary">Đồng ý</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        function RegisterVendor(e) {
            var nameRepresentative = $("input[name='name_representative']").val();
            var nameStore = $("input[name='name_store']").val();
            var phone = $("input[name='phone']").val();
            var email = $("input[name='email']").val();
            var address = $("input[name='address']").val();

         
  
            alert("Vui lòng chờ trong giây lát");
       



            $.ajax({
                url: "{{ route('add.vendor') }}",
                method: 'POST',
                data:{
                    _token:"{{csrf_token()}}",
                    name_representative:nameRepresentative,
                    name_store:nameStore,
                    phone:phone,
                    email:email,
                    address:address,
                },
                success: function(data) {
                    $("#basicModal").modal('show');
                    $(e).attr('onclick','');

                },
                error: function() {
                    alert("Please nhập đủ trường");
                }

            });
        }
    $("#confirmRegister").click(function () {
        window.location.href = "{{route('My.Account')}}";
    })
    </script>
    @endsection
