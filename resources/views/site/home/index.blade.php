@extends('site.layouts.master')
@section('content')
@php
    use App\Entity\Rate;
    use App\Entity\Comment;
@endphp
<div class="row">
	<!-- MAIN-SLIDER-AREA START -->
	<div class="main-slider-area">
		<!-- SLIDER-AREA START -->
		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
			<div class="slider-area">
				<div id="wrapper">
					<div class="slider-wrapper">
						<div id="mainSlider" class="nivoSlider caakk">
							@foreach($sliders as $slider)
							<img src="{{asset('frontend')}}/img/slider/{{ $slider['image'] }}" alt="{{ $slider['name'] }}" title="{{ $slider['id'] }}" />
							@endforeach
						</div>
						@foreach($sliders as $slider)
						<div id="{{ $slider['id'] }}" class="nivo-html-caption slider-caption">
							<div class="slider-progress"></div>
							<div class="slider-cap-text slider-text1">
								<div class="d-table-cell">
									<h2 class="animated bounceInDown">{{ $slider['title'] }}</h2>
									<p class="animated bounceInUp">{{ $slider['description'] }}</p>
									<a class="wow zoomInDown" data-wow-duration="1s" data-wow-delay="1s" href="{{ route('product.detail',$slider->id) }}">Read More <i class="fa fa-caret-right"></i></a>
								</div>
							</div>
						</div>
						@endforeach

						<div id="htmlcaption2" class="nivo-html-caption slider-caption">
							<div class="slider-progress"></div>
							<div class="slider-cap-text slider-text2">
								<div class="d-table-cell">
									<h2 class="animated bounceInDown">BEST THEMES</h2>
									<p class="animated bounceInUp">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod ut laoreet dolore magna aliquam erat volutpat!!!.</p>
									<a class="wow zoomInDown" data-wow-duration="1s" data-wow-delay="1s" href="#">Read More <i class="fa fa-caret-right"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- SLIDER-AREA END -->
		<!-- SLIDER-RIGHT START -->
		@if (sizeof($ads))
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
				<div class="slider-right zoom-img m-top">
					<a href="{{$ads[0]->link}}"><img class="img-responsive" src="{{asset('frontend/img/product')}}/{{$ads[0]->image}}" alt="sidebar left" /></a>
				</div>
			</div>
		@endif
		<!-- SLIDER-RIGHT END -->
	</div>
	<!-- MAIN-SLIDER-AREA END -->
</div>
<div class="row tow-column-product">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<!-- NEW-PRODUCT-AREA START -->
		<div class="new-product-area">
			<div class="left-title-area">
				<h2 class="left-title">New Products</h2>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="row">
						<!-- NEW-PRO-CAROUSEL START -->
						<div class="new-pro-carousel">
							<!-- NEW-PRODUCT-SINGLE-ITEM START -->
                            @foreach($newProducts as $newProduct)
							<div class="item">
								<div class="new-product">
									<div class="single-product-item">
										<div class="product-image">
											@foreach($productImages as $productImage )
											@if($newProduct->id == $productImage->product_id)
											<a href="{{ route('product.detail', $newProduct->id) }}"><img height="263px"class="img-product" src="frontend/img/product/{{ $productImage['image'] }}" alt="product-image" /></a>
											@endif
											@endforeach
                                                @if($newProduct->discount != 0)
                                                    <a href="#" class="new-mark-box">Sale!</a>
                                                @else
                                                    <a href="#" class="new-mark-box">New</a>
                                                @endif

											<div class="overlay-content">
												<ul>
													<li><a href="{{ route('product.detail', $newProduct->id) }}" title="Quick view"><i class="fa fa-search"></i></a></li>
													<li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
													<li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
													<li><a onclick="wishList({{ $newProduct->id }}, this)" title="Add to Wishlist"><i class="fa fa-heart-o"></i></a></li>
												</ul>
											</div>
										</div>
										<div class="product-info">
											<div class="customar-comments-box">
                                                <div class="rating-box">
                                                    @foreach($rating as $rate)
                                                        @if($rate->product_id == $newProduct->id)
                                                            @php
                                                                // lấy trung bình sao của 1 sản phẩm theo 1 mảng sản phẩm
                                                                 $AvgRate = round(Rate::getAvgRate($newProduct->id));
                                                            @endphp
                                                            @if($AvgRate)
                                                                @for($i = 0; $i <= $AvgRate; $i++)
                                                                    <i class="fa fa-star" style="color:#FFBA00 "></i>
                                                                @endfor
                                                                @for($i = 0; $i < (4 - $AvgRate); $i++)
                                                                    <i class="fa fa-star"></i>
                                                                @endfor
                                                            @else
                                                                @for($i = 0; $i <= 4; $i++)
                                                                    <i class="fa fa-star"></i>
                                                                @endfor
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </div>
                                                <div class="review-box">
                                                    @foreach($reviews as $rev)
                                                        @if($rev->product_id == $newProduct->id)
                                                            @php
                                                                // đếm comment của 1 sản phẩm trong 1 mảng sản phẩm
                                                                    $CountComment  = count(Comment::getCountComment($newProduct->id));
                                                            @endphp
                                                        @endif
                                                    @endforeach
                                                    <span>{{ $CountComment ?? '' }} Review(s)</span>
                                                </div>
											</div>
											<a href="{{ route('product.detail', $newProduct->id) }}">{{ $newProduct['name'] }}</a>
											<div class="price-box">
												@if($newProduct->discount == 0)
												<span class="price">{{ $newProduct['price'] }}đ</span>
												@else
												<span class="old-price"> {{ $newProduct['price'] }}đ</span>
												<span class="price">{{ $newProduct['discount'] }}đ</span>
												@endif
											</div>
										</div>
									</div>
								</div>
							</div>
							@endforeach
							<!-- NEW-PRODUCT-SINGLE-ITEM END -->
						</div>
						<!-- NEW-PRO-CAROUSEL END -->
					</div>
				</div>
			</div>
		</div>
		<!-- NEW-PRODUCT-AREA END -->
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<!-- SALE-PRODUCTS START -->
		<div class="Sale-Products">
			<div class="left-title-area">
				<h2 class="left-title">Sale Products</h2>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="row">
						<!-- SALE-CAROUSEL START -->
						<div class="sale-carousel">
							<!-- SALE-PRODUCTS-SINGLE-ITEM START -->
							@foreach($saleProducts as $saleProduct)
							<div class="item">
								<div class="new-product">
									<div class="single-product-item">
										<div class="product-image">
											@foreach($productImages as $productImage)
											@if($saleProduct->id == $productImage->product_id)
											<a href="{{ route('product.detail', $saleProduct->id) }}"><img height="263px" src="{{ asset('') }}frontend/img/product/{{ $productImage['image'] }}" alt="product-image" /></a>
											@endif
											@endforeach
											<a href="#" class="new-mark-box">sale</a>
											<div class="overlay-content">
												<ul>
                                                    <li><a href="{{ route('product.detail', $saleProduct->id) }}" title="Quick view"><i class="fa fa-search"></i></a></li>
                                                    <li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
                                                    <li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
                                                    <li><a onclick="wishList({{ $saleProduct->id }}, this)" title="Add to Wishlist"><i class="fa fa-heart-o"></i></a></li>
												</ul>
											</div>
										</div>
										<div class="product-info">
											<div class="customar-comments-box">
                                                <div class="rating-box">
                                                    @foreach($rating as $rate)
                                                        @if($rate->product_id == $saleProduct->id)
                                                            @php
                                                                // lấy trung bình sao của 1 sản phẩm theo 1 mảng sản phẩm
                                                                 $AvgRate = round(Rate::getAvgRate($saleProduct->id));
                                                            @endphp
                                                            @if($AvgRate)
                                                                @for($i = 0; $i <= $AvgRate; $i++)
                                                                    <i class="fa fa-star" style="color:#FFBA00 "></i>
                                                                @endfor
                                                                @for($i = 0; $i < (4 - $AvgRate); $i++)
                                                                    <i class="fa fa-star"></i>
                                                                @endfor
                                                            @else
                                                                @for($i = 0; $i <= 4; $i++)
                                                                    <i class="fa fa-star"></i>
                                                                @endfor
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </div>
                                                <div class="review-box">
                                                    @foreach($reviews as $rev)
                                                        @if($rev->product_id == $saleProduct->id)
                                                            @php
                                                                // đếm comment của 1 sản phẩm trong 1 mảng sản phẩm
                                                                    $CountComment  = count(Comment::getCountComment($saleProduct->id));
                                                            @endphp
                                                        @endif
                                                    @endforeach
                                                    <span>{{ $CountComment ?? '' }} Review(s)</span>
                                                </div>
											</div>
											<a href="{{ route('product.detail', $saleProduct->id) }}">{{ $saleProduct['name'] }}</a>
											<div class="price-box">
                                                @if($saleProduct->discount != 0)
												<span class="old-price">{{ $saleProduct['price'] }}đ</span>
												<span class="price">{{ $saleProduct['discount'] }}đ</span>
                                                @else
                                                    <span class="old-price">{{ $saleProduct['price'] }}đ</span>
                                                @endif
											</div>
										</div>
									</div>
								</div>
							</div>
							@endforeach
							<!-- SALE-PRODUCTS-SINGLE-ITEM END -->
						</div>
						<!-- SALE-CAROUSEL END -->
					</div>
				</div>
			</div>
		</div>
		<!-- SALE-PRODUCTS END -->
	</div>
</div>
<div class="row">
    <!-- FEATURED-PRODUCTS-AREA START -->
    <div class="featured-products-area">
        <div class="center-title-area">
            <h2 class="center-title">Sản phẩm yêu thích</h2>
        </div>
        <div class="col-xs-12">
            <div class="row">
                <!-- FEARTURED-CAROUSEL START -->
                <div class="feartured-carousel">
                    @foreach($productLove as $love)
                    <div class="item">
                        <div class="single-product-item">
                            <div class="product-image">
                                @foreach($productImages as $productImage )
                                    @if($love->id == $productImage->product_id)
                                        <a href="{{ route('product.detail', $love->id) }}"><img height="204px"class="img-product" src="frontend/img/product/{{ $productImage['image'] }}" alt="product-image" /></a>
                                    @endif
                                @endforeach
                                    @if($love->discount != 0)
                                        <a href="#" class="new-mark-box">Sale!</a>
                                    @else
                                        <a href="#" class="new-mark-box">New</a>
                                    @endif
                                <div class="overlay-content">
                                    <ul>
                                        <li><a href="#" title="Quick view"><i class="fa fa-search"></i></a></li>
                                        <li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
                                        <li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
                                        <li><a onclick="wishList({{ $love->id }}, this)" title="Add to Wishlist"><i class="fa fa-heart-o"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="product-info">
                                <div class="customar-comments-box">
                                    <div class="rating-box">
                                        @foreach($rating as $rate)
                                            @if($rate->product_id == $saleProduct->id)
                                                @php
                                                    // lấy trung bình sao của 1 sản phẩm theo 1 mảng sản phẩm
                                                     $AvgRate = round(Rate::getAvgRate($saleProduct->id));
                                                @endphp
                                                @if($AvgRate)
                                                    @for($i = 0; $i <= $AvgRate; $i++)
                                                        <i class="fa fa-star" style="color:#FFBA00 "></i>
                                                    @endfor
                                                    @for($i = 0; $i < (4 - $AvgRate); $i++)
                                                        <i class="fa fa-star"></i>
                                                    @endfor
                                                @else
                                                    @for($i = 0; $i <= 4; $i++)
                                                        <i class="fa fa-star"></i>
                                                    @endfor
                                                @endif
                                            @endif
                                        @endforeach
                                    </div>
                                    <div class="review-box">
                                        @foreach($reviews as $rev)
                                            @if($rev->product_id == $saleProduct->id)
                                                @php
                                                    // đếm comment của 1 sản phẩm trong 1 mảng sản phẩm
                                                        $CountComment  = count(Comment::getCountComment($saleProduct->id));
                                                @endphp
                                            @endif
                                        @endforeach
                                        <span>{{ $CountComment ?? '' }} Review(s)</span>
                                    </div>
                                </div>
                                <a href="{{ route('product.detail', $love->id) }}">{{$love->name}}</a>
                                <div class="price-box">
                                    @if($love->discount != 0)
                                        <span class="old-price">{{ $love['price'] }}đ</span>
                                        <span class="price">{{ $love['discount'] }}đ</span>
                                    @else
                                        <span class="old-price">{{ $love['price'] }}đ</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- FEARTURED-CAROUSEL END -->
            </div>
        </div>
    </div>
    <!-- FEATURED-PRODUCTS-AREA END -->
</div>

<div class="row">
	<!-- BESTSELLER-PRODUCTS-AREA START -->
	<div class="bestseller-products-area">
		<div class="center-title-area">
			<h2 class="center-title">Top Lượt Xem </h2>
		</div>
		<div class="col-xs-12">
			<div class="row">
				<!-- BESTSELLER-CAROUSEL START -->
				<div class="bestseller-carousel">
					<!-- BESTSELLER-SINGLE-ITEM START -->
                    @foreach($topView as $view)
					<div class="item">
						<div class="single-product-item">
							<div class="product-image">
                                @foreach($productImages as $image)
                                @if($view->id == $image->product_id)
								<a href="{{ route('product.detail', $view->id) }}"><img src="{{asset('frontend')}}/img/product/{{ $image->image }}" alt="product-image" /></a>
								@endif
                                @endforeach
                                    @if($view->discount != 0)
                                        <a href="#" class="new-mark-box">Sale!</a>
                                    @else
                                        <a href="#" class="new-mark-box">New</a>
                                    @endif
								<div class="overlay-content">
									<ul>
                                        <li><a href="{{ route('product.detail', $view->id) }}" title="Quick view"><i class="fa fa-search"></i></a></li>
                                        <li><a href="#" title="Quick view"><i class="fa fa-shopping-cart"></i></a></li>
                                        <li><a href="#" title="Quick view"><i class="fa fa-retweet"></i></a></li>
                                        <li><a onclick="wishList({{ $view->id }}, this)" title="Add to Wishlist"><i class="fa fa-heart-o"></i></a></li>
									</ul>
								</div>
							</div>

							<div class="product-info">
								<div class="customar-comments-box">
									<div class="rating-box">
                                        @foreach($rating as $rate)
                                            @if($view->id == $rate->product_id)
                                                @php
                                                    $AvgRate = round(Rate::getAvgRate($view->id));
                                                @endphp
                                                @if($AvgRate)
                                                    @for($i = 0; $i <= $AvgRate; $i++)
                                                        <i class="fa fa-star" style="color:#FFBA00 "></i>
                                                    @endfor
                                                    @for($i = 0; $i < (4 - $AvgRate); $i++)
                                                        <i class="fa fa-star"></i>
                                                    @endfor
                                                @else
                                                    @for($i = 0; $i <= 4; $i++)
                                                        <i class="fa fa-star"></i>
                                                    @endfor
                                                @endif
                                            @endif
                                        @endforeach
									</div>
									<div class="review-box">
                                        @foreach($reviews as $review)
                                            @if($view->id == $review->product_id)
                                                @php
                                                    $CountComment  = count(Comment::getCountComment($view->id));
                                                @endphp
                                            @endif
                                        @endforeach
{{--                                        <span>{{ $CountComment  }} Review (s)</span>--}}
									</div>
								</div>
								<a href="{{ route('product.detail', $view->id) }}">{{ $view->name }}</a>
								<div class="price-box">
                                    @if($saleProduct->discount != 0)
                                        <span class="old-price">{{ $saleProduct['price'] }}đ</span>
                                        <span class="price">{{ $saleProduct['discount'] }}đ</span>
                                    @else
                                        <span class="old-price">{{ $saleProduct['price'] }}đ</span>
                                    @endif
								</div>
							</div>

						</div>
					</div>
                    @endforeach
					<!-- BESTSELLER-SINGLE-ITEM END -->
				</div>
				<!-- BESTSELLER-CAROUSEL END -->
			</div>
		</div>
	</div>
	<!-- BESTSELLER-PRODUCTS-AREA END -->
</div>
@endsection

@section('scripts')

<script>
	function wishList(product_id, obj) {
		var urlWishList = "{{ route('wishlist.toggle', ':id')}}";
		urlWishList = urlWishList.replace(':id', product_id);

		var icon_tag = obj.children[0];

		$.ajax({
			url: urlWishList,
			method: 'GET',
			success: function(data) {
				switch (data) {
					case "Added":
						var text = "Đã thêm vào danh sách ước";
						var icon = "fa fa-heart";
						var status = "success";
						break;
					case "Deleted":
						var text = "Đã xóa khỏi danh sách ước";
						var icon = "fa fa-heart-o";
						var status = "success";
						break;
					case "Unauth":
						var text = "Bạn chưa đăng nhập";
						var icon = "fa fa-heart-o";
						var status = "info";
						break;
					default:
						var text = "Xử lý thông tin thất bại";
						var icon = "fa fa-heart-o";
						var status = "error";
						break;
				}
				var icon_i = obj.childNodes[0];
				icon_i.setAttribute("class", icon);
				const Toast = Swal.mixin({
					toast: true,
					position: 'top-end',
					showConfirmButton: false,
					timer: 1000
				})
				Toast.fire({
					icon: status,
					title: text
				})

			},
			error: function() {
				const Toast = Swal.mixin({
					toast: true,
					position: 'top-end',
					showConfirmButton: false,
					timer: 1000
				})
				Toast.fire({
					icon: "error",
					title: "Có lỗi xảy ra"
				})
			}
		})
	}

</script>

@endsection
