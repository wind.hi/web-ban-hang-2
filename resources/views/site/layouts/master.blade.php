<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>@yield('title', 'Web-ban-hang')</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="shortcut icon" type="image/x-icon" href="{{asset('frontend/img/favicon.png')}}">
{{--    link share--}}
{{--    <link rel="canonical" href="{{$url_canonical}}">--}}
    <meta property="og:site_name" content="http://localhost/web-ban-hang-2/public/">
{{--    <meta property="og:url" content="{{$url_canonical}}">--}}
    @include('site.partials.styles')
</head>

<body>

	<!-- HEADER-TOP  -->
	@include('site.partials.header-top')

	<!-- HEADER-MIDDLE  -->
	@include('site.partials.header-middle')

	<!-- MAIN-MENU-AREA  -->
	@include('site.partials.main-menu')

	<!-- MAIN-CONTENT-SECTION -->
	<section class="main-content-section">
		<div class="container">
		@yield('content')
		</div>
	</section>


	<!-- BRAND-CLIENT-AREA -->
	@include('site.partials.brand-client')

	<!-- COMPANY-FACALITY -->
	@include('site.partials.company-facality')

	<!-- FOOTER-TOP-AREA -->
	@include('site.partials.footer-top')

	<!-- COPYRIGHT-AREA -->
	@include('site.partials.copyright')


	@include('site.partials.scripts')
	@yield('scripts')
</body>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=303472387460075&autoLogAppEvents=1" nonce="p2n8VQWU"></script>
</html>
