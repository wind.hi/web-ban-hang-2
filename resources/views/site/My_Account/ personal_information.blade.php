@extends('site.layouts.master')
@section('title', 'My Account ')
@section('content')
    <section class="main-content-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- BSTORE-BREADCRUMB START -->
                    <div class="bstore-breadcrumb">
                        <a href="index.html">HOMe</a>
                        <span><i class="fa fa-caret-right	"></i></span>
                        <span>Authentication</span>
                    </div>
                    <!-- BSTORE-BREADCRUMB END -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2 class="page-title">Thông tin cá nhân</h2>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- PERSONAL-INFOMATION START -->
                    <div class="personal-infomation">
                        @if (Session::has('status'))
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                {{Session::get('status')}}
                            </div>
                        @endif
                        <form class="primari-box personal-info-box" id="personalinfo" method="post" action="">
                            @csrf
                            <h3 class="box-subheading">Thông tin cá nhân</h3>
                            <div class="personal-info-content">
                                <div class="form-group primary-form-group p-info-group">
                                    <label for="firstname">Name <sup>*</sup></label>
                                    <input type="text" value="{{$edit_customer_user->name}}" name="name" id="name" class="form-control input-feild">
                                    @if($errors->has('name'))
                                        <span class="error-text">
                                        {{$errors->first('name')}}
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group primary-form-group p-info-group">
                                    <label for="address">Địa Chỉ <sup>*</sup></label>
                                    <input type="text" value="{{$edit_customer_user->address}}" name="address" id="address" class="form-control input-feild">
                                    @if($errors->has('address'))
                                        <span class="error-text">
                                        {{$errors->first('address')}}
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group primary-form-group p-info-group">
                                    <label for="address">Số Điện Thoại <sup>*</sup></label>
                                    <input type="number" value="{{Auth::user()->phone}}" name="phone" id="phone" class="form-control input-feild">
                                    @if($errors->has('phone'))
                                        <span class="error-text">
                                        {{$errors->first('phone')}}
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group primary-form-group p-info-group">
                                    <label for="password">Password <sup>*</sup></label>
                                    <input type="password" value="{{$edit_customer_user->password}}" name="password" id="password" class="form-control input-feild">
                                </div>
                                <div class="form-group primary-form-group p-info-group">
                                    <label for="password">Re_Password <sup>*</sup></label>
                                    <input type="password" value="{{$edit_customer_user->password}}" name="required" id="required" class="form-control input-feild">
                                </div>
                                <div class="submit-button p-info-submit-button">
                                    <button type="submit" id="SubmitCreate" class="btn main-btn">
											<span>
												Register
												<i class="fa fa-chevron-right"></i>
											</span>
                                    </button>
                                    <span class="required-field"><sup>*</sup>Required field</span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- PERSONAL-INFOMATION END -->
                </div>
            </div>
        </div>
    </section>
@endsection
