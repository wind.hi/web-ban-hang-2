@extends('site.layouts.master')
@section('title', 'My Account ')
@section('content')
    <section class="main-content-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- BSTORE-BREADCRUMB START -->
                    <div class="bstore-breadcrumb">
                        <a href="{{route('index')}}">HOMe</a>
                        <span><i class="fa fa-caret-right	"></i></span>
                        <span>My account</span>
                    </div>
                    <!-- BSTORE-BREADCRUMB END -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2 class="page-title">My account</h2>
                </div>
                <div class="account-info-text">
                    Welcome to your account. Here you can manage all of your personal information and orders.
                </div>
                <!-- ACCOUNT-INFO-TEXT START -->
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="account-info">
                        <div class="single-account-info">
                            <ul>
                                <li><a href="{{route('user.address.add')}}"><i class="fa fa-building"></i><span>Add my first address</span>	</a></li>
                                <li><a href="#"><i class="fa fa-list-ol"></i><span>Order history and details</span>	</a></li>
                                <li><a href="#"><i class="fa fa-file-o"></i><span>My credit slips</span>	</a></li>
                                <li><a href="checkout-address.html"><i class="fa fa-building"></i><span>Địa Chỉ của tôi</span>	</a></li>
                                <li><a href="{{route('personal.information',Auth::user()->id)}}"><i class="fa fa-user"></i><span>Thông tin cá nhân</span>	</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="account-info">
                        <div class="single-account-info">
                            <ul>
                                <li><a href="{{route('wishlist.list')}}"><i class="fa fa-heart"></i><span>My wishlists</span>	</a></li>
                            </ul>
                            @if(\App\Entity\Vendor::where('user_id',\Illuminate\Support\Facades\Auth::id())->exists() && \App\Entity\Vendor::where('user_id',\Illuminate\Support\Facades\Auth::id())->first()->status == 0)
                                <ul>
                                    <li><a href="wishlist.html"><i class="fa fa-paper-plane"></i><span>My store information</span>	</a>
                                        <span style="color:red; font-size: 14px">* đang confirm</span></li>
                                </ul>
                            @elseif(\App\Entity\Vendor::where('user_id',\Illuminate\Support\Facades\Auth::id())->exists() && \App\Entity\Vendor::where('user_id',\Illuminate\Support\Facades\Auth::id())->first()->status == 1)
                                <ul>
                                    <li><a href="{{route('become.vendor')}}"><i class="fa fa-paper-plane"></i><span>My store information</span></a>
                                    </li>
                                </ul>
                            @else
                                <ul>
                                    <li><a href="{{route('become.vendor')}}"><i class="fa fa-paper-plane"></i><span>Become to vendor</span> </a>
                                    </li>
                                </ul>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- ACCOUNT-INFO-TEXT END -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- BACK TO HOME START -->
                    <div class="home-link-menu">
                        <ul>
                            <li><a href="{{route('index')}}"><i class="fa fa-chevron-left"></i> Home</a></li>
                        </ul>
                    </div>
                    <!-- BACK TO HOME END -->
                </div>
            </div>
        </div>
    </section>


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@endsection
