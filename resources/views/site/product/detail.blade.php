@extends('site.layouts.master')
@section('title', 'Chi tiết sản phẩm')
@section('content')
@php
    use App\Entity\Comment;
    use App\Entity\Rate;
@endphp
    <section class="main-content-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- BSTORE-BREADCRUMB START -->
                    <div class="bstore-breadcrumb">
                        <a href="{{ route('index') }}">home<span><i class="fa fa-caret-right"></i> </span> </a>
                        <span> <i class="fa fa-caret-right"> </i> </span>
                        <a > {{ $category->title }} </a>
                        <span>detail</span>
                    </div>
                    <!-- BSTORE-BREADCRUMB END -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <!-- SINGLE-PRODUCT-DESCRIPTION START -->
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-12">
                            <div class="single-product-view">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    @foreach($images as $image)
                                        <div class="tab-pane @if($loop->iteration == 1) active @endif" id="thumbnail_{{ $loop->iteration }}">
                                            <div class="single-product-image">
                                                <img src="{{ asset('') }}frontend/img/product/{{ $image->image }}" alt="single-product-image" />
                                                @if($status == 'new')
                                                    <a class="new-mark-box" href="#">new</a>
                                                @endif
                                                <a class="fancybox" href="{{ asset('') }}frontend/img/product/{{ $image->image }}" data-fancybox-group="gallery"><span class="btn large-btn">View larger <i class="fa fa-search-plus"></i></span></a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="select-product">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs select-product-tab bxslider">
                                    @foreach($images as $image)
                                        <li class="@if($loop->iteration == 1) active @endif">
                                            <a href="#thumbnail_{{ $loop->iteration }}" data-toggle="tab"><img src="{{ asset('') }}frontend/img/product/{{ $image->image }}" alt="pro-thumbnail" /></a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12">
                            <div class="single-product-descirption">
                                <h2>{{ $info->name }} </h2>

                                <!-- Todo: Rate, Review -->
                                <div class="single-product-review-box">
                                    <div class="rating-box">
                                        @if($sum_rate)
                                            @for($i = 0; $i <= $sum_rate; $i++)
                                                <i class="fa fa-star" style="color:#FFBA00 "></i>
                                            @endfor
                                            @for($i = 0; $i < (4 - $sum_rate); $i++)
                                                <i class="fa fa-star"></i>
                                            @endfor
                                        @else
                                            @for($i = 0; $i <= 4; $i++)
                                                <i class="fa fa-star"></i>
                                            @endfor
                                        @endif
                                    </div>
                                    <div class="read-reviews">
                                        <a href="{{ route('show.comment',$product->id) }}">Read reviews ({{ $review }})</a>
                                    </div>
                                    <div class="write-review"></div>
                                </div>


                                <div class="single-product-price">
                                    @if($product->discount != 0)
                                        <h2>$ {{ $product->discount }}</h2>
                                        <h3><del>$ {{ $product->price }}</del><br></h3>
                                    @else
                                        <h2>$ {{ $product->price }}</h2>
                                        <h3><del></del><br></h3>
                                    @endif
                                </div>

                                <div class="single-product-desc">
                                    <p>{{ $info->short_description }}</p>
                                    @if($product->quantity > 0)
                                        <div class="product-in-stock">
                                            <p>{{ $product->quantity }} Items<span>In stock</span></p>

                                        </div>
                                    @else
                                        <div class="product-out-stock">
                                            <p>{{ $product->quantity }} Items<span>Out stock</span></p>
                                        </div>
                                    @endif
                                </div>
                                <hr>
                                <div class="single-product-quantity">
                                    <p class="small-title">Quantity</p>
                                    <div class="cart-quantity">
                                        <div class="cart-plus-minus-button single-qty-btn">
                                            <input class="cart-plus-minus sing-pro-qty" type="text" name="qtybutton" value="1">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                @if(!is_null($options))
                                    <div class="container-fluid">
                                        <div class="row">
                                            <b>Options</b>
                                            <form>
                                                @foreach($options as $option)
                                                    <label class="radio-inline">
                                                        <input type="radio" onchange="update(this)" value="{{ $option->id }}" data-qty="{{$option->quantity}}" name="attribute" <?php if($loop->iteration == 1)echo "checked" ?>>
                                                        {{ $option->name }}
                                                    </label>
                                                @endforeach
                                            </form>
                                            <br>
                                        </div>
                                    </div>
                                    <br>
                            @endif
                            <!--  -->
                                <div class="single-product-add-cart">
                                    <input type="hidden" id="subproduct_id">
                                    <input type="hidden" id="quantity_subproduct">
                                    <button class="add-cart-text" onclick="return addToCart(this);" >Add to cart</button>
                                    <div style="float: right">
                                        <div class="fb-share-button" data-href="http://localhost/web-ban-hang-2/public/" data-layout="box_count" data-size="large">
                                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{$url_canonical}}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- SINGLE-PRODUCT-DESCRIPTION END -->
                    <hr>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="product-more-info-tab">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs more-info-tab">
                                    <li class="active"><a href="#moreinfo" data-toggle="tab">more info</a></li>
                                    <li><a href="#review" data-toggle="tab">reviews</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="moreinfo">
                                        <div class="tab-description">
                                            {!! $info->description !!}
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="review">
                                        @if(Auth::id())
                                            <div class="row tab-review-row">
                                                <div class="col-xs-5 col-sm-4 col-md-4 col-lg-3 padding-5">
                                                    <div class="tab-rating-box">
                                                        @if(!$rates->exists())
                                                            <span id="title_rating">Grade: </span>
                                                            <br>
                                                            <div class="rating-box" id="rating_star">
                                                                <i class="fa fa-star rating-star" data-index = "0"></i>
                                                                <i class="fa fa-star rating-star" data-index = "1"></i>
                                                                <i class="fa fa-star rating-star" data-index = "2"></i>
                                                                <i class="fa fa-star rating-star" data-index = "3"></i>
                                                                <i class="fa fa-star rating-star" data-index = "4"></i>
                                                            </div>
                                                        @else
                                                            <div class="review-author-info">
                                                                <strong>Your Rating</strong>
                                                                <hr>
                                                                @foreach($rates->get() as $rate)
                                                                    <span id="content_rating">
                                                                        @for($star = 0; $star <= $rate['point_star']; $star ++)
                                                                            <i class="fa fa-star fa-2x" style="color:#FFBA00 "></i>
                                                                        @endfor
                                                                        @for($star = 0; $star < (4 - $rate['point_star']); $star ++)
                                                                            <i class="fa fa-star fa-2x"></i>
                                                                        @endfor
                                                                    </span>
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-xs-7 col-sm-8 col-md-8 col-lg-9 padding-5">
                                                    <div class="write-your-review" id="form_comment">
                                                        <form method="post" action="{{ route('insert.comment',$product->id) }}" >
                                                            @csrf
                                                            @if ($errors->any())
                                                                <div class="alert alert-danger alert-dismissible" role="alert">
                                                                    <ul>
                                                                        @foreach ($errors->all() as $error)
                                                                            <li>{{ $error }}</li>
                                                                        @endforeach
                                                                    </ul>
                                                                </div>
                                                            @endif
                                                            <div class="form-group">
                                                                <label for="Textarea1">WRITE A REVIEW</label>
                                                                <textarea class="form-control" name="Textarea1" id="Textarea1" rows="4" placeholder="Please leave a comment" required></textarea>
                                                            </div>
                                                            <button class="btn btn-success">Comment</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <div class="text-center">
                                                <strong>Login To Comment!</strong> <a href="{{ route('login') }}" class="btn btn-success">Login</a>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="left-title-area">
                                <h2 class="left-title">Sản Phẩm liên quan</h2>
                            </div>
                        </div>
                        <div class="related-product-area featured-products-area">
                            <div class="col-sm-12">
                                <div class=" row">
                                    <!-- RELATED-CAROUSEL START -->
                                    <div class="related-product">
                                        <!-- SINGLE-PRODUCT-ITEM START -->
                                    @foreach($rale_products as $rale_product)
                                        <div class="item">
                                            <div class="single-product-item">
                                                <div class="product-image">
                                                    @foreach($productImages as $productImage )
                                                        @if($rale_product->id == $productImage->product_id)
                                                            <a href="{{ route('product.detail',$rale_product->id) }}"><img height="263px"class="img-product" src="{{asset('')}}frontend/img/product/{{ $productImage['image'] }}" alt="product-image" /></a>
                                                        @endif
                                                    @endforeach
                                                </div>
                                                <div class="product-info">
                                                    <div class="customar-comments-box">
                                                        <div class="rating-box">
                                                            @foreach($rating as $rate)
                                                                @if($rate->product_id == $rale_product->id)
                                                                    @php
                                                                        // lấy trung bình sao của 1 sản phẩm theo 1 mảng sản phẩm
                                                                         $AvgRate = round(Rate::getAvgRate($rale_product->id));
                                                                    @endphp
                                                                    @if($AvgRate)
                                                                        @for($i = 0; $i <= $AvgRate; $i++)
                                                                            <i class="fa fa-star" style="color:#FFBA00 "></i>
                                                                        @endfor
                                                                        @for($i = 0; $i < (4 - $AvgRate); $i++)
                                                                            <i class="fa fa-star"></i>
                                                                         @endfor
                                                                    @else
                                                                        @for($i = 0; $i <= 4; $i++)
                                                                            <i class="fa fa-star"></i>
                                                                        @endfor
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                        <div class="review-box">
                                                            @foreach($reviews as $rev)
                                                                @if($rev->product_id == $rale_product->id)
                                                                    @php
                                                                        // đếm comment của 1 sản phẩm trong 1 mảng sản phẩm
                                                                            $CountComment = count(Comment::getCountComment($rale_product->id));
                                                                    @endphp
                                                                @endif
                                                            @endforeach
{{--                                                            <span>{{ $CountComment }} Review(s)</span>--}}
                                                        </div>
                                                    </div>
                                                    <a href="{{ route('product.detail',$rale_product->id) }}">{{$rale_product->name}}</a>
                                                    <div class="price-box">
                                                        @if($rale_product->discount != 0)
                                                            <span class="old-price">${{ $rale_product['price'] }}</span>
                                                            <span class="price">${{ $rale_product['discount'] }}</span>
                                                        @else
                                                            <span class="price">${{ $rale_product['price'] }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                        <!-- SINGLE-PRODUCT-ITEM END -->
                                        <!-- SINGLE-PRODUCT-ITEM START -->
                                    </div>
                                    <!-- RELATED-CAROUSEL END -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- RELATED-PRODUCTS-AREA END -->
                </div>
                <!-- RIGHT SIDE BAR START -->
                @if (sizeof($ads))
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <!-- SINGLE SIDE BAR START -->
                        <div class="single-product-right-sidebar">
                            @foreach ($ads as $item)
                                <div class="slider-right zoom-img" style="margin-bottom: 30px">
                                    <a href="{{$item->link}}"><img class="img-responsive" src="{{ asset('frontend/img/product/') }}{{$item->image}}" alt="sidebar left" /></a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif

                <!-- SINGLE SIDE BAR END -->
            </div>
        </div>
    </section>

    {{--    modal notifi add cart--}}
    <div class="modal fade" id="modalNotifi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width:300px;">
            <div class="modal-content">
                <div class="modal-body" id="modal-content">
                    <h4>Đã thêm vào giỏ hàng</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tiếp tục mua</button>
                    <a href="{{route('my.cart')}}" target="_self"><button type="button" class="btn btn-primary">Đi tới giỏ hàng</button></a>
                </div>
            </div>
        </div>
    </div>
    {{--    end modal--}}
@endsection

@section('scripts')
    <script>
        function update(obj) {
            var id_product = {{ $info->id }};
            var id_option = obj.value;
            var quantity_option = $(obj).data('qty');
            var url = "{{ route('product.option', ['id' => ':id_product', 'option_id' => ':id_option']) }}";
            url = url.replace(':id_product', id_product);
            url = url.replace(':id_option', id_option);
            $("#subproduct_id").val(id_option);
            $("#quantity_subproduct").val(quantity_option);

            $.ajax({
                url: url,
                method: 'GET',
                success: function(data) {
                    show(data);
                }

            })
        }

        function show(data) {
            var obj = JSON.parse(data)
            if (obj.discount != 0) {
                $(".single-product-price")
                    .empty()
                    .append(
                        $("<h2></h2>").text(" ₫ " + obj.discount),
                        $("<h3></h3>").html("<del> ₫ " + obj.price + "</del><br>")
                    );
            } else {
                $(".single-product-price")
                    .empty()
                    .append(
                        $("<h2></h2>").text(" ₫ " + obj.price),
                        $("<h3><del></del><br></h3>")
                    );
            }
            if (obj.quantity) {
                var quantityClass = "product-in-stock";
                var quantityText = "In";
            } else {
                var quantityClass = "product-out-stock";
                var quantityText = "Out";
            }
            var quantityElement = "<p>" + obj.quantity + " Items" + "<span>" + quantityText + " stock</span></p>";

            $(".product-in-stock")
                .attr("class", quantityClass)
                .empty()
                .append(quantityElement)
        }

        function addToCart(e){
            var quantityProduct = $("#quantity_subproduct").val();
            var product_id = $("#subproduct_id").val();
            var quantity = $("input[name='qtybutton']").val();
            if(Number(quantityProduct) < Number(quantity)){
                alert('please nhập nhỏ hơn số lượng hàng đang có !');
                return false;
            }
            $.ajax({
                    url:"{{ route('add_to_cart') }}",
                    method:'POST',
                    data:{
                        _token: "{{ csrf_token() }}",
                        product_id:product_id,
                        quantity:quantity
                    },
                    success: function (data) {
                        console.log(data);
                        $("#modalNotifi").modal('show');
                        // $.ajax({
                        //     url:'route('update_cart')',
                        //     method:'GET',
                        //     success: function (data) {
                        //         $('#header-cart').empty();
                        //         $('#header-cart').html(data);
                        //         console.log(data);
                        //     },
                        //     error: function(error) {
                        //     }
                        // })
                    },
                    error: function(error) {
                    }
                },
            );
        }
    </script>
    <script>
        if (!!window.performance && window.performance.navigation.type === 2) {
            // value 2 means "The page was accessed by navigating into the history"
            console.log('Reloading');
            window.location.reload(); // reload whole page

        }
        $(document).ready(function () {
            if ($("input[name='attribute']").is(':checked')) {
                var id = $("input[name='attribute']").attr('value');
                var qty_option = $("input[name='attribute']").data('qty');
                $("#subproduct_id").val(id);
                $("#quantity_subproduct").val(qty_option);
            } else{
                $("#subproduct_id").val('{{$product->id}}');
                $("#quantity_subproduct").val('{{$product->quantity}}');
            }
        })

        // js phần rating
        var rateIndex = -1;
        $(document).ready(function () {
            resetColor();
            if(localStorage.getItem('rateIndex') != null)
                setStar(parseInt(localStorage.getItem('rateIndex')));
            <?php if (!$rates ->exists()){ ?>
            $('#form_comment').hide();
            <?php } ?>
            $('.rating-star').on('click',function () {
                rateIndex = parseInt($(this).data('index'));
                localStorage.setItem('rateIndex',rateIndex);
                var productId = "{{$product->id}}";
                $('.review-author-info').show();
                $('#rating_star').hide(900);
                $('#form_comment').show(1000)
                addToDB(rateIndex,productId);
            });
            $('.rating-star').mouseover(function () {
                resetColor();
                var curentIndex = parseInt($(this).data('index'));
                setStar(curentIndex);
            });
            $('.rating-star').mouseleave(function () {
                resetColor();
                if(rateIndex != -1){
                    setStar(rateIndex);
                }
            });

        });

        function resetColor() {
            $('.rating-star').css('color','#959595');
        }
        function setStar(max) {
            for( var i = 0; i <= max; i++ ){
                $('.rating-star:eq('+i+')').css('color','#FFBA00');
            }
        }
        function addToDB(rateIndex, productId) {
            $.ajax({
                url :   "{{ route('rating') }}",
                method: "GET",
                data  : {
                    "star_index":rateIndex,
                    "product_id":productId
                },success:function () {
                    $point = rateIndex;
                    for (var i =0; i <= $point ; i++){
                        $('#title_rating').append('<i class="fa fa-star" style="color:#FFBA00 "></i>');
                    }
                    for (var i =0; i <  (4 - $point) ; i++){
                        $('#title_rating').append('<i class="fa fa-star"></i>');
                    }
                }
            });
        }
    </script>

@endsection
