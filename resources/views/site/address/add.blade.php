@extends('site.layouts.master')
@section('title', 'Địa chỉ')
@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <!-- BSTORE-BREADCRUMB START -->
        <div class="bstore-breadcrumb">
            <a href="{{ route('index') }}">HOME<span><i class="fa fa-caret-right"></i> </span> </a>
            <span> Addresses </span>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3 class="box-subheading">Create new address</h3>
        <form class="new-account-box primari-box" id="create-address" method="post" action="#">
            <div class="form-row">
                <div class="col-6  primary-form-group">
                    <label for="address_name">Tên địa chỉ</label>
                    <input type="text" name="address_name" id="address_name" class="form-control" required>
                </div>
                <div class="col-6 primary-form-group">
                    <label for="name">Người nhận</label>
                    <input type="text" name="name" id="name" class="form-control" required>
                </div>
                <div class="col-6 primary-form-group">
                    <label for="address">Địa chỉ</label>
                    <input type="text" name="address" id="address" class="form-control" required>
                </div>
                <div class="col-6 primary-form-group">
                    <label for="phone">Số điện thoại liên lạc</label>
                    <input type="text" name="phone" id="phone" class="form-control" required>
                </div>
            </div>
            @csrf
            <div class="form-content">
                <div class="submit-button">
                    <button form="create-address" type="submit" class="btn main-btn">
                        <span>
                            <i class="fa fa-building submit-icon"></i>
                            Create new address
                        </span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
