@extends('site.layouts.master')
@section('title', 'Địa chỉ')
@section('content')
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<!-- BSTORE-BREADCRUMB START -->
		<div class="bstore-breadcrumb">
			<a href="{{ route('index') }}">HOME<span><i class="fa fa-caret-right"></i> </span> </a>
			<span> Addresses </span>
		</div>
		<!-- BSTORE-BREADCRUMB END -->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="add-new-address">
					<a href="{{ route('user.address.add') }}" class="new-address-link">Add a new address<i class="fa fa-chevron-right"></i></a>
				</div>
			</div>
			@foreach ($addresses as $address)

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="primari-box">
					<ul class="address">
						<li>
							<h3 class="page-subheading box-subheading">
								{{ $address->address_name }}
							</h3>
						</li>
						<li><span class="address_address">{{ $address->name }}</span></li>
						<li><span class="address_name">{{ $address->address }}</span></li>
						<li><span class="address_phone">{{ $address->phone }}</span></li>
						<li class="update-button">
							<a href="{{ route('user.address.update',$address->id) }}">Update<i class="fa fa-chevron-right"></i></a>
							<a href="{{ route('user.address.delete',$address->id) }}">Remove<i class="fa fa-chevron-right"></i></a>
						</li>
						<li class="update-button">
							<!-- DELIVERY ADDRESS END -->
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>

@endsection
