@extends('site.layouts.master')
@section('title', 'Checkout Signin')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!-- BSTORE-BREADCRUMB START -->
            <div class="bstore-breadcrumb">
                <a href="index.html">HOMe</a>
                <span><i class="fa fa-caret-right	"></i></span>
                <span>Shipping:</span>
            </div>
            <!-- BSTORE-BREADCRUMB END -->
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2 class="page-title">Shipping:</h2>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!-- SHOPING-CART-MENU START -->
            <div class="shoping-cart-menu">
                <ul class="step">
                    <li class="step-todo first step-done">
                        <span><a href="cart.html">01. Summary</a></span>
                    </li>
                    <li class="step-todo second step-done">
                        <span><a href="checkout-signin.html">02. Sign in</a></span>
                    </li>
                    <li class="step-todo third step-done">
                        <span><a href="checkout-address.html">03. Address</a></span>
                    </li>
                    <li class="step-current four">
                        <span>04. Shipping</span>
                    </li>
                    <li class="step-todo last" id="step_end">
                        <span>05. Payment</span>
                    </li>
                </ul>
            </div>
            <!-- SHOPING-CART-MENU END -->
        </div>
    </div>
    @if(\Gloudemans\Shoppingcart\Facades\Cart::count() > 0)
        <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!-- PRODUCT-DELIVERY-OPTION START -->
            <div class="product-delivery-option">
                <div class="product-delivery-address">
                    <p>Choose a shipping option for this address: My address</p>
                </div>
                <!-- PRODUCT-DELIVERY-ITEM START -->
                <div class="product-delivery-item">
                    <div class="product-delivery-single-item">
                        <div class="table-responsive">
                            <!-- PRODUCT-DELIVERY SINGLE OPTION START -->
                            <table class="table table-bordered delivery-table">
                                <tbody>
                                <tr>
                                    <td class="delivery-option-radio">
                                        <div class="dalivery-radio">
														<span class="radio-box">
															<input type="radio" value="1" name="deliverymethod" checked >
														</span>
                                        </div>
                                    </td>
                                    <td class="delivery-method-icon">
                                        <img src="img/bank.png" alt="">
                                    </td>
                                    <td class="carrey-info">
                                        <strong>
                                            Ship to home (cod)<br/>
                                            free ship
                                        </strong><br>

                                    </td>
                                    <td class="carrey-cost">Free</td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- PRODUCT-DELIVERY SINGLE OPTION END -->
                        </div>
                        <div class="table-responsive">
                            <!-- PRODUCT-DELIVERY SINGLE OPTION START -->
                            <table class="table table-bordered delivery-table">
                                <tbody>
                                <tr>
                                    <td class="delivery-option-radio">
                                        <div class="dalivery-radio">
														<span class="radio-box">
															<input type="radio" value="2" name="deliverymethod" >
														</span>
                                        </div>
                                    </td>
                                    <td class="delivery-method-icon">
                                        <img src="img/delivery-method.jpg" alt="">
                                    </td>
                                    <td class="carrey-info">
                                        <strong>Express delivery<br/>
                                            receive in 1 hour
                                        </strong>

                                    </td>
                                    <td class="carrey-cost">
                                        2$
                                    </td>
                                </tr>
                                </tbody></table>
                            <!-- PRODUCT-DELIVERY SINGLE OPTION END -->
                        </div>
                    </div>
                </div>
                <!-- PRODUCT-DELIVERY-ITEM START -->
                <!-- TERMS-OF-SERVICE START -->
                <div class="terms-of-service">
                    <p>Terms of service</p>
                    <div class="form-group new-ac-form-group p-info-group ">
                        <label class="cheker">
                            <input type="checkbox" name="checkbox">
                            <span></span>
                        </label>
                        <span class="agree">I agree to the terms of service and will adhere to them unconditionally.<a href="#">(Read the Terms of Service)</a></span>
                    </div>
                </div>
                <!-- TERMS-OF-SERVICE END -->
            </div>
            <!-- PRODUCT-DELIVERY-OPTION END -->
            <!-- RETURNE-CONTINUE-SHOP START -->
            <div class="returne-continue-shop">
                <a href="index.html" class="continueshoping"><i class="fa fa-chevron-left"></i>Continue shopping</a>
                <a  class="procedtocheckout" onclick="return nextStep5(this);">Proceed to checkout<i class="fa fa-chevron-right"></i></a>
            </div>

        <!-- RETURNE-CONTINUE-SHOP END -->
        </div>
    </div>
    @else
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!-- BSTORE-BREADCRUMB START -->
                {{--                <p style="font-weight: bold">Not have account. <a href="{{route('customer.add.new')}}">Register</a> or <a href="{{route('LoginSite')}}">Login</a></p>--}}
                <p style="font-weight: bold">Not items in cart. </p>

                <!-- BSTORE-BREADCRUMB END -->
            </div>
        </div>
    @endif
@endsection

@section('scripts')
    <script>
        function nextStep5(e) {
            var typeShip = $('input[name="deliverymethod"]:checked').val();
            var addIdBill = "{{$addressBill}}";
            var addIdDeli = "{{$addressDeli}}";
            window.location.href = "{{route('checkout')}}"+'?addDeli='+addIdDeli+'&addBill='+addIdBill+'&methodShipping='+typeShip;

        {{--$.ajax({--}}
            {{--    url: '{{route('get.info.address')}}',--}}
            {{--    method: 'GET',--}}
            {{--    data: {--}}
            {{--        address_id : addId,--}}
            {{--    },--}}
            {{--    success: function (data) {--}}
            {{--        $(".address-bill").find('.info-address').remove();--}}
            {{--        $(".address-bill").append(data);--}}
            {{--    },--}}
            {{--    error: function(error) {--}}
            {{--        console.log(error);--}}
            {{--    }--}}
            {{--});--}}
        }
    </script>
@endsection
