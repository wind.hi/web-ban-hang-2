@extends('site.layouts.master')
@section('title', 'Checkout Signin')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!-- BSTORE-BREADCRUMB START -->
            <div class="bstore-breadcrumb">
                <a href="index.html">HOMe</a>
                <span><i class="fa fa-caret-right	"></i></span>
                <span>Addresses</span>
            </div>
            <!-- BSTORE-BREADCRUMB END -->
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2 class="page-title">Addresses</h2>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!-- SHOPING-CART-MENU START -->
            <div class="shoping-cart-menu">
                <ul class="step">
                    <li class="step-todo first step-done">
                        <span><a href="cart.html">01. Summary</a></span>
                    </li>

                        <li class="step-todo second">
                            <span>02. Sign in</span>
                        </li>


                        <li class="step-current third">
                            <span>03. Address</span>
                        </li>

                    <li class="step-todo four">
                        <span>04. Shipping</span>
                    </li>
                    <li class="step-todo last" id="step_end">
                        <span>05. Payment</span>
                    </li>
                </ul>
            </div>
            <!-- SHOPING-CART-MENU END -->
        </div>
        <!-- ADDRESS AREA START -->
        @if(\Gloudemans\Shoppingcart\Facades\Cart::count() > 0)
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="first_item primari-box">
                <!-- DELIVERY ADDRESS START -->

                <ul class="address address-deli">
                    <li>
                        <h3 class="page-subheading box-subheading">
                            Your delivery address
                        </h3>
                        <div class="form-group primary-form-group p-info-group deli-address-group">
                            <label>Choose a address:</label>
                            <div class="birth-day delivery-address">
                                <select class="deli-address" id="deli-address" name="deliveryaddress">
                                    <option value="" selected>---------------</option>

                                @foreach($address as $add)
                                    <option value="{{$add->id}}">{{$add->address_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </li>



                </ul>
                <!-- DELIVERY ADDRESS END -->
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="second_item primari-box">
                <!-- BILLING ADDRESS START -->
                <ul class="address address-bill">
                    <li>
                        <h3 class="page-subheading box-subheading">
                            Your billing address
                        </h3>
                        <div class="form-group primary-form-group p-info-group deli-address-group">
                            <label>Choose a address:</label>
                            <div class="birth-day delivery-address">
                                <select class="bill-address" id="deli-address" name="deliveryaddress">                                    <option value="" selected>---------------</option>
                                    @foreach($address as $add)
                                        <option value="{{$add->id}}">{{$add->address_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </li>

                </ul>
                <!-- BILLING ADDRESS END -->
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="add-new-address">
                <a href="{{ route('user.address.add') }}" class="new-address-link">Add a new address<i class="fa fa-chevron-right"></i></a>
{{--                <div class="form-group p-info-group type-address-group">--}}
{{--                    <label>If you would like to add a comment about your order, please write it in the field below.</label>--}}
{{--                    <textarea class="form-control input-feild " name="addcomment"></textarea>--}}
{{--                </div>--}}
            </div>
            <!-- ADDRESS AREA START -->
            <!-- RETURNE-CONTINUE-SHOP START -->
            <div class="returne-continue-shop ship-address">
                <a href="index.html" class="continueshoping"><i class="fa fa-chevron-left"></i>Continue shopping</a>
                <a  class="procedtocheckout" onclick="return nextStep4(this);">Proceed to checkout<i class="fa fa-chevron-right"></i></a>
            </div>
            <!-- RETURNE-CONTINUE-SHOP END -->
        </div>
    </div>
    @else
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!-- BSTORE-BREADCRUMB START -->
{{--                <p style="font-weight: bold">Not have account. <a href="{{route('customer.add.new')}}">Register</a> or <a href="{{route('LoginSite')}}">Login</a></p>--}}
                <p style="font-weight: bold">Not items in cart. </p>

                <!-- BSTORE-BREADCRUMB END -->
            </div>
        </div>
    @endif
@endsection

@section('scripts')
<script>
   $(".deli-address").change(function () {
       var addId = $('.deli-address option:selected').val();
       if($('.deli-address option:selected').text() === '---------------'){
           $(".address-deli").find('.info-address').remove();
       }
       $.ajax({
           url: '{{route('get.address')}}',
           method: 'GET',
           data: {
               address_id : addId,
           },
           success: function (data) {
               $(".address-deli").find('.info-address').remove();
               $(".address-deli").append(data);
           },
           error: function(error) {
               console.log(error);
           }
       });
   })
   $(".bill-address").change(function () {
       var addId = $('.bill-address option:selected').val();
       if($('.bill-address option:selected').text() === '---------------'){
           $(".address-bill").find('.info-address').remove();
       }
       $.ajax({
           url: '{{route('get.address')}}',
           method: 'GET',
           data: {
               address_id : addId,
           },
           success: function (data) {
               $(".address-bill").find('.info-address').remove();
               $(".address-bill").append(data);
           },
           error: function(error) {
               console.log(error);
           }
       });
   })
</script>
    <script>
        function nextStep4(e) {
            var addIdDeli = $('.deli-address option:selected').val();
            var addIdBill = $('.bill-address option:selected').val();
            if(addIdDeli == '' || addIdBill == ''){
                alert('please choose address deli and bill ');
                return false;
            }
            // alert(addIdDeli);
            window.location.href = "{{route('checkout.shipping')}}"+'?addDeli='+addIdDeli+'&addBill='+addIdBill;
        }
    </script>
@endsection
