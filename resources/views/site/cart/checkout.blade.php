@extends('site.layouts.master')
@section('title', 'Checkout ')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!-- BSTORE-BREADCRUMB START -->
            <div class="bstore-breadcrumb">
                <a href="index.html">HOMe</a>
                <span><i class="fa fa-caret-right"></i></span>
                <span>Your payment method</span>
            </div>
            <!-- BSTORE-BREADCRUMB END -->
        </div>
    </div>
    <form  method="post" action="{{route('create.order')}}" id="insert-order">
        @csrf
        @if(\Gloudemans\Shoppingcart\Facades\Cart::count() > 0)
         <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2 class="page-title">Choose your payment method <span class="shop-pro-item">Your shopping cart contains: 3 products </span></h2>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!-- SHOPING-CART-MENU START -->
            <div class="shoping-cart-menu">
                <ul class="step">
                    <li class="step-todo first step-done">
                        <span><a href="cart.html">01. Summary</a></span>
                    </li>
                    <li class="step-todo second step-done">
                        <span><a href="checkout-signin.html">02. Sign in</a></span>
                    </li>
                    <li class="step-todo third step-done">
                        <span><a href="checkout-address.html">03. Address</a></span>
                    </li>
                    <li class="step-todo four step-done">
                        <span><a href="checkout-shipping.html">04. Shipping</a></span>
                    </li>
                    <li class="step-current last" id="step_end">
                        <span>05. Payment</span>
                    </li>
                </ul>
            </div>
            <!-- SHOPING-CART-MENU END -->
            <!-- CART TABLE_BLOCK START -->
            <div class="table-responsive">
                <!-- TABLE START -->
                <table class="table table-bordered" id="cart-summary">
                    <!-- TABLE HEADER START -->
                    <thead>
                    <tr>
                        <th class="cart-product">Product</th>
                        <th class="cart-description">Description</th>
                        <th class="cart-availability text-center">Availability</th>
                        <th class="cart-unit text-right">Unit price</th>
                        <th class="cart_quantity text-center">Qty</th>
                        <th class="cart-total text-right">Total</th>
                    </tr>
                    </thead>
                    <!-- TABLE HEADER END -->
                    <!-- TABLE BODY START -->

                    <tbody>
                    <!-- SINGLE CART_ITEM START -->
                    <?php
                    $total = 0;
                    ?>
                    @foreach(\Gloudemans\Shoppingcart\Facades\Cart::content() as $item)
                        <tr>
                            <td class="cart-product">
                                <a href="#"><img alt="Blouse" src="{{asset('frontend/img/product/'.$item->options->image)}}"></a>
                            </td>
                            <td class="cart-description">
                                <p class="product-name"><a href="#">{{$item->name}} </a></p>

                                @if(count((array)$item->options->attribute) > 0)
                                    @foreach($item->options->attribute as $key=>$value)
                                        <small><a href="#">{{$key}} : {{$value}}</a></small>
                                    @endforeach
                                @endif

                            </td>
                            <td class="cart-avail"><span class="label label-success">In stock</span></td>
                            <td class="cart-unit">
                                <ul class="price text-right">
                                    <li class="price">{{number_format($item->price,'0','.',',')}} vnd</li>
                                </ul>
                            </td>
                            <td class="cart_quantity text-center">

                                <span>{{$item->qty}}</span>

                            </td>

                            <td class="cart-total">
                                <span class="price">{{number_format($item->price * $item->qty,'0','.',',')}} vnd</span>
                            </td>
                        </tr>
                        <?php
                        $total += $item->price * $item->qty;
                        ?>
                    @endforeach
                    <!-- SINGLE CART_ITEM END -->
                    </tbody>
                    <!-- TABLE BODY END -->
                    <!-- TABLE FOOTER START -->

                    <tfoot>
                    <tr>
                        <td class="text-right" colspan="4">Total products</td>
                        <td class="price" colspan="2">{{number_format($total,'0','.',',')}} vnd</td>
                    </tr>
                    <tr>
                        <td class="text-right" colspan="4">Method shipping</td>
                        @if($methodShip == 1)
                        <td class="price" colspan="2">Cod</td>
                            @else
                            <td class="price" colspan="2">Express delivery</td>
                            @endif

                    </tr>
                    <tr>
                        <td class="text-right" colspan="4">Total shipping</td>
                        @if($methodShip == 1)
                            <td class="price" colspan="2">Free</td>
                        @else
                            <td class="price" colspan="2">20 k</td>
                        @endif

                    </tr>
                    <input type="hidden" name="method" value="{{$methodShip}}">
                    <tr>
                        <td class="text-right" colspan="4">Total vouchers</td>
                        <td class="price" colspan="2">$0.00</td>
                    </tr>

                    <tr>
                        <td class="total-price-container text-right" colspan="4">
                            <span>Total</span>
                        </td>
                        <td id="total-price-container" class="price" colspan="2">
                            <span id="total-price">{{number_format($total,'0','.',',')}} vnd</span>
                        </td>
                    </tr>
                    <input type="hidden" name="total_price" value="{{$total}}">

                    </tfoot>
                    <!-- TABLE FOOTER END -->
                </table>
                <!-- TABLE END -->
            </div>
            <!-- CART TABLE_BLOCK END -->
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="first_item primari-box">
                        <!-- DELIVERY ADDRESS START -->

                        <ul class="address address-deli">
                            <li>
                                <h3 class="page-subheading box-subheading">
                                    Your delivery address
                                </h3>
                            </li>
                            <li><span class="info-address address_name">{{$addressDeli->name}}</span></li>
                            <li><span class="info-address address_address1">{{$addressDeli->address_name}}</span></li>
                            <li><span class="info-address address_address2">{{$addressDeli->address}}</span></li>
                            <li><span class="info-address address_phone">{{$addressDeli->phone}}</span></li>
                             </ul>
                        <input type="hidden" name="address_deli_id" value="{{$addressDeli->id}}">
                        <input type="hidden" name="phone" value="{{$addressDeli->phone}}">

                        <!-- DELIVERY ADDRESS END -->
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="second_item primari-box">
                        <!-- BILLING ADDRESS START -->
                        <ul class="address address-bill">
                            <li>
                                <h3 class="page-subheading box-subheading">
                                    Your billing address
                                </h3>

                            </li>
                            <li><span class="info-address address_name">{{$addressBill->name}}</span></li>
                            <li><span class="info-address address_address1">{{$addressBill->address_name}}</span></li>
                            <li><span class="info-address address_address2">{{$addressBill->address}}</span></li>
                            <li><span class="info-address address_phone">{{$addressBill->phone}}</span></li>
                            </ul>
                        <input type="hidden" name="address_bill_id" value="{{$addressBill->id}}">

                        <!-- BILLING ADDRESS END -->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!-- RETURNE-CONTINUE-SHOP START -->
            <div class="add-new-address">
                <button type="button" class="btn btn-warning" onclick="return addOrder();">Checkout Complete</button>
            </div>
            <div class="returne-continue-shop">
                <a href="index.html" class="continueshoping"><i class="fa fa-chevron-left"></i>Continue shopping</a>
            </div>
            <!-- RETURNE-CONTINUE-SHOP END -->
        </div>
    </div>
            @else
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- BSTORE-BREADCRUMB START -->
                    {{--                <p style="font-weight: bold">Not have account. <a href="{{route('customer.add.new')}}">Register</a> or <a href="{{route('LoginSite')}}">Login</a></p>--}}
                    <p style="font-weight: bold">Not items in cart. </p>

                    <!-- BSTORE-BREADCRUMB END -->
                </div>
            </div>
        @endif
    </form>



    {{--    modal notifi add cart--}}
    <div class="modal fade" id="modalNotifi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width:300px;">
            <div class="modal-content">
                <div class="modal-body" id="modal-content">
                    <h4>Thanks you !</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Continue shopping</button>
                    <a href="{{route('get.order')}}" target="_self"><button type="button" class="btn btn-primary">View order</button></a>
                </div>
            </div>
        </div>
    </div>
    {{--    end modal--}}

@endsection

@section('scripts')
<script>
    function addOrder() {
        var frm = $('#insert-order');
        var form = $('form#insert-order')[0];
        var formData = new FormData(form);
        $.ajax({
                method: frm.attr('method'),
                url: frm.attr('action'),
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    $("#modalNotifi").modal('show');
                }
            },
        );
        console.log(data);
    }
</script>
@endsection
