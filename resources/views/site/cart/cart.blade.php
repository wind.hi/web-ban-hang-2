@extends('site.layouts.master')
@section('title', 'My cart')
@section('content')

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!-- BSTORE-BREADCRUMB START -->
            <div class="bstore-breadcrumb">
                <a href="{{route('index')}}">HOMe</a>
                <span><i class="fa fa-caret-right	"></i></span>
                <span>Your shopping cart</span>
            </div>
            <!-- BSTORE-BREADCRUMB END -->
        </div>
    </div>
    <div class="row">
        @if(\Gloudemans\Shoppingcart\Facades\Cart::count() > 0)
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!-- SHOPPING-CART SUMMARY START -->
            <h2 class="page-title">Shopping-cart summary <span class="shop-pro-item">Your shopping cart contains: {{count(\Gloudemans\Shoppingcart\Facades\Cart::content())}} products</span></h2>
            <!-- SHOPPING-CART SUMMARY END -->
        </div>
        @else
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!-- SHOPPING-CART SUMMARY START -->
                <h2 class="page-title">Shopping-cart summary <span class="shop-pro-item">Your shopping cart contains: 0 products</span></h2>
                <!-- SHOPPING-CART SUMMARY END -->
            </div>
        @endif
        @if(\Gloudemans\Shoppingcart\Facades\Cart::count() > 0)
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!-- SHOPING-CART-MENU START -->
            <div class="shoping-cart-menu">
                <ul class="step">
                    <li class="step-current first">
                        <span>01. Summary</span>
                    </li>

                    <li class="step-todo second">
                        <span>02. Sign in</span>
                    </li>
                        <li class="step-todo third">
                            <span>03. Address</span>
                        </li>

                    <li class="step-todo four">
                        <span>04. Shipping</span>
                    </li>
                    <li class="step-todo last" id="step_end">
                        <span>05. Payment</span>
                    </li>
                </ul>
            </div>
            <!-- SHOPING-CART-MENU END -->
            <!-- CART TABLE_BLOCK START -->
            <div class="table-responsive">
                <!-- TABLE START -->
                <table class="table table-bordered" id="cart-summary">
                    <!-- TABLE HEADER START -->
                    <thead>
                    <tr>
                        <th class="cart-product">Product</th>
                        <th class="cart-description">Description</th>
                        <th class="cart-avail text-center">Availability</th>
                        <th class="cart-unit text-right">Unit price</th>
                        <th class="cart_quantity text-center">Qty</th>
                        <th class="cart-delete">&nbsp;</th>
                        <th class="cart-total text-right">Total</th>
                    </tr>
                    </thead>
                    <!-- TABLE HEADER END -->
                    <!-- TABLE BODY START -->

                    <tbody>
                    <!-- SINGLE CART_ITEM START -->
                    <?php
                        $total = 0;
                    ?>
					@foreach(\Gloudemans\Shoppingcart\Facades\Cart::content() as $item)
					<tr>
						<td class="cart-product">
							<a href="#"><img alt="Blouse" src="{{asset('frontend/img/product/'.$item->options->image)}}"></a>
						</td>
						<td class="cart-description">
							<p class="product-name"><a href="#">{{$item->name}} </a></p>

							@if(count((array)$item->options->attribute) > 0)
								@foreach($item->options->attribute as $key=>$value)
									<small><a href="#">{{$key}} : {{$value}}</a></small>
									@endforeach
									@endif

						</td>
						<td class="cart-avail"><span class="label label-success">In stock</span></td>
						<td class="cart-unit">
							<ul class="price text-right">
								<li class="price">{{number_format($item->price,'0','.',',')}} vnd</li>
							</ul>
						</td>
						<td class="cart_quantity text-center">
							<div class="cart-plus-minus-button">
								<input class="cart-plus-minus" type="text" name="qtybutton" value="{{$item->qty}}">
								<input type="text" name="value" value="1" hidden>
                            </div>
						</td>
						<input type="hidden" class="rowId_hidden" value="{{$item->rowId}}">
						<td class="cart-delete text-center">
							<span>
								<a href="#" class="cart_quantity_delete" title="Delete"></a>
								<button type="button" class="cart_quantity_delete" title="Delete" data-id="{{$item->rowId}}" onclick="return removeItem(this);">
									<i class="fa fa-trash-o"></i>
								</button>
								<button type="button" class="cart_quantity_delete" title="Update" data-id="{{$item->rowId}}" onclick="return updateItem(this);">
									<i class="fa fa-refresh"></i>
								</button>

							</span>

						</td>
						<td class="cart-total">
							<span class="price">{{number_format($item->price * $item->qty,'0','.',',')}} vnd</span>
						</td>
					</tr>
					<?php
                    $total += $item->price * $item->qty;
                    ?>
					@endforeach
					<!-- SINGLE CART_ITEM END -->
				</tbody>
				<!-- TABLE BODY END -->
				<!-- TABLE FOOTER START -->
				<tfoot>
					<tr class="cart-total-price" id="total-price-first">
						<td class="cart_voucher" colspan="3" rowspan="4"></td>
						<td class="text-right" colspan="3">Total products</td>
						<td id="total_product" class="price" colspan="1">{{number_format($total,'0','.',',')}} vnd</td>
					</tr>
					<tr>
						<td class="text-right" colspan="3">Total vouchers (tax excl.)</td>
						<td class="price" colspan="1">0</td>
					</tr>
					<tr id="total-price">
						<td class="total-price-container text-right" colspan="3">
							<span>Total</span>
						</td>
						<td id="total-price-container" class="price" colspan="1">
							<span id="total-price-place">{{ number_format($total,'0','.',',') }} vnd</span>
						</td>
					</tr>
				</tfoot>
				<!-- TABLE FOOTER END -->
			</table>
			<!-- TABLE END -->
		</div>
		<!-- CART TABLE_BLOCK END -->
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<!-- RETURNE-CONTINUE-SHOP START -->
		<div class="returne-continue-shop">
			<a href="{{route('index')}}" class="continueshoping"><i class="fa fa-chevron-left"></i>Continue shopping</a>


			<a href="{{route('checkout.signin')}}" class="procedtocheckout">Proceed to checkout<i class="fa fa-chevron-right"></i></a>

		</div>

		<!-- RETURNE-CONTINUE-SHOP END -->
	</div>
	@endif


</div>
@endsection

@section('scripts')
<script>
	function updateItem(e) {
		var product_id = $(e).data('id');
		var rowId_hidden = $(".rowId_hidden");
		var quantity = $("input[name='qtybutton']");
		var num = '';
		for (var i = 0; i < rowId_hidden.length; i++) {
			for (var i = 0; i < quantity.length; i++) {
				if (product_id == rowId_hidden[i].value) {
					num = quantity[i].value;
				}
			}
		}
		$.ajax({
			url: "{{ route('update_item_cart') }}",
			method: 'GET',
			data: {
				product_id: product_id,
				quantity: num,
			},
			success: function(data) {
				console.log('thanh cong');
				$("#cart-summary tbody").empty();
				$("#cart-summary tbody").append(data);
				$.ajax({
					url: "{{ route('reload_total_price') }}",
					method: 'GET',
					success: function(data) {
						console.log(data);
						$("#total-price-place")
							.empty()
							.append(
								$("<span id='total-price'></span>").html(data + ' vnd'),
							);
						// $("#total-price").find('td').remove();
						// $("#total-price").append('<td class="total-price-container text-right" colspan="3"><span>Total:</span></td>');
						// $("#total-price").append('<td id="total-price-container" class="price" colspan="1"> <span id="total-price"> ' + data + ' vnd</span></td>');
						//
						// $("#total-price-first").find('td').remove();
						// $("#total-price-first").append(' <td class="cart_voucher" colspan="3" rowspan="4"></td>');
						// $("#total-price-first").append('   <td class="text-right" colspan="3">Total products</td>');
						// $("#total-price-first").append('<td id="total_product" class="price" colspan="1"> ' + data + ' vnd</td>');

					},
					error: function() {
						alert("Error");
					}
				});
			},
		})
	}

	function removeItem(e) {
		var product_id = $(e).data('id');
		$.ajax({
			url: "{{ route('remove_item_cart') }}",
			method: 'GET',
			data: {
				product_id: product_id,
			},
			success: function(data) {
				console.log('thanh cong');
				$("#cart-summary tbody").empty();
				$("#cart-summary tbody").append(data);
				$.ajax({
					url: "{{ route('reload_total_price') }}",
					method: 'GET',
					success: function(data) {
						$("#total-price").find('td').remove();
						$("#total-price").append('<td class="total-price-container text-right" colspan="3"><span>Total:</span></td>');
						$("#total-price").append('<td id="total-price-container" class="price" colspan="1"> <span id="total-price"> ' + data + ' vnd</span></td>');

						$("#total-price-first").find('td').remove();
						$("#total-price-first").append(' <td class="cart_voucher" colspan="3" rowspan="4"></td>');
						$("#total-price-first").append('   <td class="text-right" colspan="3">Total products</td>');
						$("#total-price-first").append('<td id="total_product" class="price" colspan="1"> ' + data + ' vnd</td>');

					},
					error: function() {
						alert("Error");
					}
				});

			},
		})
	}
</script>
@endsection
