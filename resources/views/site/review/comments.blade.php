@extends('site.layouts.master')
@section('title', 'List Comments')
@php
    use Carbon\Carbon;
@endphp
@section('content')
    <div class="container">
        <div class="row" style="margin:30px 0px" >
            <h2 style="float: left">List Comments</h2>
            <a href="{{ URL::previous() }}" style="float: right;"><h3><i class="fa fa-arrow-left"></i> Back</h3></a>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    @foreach($comments as $comment)
                    <div class="col-md-2">
                        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" style="width: 80px;height: 80px;float:right;margin-top: 15px;" class="img img-rounded img-fluid"/>
                    </div>
                    <div class="col-md-10">
                        <p>
                            <strong>{{ $comment['name'] }}</strong>
                            @php
                            $rate = \App\Entity\Rate::where('product_id',$comment->product_id)->where('user_id',$comment->user_id);
                            @endphp
                            @if($rate->exists())
                            <span style="float: right">
                                 @for($i = 0; $i <= $rate->first()->point_star; $i++)
                                    <i class=" fa fa-star fa-2x" style="color:#FFBA00 "></i>
                                 @endfor
                                 @for($i = 0; $i < (4-$rate->first()->point_star); $i++)
                                     <i class=" fa fa-star fa-2x"></i>
                                 @endfor
                                @endif
                            </span>
                        </p>
                        <div class="clearfix"></div>
                        <p>{{ $comment['content'] }}</p>
                        <i style="float: right">
                           @php
                               $times = Carbon::create($comment['time']);
                               $now = Carbon::now();
                               $time = $times->diffForHumans($now);
                           @endphp
                            {{$time}}
                        </i>
                        <p>
                            @if($comment->user_id == \Illuminate\Support\Facades\Auth::id())
                            <a href="" class=" btn btn-primary btn-sm" data-toggle="modal" data-target="#flipFlop{{ $comment['commentID'] }}">Edit</a>
                            <a href="{{ route('delete.comment',$comment->commentID) }}" class=" btn  btn-danger btn-sm">Delete</a>
                            @endif
                        </p>
                        <div class="modal fade" tabindex="-1" role="dialog" id="flipFlop{{ $comment['commentID'] }}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form action="{{ route('update.comment',$comment['commentID']) }}" method="get">
                                        <div class="modal-body">
                                                <textarea name="textarea2" class="form-control">{{ $comment['content'] }}</textarea>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary" >Update</button>
                                            <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="text-right">{{ $comments->links() }}</div>
@endsection

