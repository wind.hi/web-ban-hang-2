@extends('admin.layout.admin')
@section('group', 'Product')
@section('action', 'Add')
@section('content')

    <!-- Container Fluid-->

    <!-- /.row -->
    <div class="card card-primary card-outline">
        {{--            <form method="post" action="{{route('product.tab.add')}}" enctype="multipart/form-data">--}}
        {{--                @csrf--}}
        <div class="card-header">
            <div class="row">
                <div class="col-md-7">
                    <h5 class="card-title">
                        <i class="fas fa-edit"></i>
                        add product
                    </h5>
                </div>
                <div class="col-md-5 button-add-product">
                    <a href="{{route('product.index')}}" class="btn btn-success btn-icon-split btn-sm">
                         <span class="icon text-white-50">
                        <i class="fa fa-list"></i>
                        </span>
                        <span class="text">List products</span>
                    </a>


{{--                    <button type="submit" class="btn btn-primary btn-icon-split btn-sm"  name="action" value="1" form="add-simple-product">--}}
{{--                            <span class="icon text-white-50">--}}
{{--                            <i class="fas fa-check"></i>--}}
{{--                            </span> <span class="text">Save and continue Edit</span>--}}
{{--                    </button>--}}
                    <button type="button" class="btn btn-primary btn-icon-split btn-sm"  name="action" value="1" onclick="return addProduct(this);">
                            <span class="icon text-white-50">
                            <i class="fas fa-check"></i>
                            </span> <span class="text">Save and continue Edit</span>
                    </button>
                </div>

{{--                <div class="col-md-5 button-add-subproduct">--}}
{{--                    <button type="submit" class="btn btn-primary btn-icon-split btn-sm" form="add-subproduct">--}}
{{--                            <span class="icon text-white-50">--}}
{{--                            <i class="fas fa-check"></i>--}}
{{--                            </span> <span class="text">Save</span>--}}
{{--                    </button>--}}
{{--                </div>--}}
            </div>

        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-5 col-sm-3">
                    <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="tab-add-product" data-toggle="pill" href="#vert-tabs-home" role="tab" aria-controls="vert-tabs-home" aria-selected="true">add product</a>
                        <a class="nav-link" id="tab-validate" data-toggle="pill" href="#vert-tabs-validate" role="tab" aria-controls="vert-tabs-validate" aria-selected="false">add thumbnali image</a>

                        <a class="nav-link" id="tab-add-subproduct" data-toggle="pill" href="#vert-tabs-settings" role="tab" aria-controls="vert-tabs-settings" aria-selected="false">add thumbnail image</a>
                    </div>
                </div>
                <div class="col-7 col-sm-9">
                    <div class="tab-content" id="vert-tabs-tabContent">
                        <div class="tab-pane text-left fade show active" id="vert-tabs-home" role="tabpanel" aria-labelledby="tab-add-product">
                            @include('admin.product.tab_simple_product.add_simple_product')
                        </div>
{{--                        validate --}}
                        <div class="tab-pane text-left " id="vert-tabs-validate" role="tabpanel" aria-labelledby="tab-validate">
                            <p>please điền thông tin product</p>

                        </div>
                        <div class="tab-pane fade" id="vert-tabs-settings" role="tabpanel" aria-labelledby="tab-add-subproduct">
                            @include('admin.product.tab_simple_product.add_thumbimage')
                        </div>
                    </div>
                </div>
            </div>

        </div>

        {{--            table sub product--}}

        <div class="row table-subproduct">

        </div>
        {{--            end table--}}
    </div>



    <!---Container Fluid-->
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $("#tab-add-subproduct").hide();
            $('.formatPrice').priceFormat({
                prefix: '',
                centsLimit: 0,
                thousandsSeparator: '.'
            });
        })
    </script>
    <script>

        function addProduct() {
            var x = 0;
            var price = parseInt($('.pro-price').val().split('.').join(""));
            var discount = parseInt($('.pro-discount').val().split('.').join(""));
            if($('.pro-name').val().trim().length < 1){
                $('#error-name').text('Tên sản phẩm không được để trống !');
                x = 1;
            }else{
                $('#error-name').text('');
            }
            if(parseInt($('.pro-count').val()) < 1 || $('.pro-count').val().length < 1){
                $('#error-count').text('Nhập số lượng sản phẩm !');
            }else{
                $('#error-count').text('');
            }
            if(price < 1 || $('.pro-price').val().length<1){
                $('#error-price').text('Nhập giá sản phẩm !');
                x = 1;
            }else{
                $('#error-price').text('');
            }
            if(discount >= price){
                $('#error-discount').text('Giá khuyến mãi phải nhỏ hơn giá !');
                x = 1;
            }else{
                $('#error-discount').text('');
            }
            if($('.pro-short').val().trim().length < 1){
                $('#error-short').text('Nhập mô tả ngắn !');
                x = 1;
            }else{
                $('#error-short').text('');
            }
            if($('.pro-img').val().length < 1){
                $('#error-img').text('Chọn ảnh sản phẩm !');
                x = 1;
            }else{
                $('#error-img').text('');
            }
            if($('[name="category_id"]').val().length < 1){
                $('#error-cate').text('Chọn danh mục sản phẩm !');
                x = 1;
            }else{
                $('#error-cate').text('');
            }

            if(x == 0){
                var frm = $('#add-simple-product');
                var form = $('form#add-simple-product')[0];
                var formData = new FormData(form);
                formData.append('description', CKEDITOR.instances['editor1'].getData());
                $.ajax({
                        method: frm.attr('method'),
                        url: frm.attr('action'),
                        data: formData,
                        mimeType: "multipart/form-data",
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            console.log(data)
                            $("#tab-add-subproduct").show();
                            $("#tab-add-product").removeClass('active');
                            $("#tab-add-subproduct").addClass('active');
                            $("div#vert-tabs-settings").addClass('fade active show');
                            $("div#vert-tabs-home").removeClass('fade active show');
                            $("#tab-validate").hide();
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            })
                            Toast.fire({
                                icon: "success",
                                title: "add product success"
                            })
                        }
                    },
                );
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#pick-images").change(function() {
                $('.image-list').show();
                $('.image-list').empty();
                $('.image-pics').empty();

                var reader = new FileReader();
                reader.onload = function(event) {
                    $($.parseHTML('<div class="image-pic"><img width="125px" height="125px" src="'+event.target.result+'" style="margin-right: 10px" ><div class="remove"><a href="#"><i class="fas fa-times-circle "></i></a></div></div>')).appendTo('.image-pics');
                    $(".remove").click(function(){
                        $(this).parent(".image-pic").remove();
                        // var fileName = $(this).parent(".image-pic img").attr('title');
                        // console.log(fileName);
                        $('#pick-images').val('');

                    });
                }
                reader.readAsDataURL(this.files[0]);
            });

        })
    </script>
    <script>
        $(document).ready(function () {
            $('#cate-tree').combotree('loadData', {!! json_encode(\App\Entity\Category::getSubCategory($categories,0)) !!});
            $("#cate-tree").combotree({
                onClick: function(){
                    var tree =  $("#cate-tree").combotree('tree');	// get the tree object
                    var node = tree.tree('getSelected');
                    var id = node.id;
                    var text = node.text;
                    var child = node.children;
                    if(child.length > 0){
                        alert('Vui lòng chọn một danh mục thuộc danh mục '+node.text);
                        $("#cate-tree").combotree('clear');
                        $("input[name='category_id']").val('');
                        $(".line-category").empty();
                        return;
                    }
                    changeCate(id,text);
                    $("input[name='category_id']").val(id);
                },
            });
        });

    </script>
    <script>
        function changeCate(cateId,text) {
            var url = "{{ route('line.cate', "cateId") }}";
            url = url.replace('cateId', cateId);
            $.ajax({
                    url:url,
                    method:'GET',
                    success: function (data) {

                        // $("#notifiSuccess").modal('show');
                        $(".line-category").empty();
                        $(".line-category").html(data);
                        getCateSelected(text);
                        console.log(data);
                    },
                    error: function(error) {
                    }
                },
            );
        }
        function getCateSelected(cate) {
            $(".line-category").append(' <span> '+cate+' </span>');
        }
    </script>
    <script type="text/javascript">
        $("#choose-att").click(function() {
            var attribute_id = $('.select-attribute option:selected').val();

            var label_att = $(".label-att");
            for ($i = 0; $i < label_att.length; $i++) {
                if ($('.select-attribute option:selected').text() == label_att[$i].value) {
                    return;
                }
            }
            $.ajax({
                url: "{{ route('get.att') }}",
                method: 'GET',
                data: {
                    attribute_id: attribute_id
                },
                success: function(data) {
                    $('.add-attribute').append(data);
                    // $("#notifiSuccess").modal('show');
                    $('.select2').select2()

                    //Initialize Select2 Elements
                    $('.select2bs4').select2({
                        theme: 'bootstrap4'
                    })
                },
                error: function(error) {}
            }, );
        });

    </script>
    <script>
        $(document).ready(function () {
            $(".button-add-subproduct").hide();
        });
        $("a#tab-add-product").click(function () {
            $(".button-add-product").show();
            $(".button-add-subproduct").hide();
        });
        $("a#tab-add-subproduct").click(function () {
            $(".button-add-product").hide();
            $(".button-add-subproduct").show();
        })
    </script>
    <script>
        function addSubProduct() {
            var names = [];
            var attribute = $(".select-att");
            for (var i = 0; i < attribute.length; i++)
            {

                names.push(attribute[i].value);
            }
            var quantity = $("input[name='quantity_sub']").val();
            var price = $("input[name='price_sub']").val();
            $.ajax({
                    url:"{{route('product.ajax.addSubProduct')}}",
                    method:'GET',
                    data: {
                        quantity: quantity,
                        price: price,
                        attribute: names,
                    },
                    success: function (data) {
                        $(".table-subproduct").html(data);
                        console.log(data);
                    },
                    error: function(error) {
                        console.log(error);
                    }
                },
            );

        }
    </script>

@endpush



