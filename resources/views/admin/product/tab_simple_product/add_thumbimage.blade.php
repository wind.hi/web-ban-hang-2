
        <div class="container">
            <div class="row">
                    <div class="col-md-1">

                    </div>
                <div class="col-md-10">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group">
                        <input id="input-705" name="file" type="file" accept="image/*" multiple>
                    </div>
                </div>

                <div class="col-md-1">

                </div>




            </div>
        </div>



@push('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/js/plugins/piexif.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/js/plugins/purify.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js"></script>


    <script>
            $(document).ready(function() {
                var $el1 = $("#input-705");
                $el1.fileinput({
                    theme: 'fa',
                    allowedFileExtensions: ['jpg', 'png', 'gif'],
                    uploadUrl: "/admin/add-images-product",
                    uploadExtraData:function(){
                        return{
                            _token:$("input[name='_token']").val()
                        };
                    },
                    uploadAsync: true,
                    // deleteUrl: "/site/file-delete",
                    // showUpload: false, // hide upload button
                    // showRemove: false, // hide remove button
                    overwriteInitial: false, // append files to initial preview
                    minFileCount: 1,
                    maxFileCount: 5,
                    // initialPreviewAsData: true,
                });
            });
        </script>

@endpush



