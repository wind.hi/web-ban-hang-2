

    <!-- Container Fluid-->
    <div class="container-fluid" id="container-wrapper">
        {{--{{dd(\App\Entity\Category::getSubCategory($categories,0))}}--}}
        <form method="post" action="{{route('product.tab.add.SimpleProduct')}}" enctype="multipart/form-data" id="add-simple-product">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Basic -->
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Nhập thông tin</h6>
                        </div>
                        <div class="card-body">

                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên sản phẩm</label>
                                <input type="text" class="form-control pro-name" name="name" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" value="{{old('name')}}">
                                    <span id="error-name" class="error-text">
                                    </span>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Số lượng</label>

                                <input type="text" class="form-control formatPrice pro-count" id="exampleInputEmail1" placeholder="nhập tên danh mục" name="quantity"  value="{{old('quantity')}}">
                                    <span id="error-count" class="error-text">
                                    </span>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Đơn giá</label>

                                <input type="text" class="form-control formatPrice pro-price" id="stepProductCreate3" placeholder="nhập tên danh mục" name="price" min="1" value="{{old('price')}}">
                                    <span id="error-price" class="error-text">
                                    </span>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Giá khuyến mãi</label>
                                <input type="text" class="form-control formatPrice pro-discount" name="discount" placeholder="Giá khuyến mãi" min="1" id="stepProductCreate3">
                                <span id="error-discount" class="error-text">
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Chọn danh mục </h6>
                        </div>
                        <div class="card-body">
                            <div class="form-group">

                                <input class="easyui-combotree" id="cate-tree" style="width:100%">
                                <input type="hidden" class="" name="category_id" >

                            </div>
                            <span id="error-cate" class="error-text">
                            </span>
                            <div class="form-group line-category">

                            </div>
                        </div>
                    </div>

                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Thêm thông tin</h6>
                        </div>

                        <div class="card-body">
                            <div class="form-group">
                                <label>Mô tả ngắn</label>
                                <textarea name="short_description" class="form-control editor pro-short">{{old('short_description')}}</textarea>
                                    <span id="error-short" class="error-text">
                                    </span>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="form-group">
                                <label>Mô tả</label>
                                <textarea name="description" class="form-control editor" id="editor1">{{old('description')}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Chọn Ảnh Sản phẩm</h6>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputFile">Ảnh SP</label>
                                <div class="input-group" style="">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input pro-img" id="pick-images" name="image" >
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                                    <span id="error-img" class="error-text">
                                    </span>
                            </div>
                            <div class="form-group image-list">

                            </div>
                            <style>
                                .image-pic {
                                    position: relative;
                                    display: inline-block;
                                }
                                .image-pic:hover .remove {
                                    display: block;
                                }

                                .remove {
                                    padding-right: 7px;
                                    position: absolute;
                                    right: 0;
                                    top: 0;
                                    display: none;
                                }
                            </style>
                            <div class="image-pics">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!--Row-->

        <!-- Documentation Link -->
    </div>





