@extends('admin.layout.admin')


@section('title', 'Danh sách sản phẩm con')

@section('content')

    <div class="container-fluid" id="container-wrapper">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">

        </div>
        <!-- Row -->
        <div class="row">
            <!-- Datatables -->
            <div class="col-lg-12">
                <div class="card mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">DataTables</h6>
                    </div>
                    <div class="table-responsive p-3">
                        <table class="table align-items-center table-flush" id="dataTable">
                            <thead class="thead-light">
                            <tr>
                                <th>Sản phẩm</th>
                                <th>Danh mục</th>
                                <th>Hình ảnh</th>
                                <th>Giá tiền</th>
                                <th>Giá khuyến mại</th>
                                <th>Số lượng</th>
                                <th>thao tác</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Sản phẩm</th>
                                <th>Danh mục</th>
                                <th>Hình ảnh</th>
                                <th>Giá tiền</th>
                                <th>Giá khuyến mại</th>
                                <th>Số lượng</th>
                                <th>thao tác</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <tr>
                                @foreach($product_option as $option)
                                <td>{{$option->name}}</td>
                                <td>System Architect</td>
                                <td>Edinburgh</td>
                                <td>{{$option->price}}</td>
                                <td>2011/04/25</td>
                                <td>$320,800</td>
                                @endforeach
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!--Row-->
        <!-- Documentation Link -->

    </div>
@endsection
