@extends('admin.layout.admin')
@section('title', 'Thêm sản phẩm')
@section('group', 'Product')
@section('action', 'Add')
@section('content')

    <!-- Container Fluid-->
    <div class="container-fluid" id="container-wrapper">
        <div class="row">
            <div class="col-lg-3">

            </div>
            <div class="col-lg-6">
                <!-- Dropdown Basics -->
                <div class="card mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Choose type Product</h6>
                    </div>
                    <div class="card-body">
                        <div class="dropdown">
                            <a class="btn btn-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">select below here
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="{{route('product.add.SimpleProduct')}}">Simple Product</a>
                                <a class="dropdown-item" href="{{route('product.add.exProduct')}}">Extend Products</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
        </div>
        </div>
    </div>
    <!---Container Fluid-->
@endsection

@push('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$(".amsify-select-operations").hide();
		$('.formatPrice').priceFormat({
			prefix: '',
			centsLimit: 0,
			thousandsSeparator: '.'
		});
		$(".date-sale").hide();
		$("#stepProductCreate4").keyup(function() {
			$(".date-sale").show();
			if ($("#stepProductCreate4").val() == 0) {
				$(".date-sale").hide();
			}
		});
		$('.image-list').hide();
		$("#pick-images").change(function() {
			$('.image-list').show();
			$('.image-list').empty();
			var names = [];
			for (var i = 0; i < $(this).get(0).files.length; ++i) {
				names.push($(this).get(0).files[i].name);

                    var reader = new FileReader();
                    reader.onload = function(event) {
                        $($.parseHTML('<img width="85px" height="85px" style="margin-right: 10px" >')).attr('src', event.target.result).appendTo('.image-list');
                    }

                    reader.readAsDataURL(this.files[i]);

			}
			for (i in names) {
				$('.image-list').append(
                    '<div class="custom-control custom-radio">\n' +
					'                                    <input type="radio" id="' + names[i] + '" value="' + names[i] + '"name="customRadio" class="custom-control-input" >\n' +
					'                                    <label class="custom-control-label" for="' + names[i] + '">' + names[i] + '</label>\n' +
					'                                </div>');
			}
		});
	});
    function readIMG(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
	//Initialize Select2 Elements
	$('.select2').select2()

	//Initialize Select2 Elements
	$('.select2bs4').select2({
		theme: 'bootstrap4'
	})

</script>
<script type="text/javascript">
	$("#choose-att").click(function() {
		var attribute_id = $('.select-attribute option:selected').val();

		var label_att = $(".label-att");
		for ($i = 0; $i < label_att.length; $i++) {
			if ($('.select-attribute option:selected').text() == label_att[$i].value) {
				return;
			}
		}
		$.ajax({
			url: "{{ route('get.att') }}",
			method: 'GET',
			data: {
				attribute_id: attribute_id
			},
			success: function(data) {
				$('.add-attribute').append(data);
				// $("#notifiSuccess").modal('show');
				$('.select2').select2()

				//Initialize Select2 Elements
				$('.select2bs4').select2({
					theme: 'bootstrap4'
				})
			},
			error: function(error) {}
		}, );
	});

</script>

<script>
	$('.select-parent').on('change', function(e) {
		var value = $('.select-parent option:selected').val();
		$.ajax({
			url: "{{ route('get.child.cate' )}}",
			method: 'GET',
			data: {
				cateID: value,
			},
			success: function(data) {
				console.log(data);
				var $subCate = $('.select-child').empty();

				$subCate.append('<option value="" selected>-------chọn danh mục--------</option>');

				for (i in data) {
					$subCate.append('<option value = ' + data[i].id + '>' + data[i].title + '</option>');
				}
			},
			error: function(error) {
				console.log(error);
			}

		});
	});
	$('.select-child').change(function(e) {
		var title = $(".select-child option:selected").text();
		$('.note-cate').find('span').remove();
		$('.note-cate').find('i').remove();
		$(".note-cate").append('<i class="fas fa-angle-right"></i><span> ' + title + '</span>');
		if (title === '-------chọn danh mục--------') {
			$(".note-cate").find('span').remove();
		}
	});

</script>

<script>
    $(document).ready(function () {
        $('#cate-tree').combotree('loadData', {!! json_encode(\App\Entity\Category::getSubCategory($categories,0)) !!});
        $("#cate-tree").combotree({
            onClick: function(){
                var t =  $("#cate-tree").combotree('tree');	// get the tree object
                var n = t.tree('getSelected');
                var id = n.id;
                $("input[name='category_id']").val(id);
            },
        });
    });
</script>
<script>


</script>




