@extends('admin.layout.admin')


@section('title', 'Danh sách sản phẩm')

@section('content')

    <div class="container-fluid" id="container-wrapper">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">

        </div>
        <!-- Row -->
        <div class="row">
            <!-- Datatables -->
            <!-- DataTable with Hover -->
            <div class="col-lg-12">
                <div class="card mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">DataTables with Hover</h6>
                    </div>
                    <div class="table-responsive p-3">
                        <div id="dataTableHover_wrapper" class="dataTables_wrapper dt-bootstrap4">

                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table align-items-center table-flush table-hover dataTable" id="products" role="grid" aria-describedby="dataTableHover_info">
                                        <thead class="thead-light">
                                        <tr role="row">
                                            <th>Sản phẩm</th>
                                            <th>Danh mục</th>
                                            <th>Hình ảnh</th>
                                            <th>Giá tiền</th>
                                            <th>Trạng thái</th>
                                            <th>Số lượng</th>
                                            <th>Thao tác</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Row-->
        <!-- Documentation Link -->

    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(function() {
            $('#products').DataTable({
                pageLength: 6,
                type: 'GET',
                ajax: '{{route('dt.product')}}{!! isset($_GET['vendor_id']) ? '?vendor_id='.$_GET['vendor_id'] : '' !!}',
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'category', name: 'category' },
                    { data: 'image', name: 'image', render:function (data) {
                            return '<img src="{{ asset('frontend/img/product/') }}' + '/' + data + '" border="0" width="60" class="img-rounded" align="center" />'
                        } },
                    { data: 'price', name: 'price',render:function (price) {
                            return number_format(price,'0',',','.')
                        } },
                    { data: 'status', name: 'status',render:function (status) {
                            if(status == 0){
                                return "chưa duyệt";
                            }
                            return "đã duyệt";
                        } },
                    { data: 'quantity', name: 'quantity' , render:function (quantity) {
                            return number_format(quantity,'0',',','.')
                        }},
                    { data: 'action', name: 'action', searchable: false, orderable: false }
                ],
                order : [
                    [ 0, 'desc' ]
                ]
            });
        });
    </script>
@endpush
