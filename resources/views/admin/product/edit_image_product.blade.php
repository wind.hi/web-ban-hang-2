@extends('admin.layout.admin')
@section('title', 'update image product')
@section('group', 'Product')
@section('action', 'Add')
@section('content')

    <div class="container-fluid" id="container-wrapper">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="./">Home</a></li>
                <li class="breadcrumb-item">Tables</li>
                <li class="breadcrumb-item active" aria-current="page">Simple Tables</li>
            </ol>
        </div>

        <div class="row">
            <div class="col-lg-12 mb-4">
                <!-- Simple Tables -->
                <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Sản phẩm: {{$product->name}}</h6>
                    </div>
                    <div class="table-responsive">
                        <table id="list-image" class="table align-items-center table-flush">
                            @if (Session::has('status'))
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                {{Session::get('status')}}
                            </div>
                            @endif
                            <thead class="thead-light">
                            <tr>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($productImages as $image)
                                <tr>
                                    <td><img src="{{asset('frontend/img/product/'.$image->image)}}" width="75px" height="75px" title="{{$image->image}}"></td>
                                    @if($image->status == 1)
                                    <td><span class="badge badge-success">image avatar</span></td>
                                    @else
                                        <td><span class="badge badge-info">image thumbnail</span></td>
                                    @endif
                                    <td>
                                        <div class="btn-group mb-1">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                action
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" data-toggle="modal" data-target="#editImage" data-imgid="{{$image->image_id}}" onclick="return getImageProduct(this);">Edit</a>
                                                <a class="dropdown-item" href="{{route('delete.image.product',$image->image_id)}}">Delete</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- Image Js-->
                        <table id="image-js" class="table align-items-center table-flush">
                            <tbody>
                            <tr>
                                <td id="image-js-open"><img src="" width="75px" height="75px" title=""></td>
                                @if($image->status == 1)
                                <td><span class="badge badge-success">image avatar</span></td>
                                @else
                                    <td><span class="badge badge-info">image thumbnail</span></td>
                                @endif
                                <td>
                                    <div class="btn-group mb-1">
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            action
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" data-toggle="modal" data-target="#editImage" data-imgid="{{$image->image_id}}" onclick="return getImageProduct(this);">Edit</a>
                                            <a class="dropdown-item" href="{{route('delete.image.product',$image->image_id)}}">Delete</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <form action="{{route('POSTedit.image.product')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="file" id="add-image" name="avatar"  />
                                <input type="hidden" name="product_id" value={{$product->id}}>
                                <p style="text-align: right; margin-top: 20px;">
                                    <input type="submit" value="upload Files" class="btn btn-primary" />
                                </p>

                            </form>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
            </div>
        </div>
        <!--Row-->
    </div>

    {{-- modal edit image product--}}

    {{--    end modal--}}
@endsection
@push('scripts')

<script>
    function getImageProduct(e) {
        var imageId = $(e).data('imgid');
        $.ajax({
                url:'{{route('get.detail.image')}}',
                method:'GET',
                data:{
                    image_id:imageId,
                },
                success: function (data) {
                    $('#modal-content2').empty();
                    $('#modal-content2').html(data);
                    $('.image-list').empty();
                    $('#pick-image').val('');
                },
                error: function(error) {
                }
            },
        );
    }
</script>
    <script>
        $(document).ready(function () {
            $('.image-list').hide();
            $("#pick-image").change(function() {
                $('.image-list').show();
                $('.image-list').empty();
                var reader = new FileReader();
                    reader.onload = function(event) {
                        $($.parseHTML('<img width="85px" height="85px" style="margin-right: 10px" >')).attr('src', event.target.result).appendTo('.image-list');
                    };
                    reader.readAsDataURL(this.files[0]);
                    $('.image-list').append('<p>'+$(this).get(0).files[0].name+'</p>');
            });
        })
    </script>
{{--    <script>--}}
{{--        function updateImageProduct() {--}}
{{--                var image_id = $("#image_id").val();--}}
{{--                var status = $("#status_image option:selected").val();--}}
{{--                console.log(image_id);--}}
{{--                console.log(status);--}}
{{--        }--}}
{{--    </script>--}}
<link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />
<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>

<script type="text/javascript">

    // function applyUpFile(){
        $("#add-image").change(function() {
            var reader = new FileReader();
            reader.onload = function(event) {
                $($.parseHTML('<img width="75px" height="75px" style="margin-right: 10px" >')).attr('src', event.target.result).appendTo('table#image-js td#image-js-open');
            };
            reader.readAsDataURL(this.files[0]);
            $("table#image-js tbody").append('<tr>' +
                    '<td>thumbline</td>'+
                '</tr>')
        });

    // }

</script>
@endpush



