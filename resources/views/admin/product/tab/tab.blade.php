@extends('admin.layout.admin')
@section('group', 'Product')
@section('action', 'Add')
@section('content')

    <!-- Container Fluid-->

        <!-- /.row -->
        <div class="card card-primary card-outline">
{{--            <form method="post" action="{{route('product.tab.add')}}" enctype="multipart/form-data">--}}
{{--                @csrf--}}
            <div class="card-header">
                <div class="row">
                    <div class="col-md-7">
                        <h5 class="card-title">
                            <i class="fas fa-edit"></i>
                            add product
                        </h5>
                    </div>
                    <div class="col-md-5 button-add-product">
                        <a href="{{route('product.index')}}" class="btn btn-success btn-icon-split btn-sm">
                         <span class="icon text-white-50">
                        <i class="fa fa-list"></i>
                        </span>
                           <span class="text">List products</span>
                        </a>

{{--                        <button type="submit" class="btn btn-primary btn-icon-split btn-sm"  name="action" value="1" form="add-product">--}}
{{--                            <span class="icon text-white-50">--}}
{{--                            <i class="fas fa-check"></i>--}}
{{--                            </span> <span class="text">Save and continue Edit</span>--}}
{{--                        </button>--}}
                        <button type="button" class="btn btn-primary btn-icon-split btn-sm"  name="action" value="1" onclick="return addProduct(this);">
                            <span class="icon text-white-50">
                            <i class="fas fa-check"></i>
                            </span> <span class="text">Save and continue Edit</span>
                        </button>
                    </div>
                    <div class="col-md-5 button-add-subproduct">
                        <div class="row">
                            <div class="col-md-3">

                            </div>
                            <div class="col-md-3">

                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary btn-icon-split btn-sm" form="add-subproduct">
                            <span class="icon text-white-50">
                            <i class="fas fa-check"></i>
                            </span> <span class="text">save and finish</span>
                                </button>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-5 col-sm-3">
                        <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="tab-add-product" data-toggle="pill" href="#vert-tabs-home" role="tab" aria-controls="vert-tabs-home" aria-selected="true">add product</a>
{{--                                 // validate 1--}}
                            <a class="nav-link" id="tab-validate" data-toggle="pill" href="#vert-tabs-validate" role="tab" aria-controls="vert-tabs-validate" aria-selected="false">add thumbnali image</a>
{{--                                // validate 2--}}
                            <a class="nav-link" id="tab-validate2" data-toggle="pill" href="#vert-tabs-validate2" role="tab" aria-controls="vert-tabs-validate2" aria-selected="false">add extend products</a>

                            <a class="nav-link " id="tab-add-thumbnali" data-toggle="pill" href="#vert-tabs-settings2" role="tab" aria-controls="vert-tabs-settings2" aria-selected="false">add thumbnali image</a>
                            <a class="nav-link" id="tab-add-subproduct" data-toggle="pill" href="#vert-tabs-settings" role="tab" aria-controls="vert-tabs-settings" aria-selected="false">add extend products</a>

                        </div>
                    </div>
                    <div class="col-7 col-sm-9">
                        <div class="tab-content" id="vert-tabs-tabContent">

                            <div class="tab-pane text-left fade show active" id="vert-tabs-home" role="tabpanel" aria-labelledby="tab-add-product">

                                @include('admin.product.tab.add')

                            </div>
{{--                            validate 1---}}
                            <div class="tab-pane text-left " id="vert-tabs-validate" role="tabpanel" aria-labelledby="tab-validate">
                                <p>please điền thông tin product</p>

                            </div>
{{--                            validate 2--}}
                            <div class="tab-pane text-left " id="vert-tabs-validate2" role="tabpanel" aria-labelledby="tab-validate2">
                                <p>please điền thông tin product</p>
                            </div>

                            <div class="tab-pane text-left fade " id="vert-tabs-settings2" role="tabpanel" aria-labelledby="tab-add-thumbnali">

                                @include('admin.product.tab.add_thumbimage')

                            </div>
                            <div class="tab-pane fade" id="vert-tabs-settings" role="tabpanel" aria-labelledby="tab-add-subproduct">

                                @include('admin.product.tab.add_sub_product')

                            </div>
                        </div>
                    </div>
                </div>

            </div>

{{--            table sub product--}}

            <div class="row table-subproduct">

            </div>
{{--            end table--}}
        </div>
    <!---Container Fluid-->
@endsection
@push('scripts')
    <script type="text/javascript">
        // check from
        $(document).ready(function () {
            // $("#pick-images").change(function() {
            //     $('.image-list').show();
            //     $('.image-list').empty();
            //     $('.image-pics').empty();
            //     var names = [];
            //     for (var i = 0; i < $(this).get(0).files.length; ++i) {
            //         names.push($(this).get(0).files[i].name);
            //         var reader = new FileReader();
            //         var f = this.files[i];
            //         reader.onload = function(event) {
            //             $($.parseHTML('<div class="image-pic"><img width="85px" height="85px" src="'+event.target.result+'" style="margin-right: 10px" title="'+f.name+'"><div class="remove"><a href="#"><i class="fas fa-times-circle "></i></a></div></div>')).appendTo('.image-pics');
            //             $(".remove").click(function(){
            //                 // $(this).parent(".image-pic").remove();
            //                 var fileName = $(this).parent(".image-pic img").attr('title');
            //                 console.log(fileName);
            //                 // src = $(this).parent(".image-pic img").val();
            //                 // $('#pick-images').val(event.target.name);
            //
            //             });
            //         }
            //         reader.readAsDataURL(this.files[i]);
            //     }
            //     for (i in names) {
            //         $('.image-list').append(
            //             '<div class="custom-control custom-radio">\n' +
            //
            //             '                                    <input type="radio" id="' + names[i] + '" value="' + names[i] + '"name="customRadio" class="custom-control-input" >\n' +
            //             '                                    <label class="custom-control-label" for="' + names[i] + '">' + names[i] + '</label>\n' +
            //             '                                </div>'
            //         );
            //     }
            // });
            $('.formatPrice').priceFormat({
                prefix: '',
                centsLimit: 0,
                thousandsSeparator: '.'
            });
            $("#pick-images").change(function() {
                $('.image-list').show();
                $('.image-list').empty();
                $('.image-pics').empty();
                $('#image-avatar').val($(this).get(0).files[0].name);
                var reader = new FileReader();
                reader.onload = function(event) {
                    $($.parseHTML('<div class="image-pic"><img width="125px" height="125px" src="'+event.target.result+'" style="margin-right: 10px" ><div class="remove"><a href="#"><i class="fas fa-times-circle "></i></a></div></div>')).appendTo('.image-pics');
                    $(".remove").click(function(){
                        $(this).parent(".image-pic").remove();
                        // var fileName = $(this).parent(".image-pic img").attr('title');
                        // console.log(fileName);
                        $('#pick-images').val('');

                    });
                }
                reader.readAsDataURL(this.files[0]);
            });
        })
    </script>
<script>
    $(document).ready(function () {
        $('#cate-tree').combotree('loadData', {!! json_encode(\App\Entity\Category::getSubCategory($categories,0)) !!});
        $("#cate-tree").combotree({
            onClick: function(){
                var tree =  $("#cate-tree").combotree('tree');	// get the tree object
                var node = tree.tree('getSelected');
                var id = node.id;
                var text = node.text;
                var child = node.children;
                if(child.length > 0){
                    alert('Vui lòng chọn một danh mục thuộc danh mục '+node.text);
                    $("#cate-tree").combotree('clear');
                    $("input[name='category_id']").val('');
                    $(".line-category").empty();
                    return;
                }
                changeCate(id,text);
                $("input[name='category_id']").val(id);
            },
        });
    });

</script>
    <script>
        function changeCate(cateId,text) {
            var url = "{{ route('line.cate', "cateId") }}";
            url = url.replace('cateId', cateId);
            $.ajax({
                    url:url,
                    method:'GET',
                    success: function (data) {

                        // $("#notifiSuccess").modal('show');
                        $(".line-category").empty();
                        $(".line-category").html(data);
                        getCateSelected(text);
                        console.log(data);
                    },
                    error: function(error) {
                    }
                },
            );
        }
        function getCateSelected(cate) {
            $(".line-category").append(' <span> '+cate+' </span>');
        }
    </script>
    <script type="text/javascript">
        $("#choose-att").click(function() {
            var attribute_id = $('.select-attribute option:selected').val();

            var label_att = $(".label-att");
            for ($i = 0; $i < label_att.length; $i++) {
                if ($('.select-attribute option:selected').text() == label_att[$i].value) {
                    return;
                }
            }
            $.ajax({
                url: "{{ route('get.att') }}",
                method: 'GET',
                data: {
                    attribute_id: attribute_id
                },
                success: function(data) {
                    $('.add-attribute').append(data);
                    // $("#notifiSuccess").modal('show');
                    $('.select2').select2()

                    //Initialize Select2 Elements
                    $('.select2bs4').select2({
                        theme: 'bootstrap4'
                    })
                },
                error: function(error) {}
            }, );
        });

    </script>
    <script>
        $(document).ready(function () {
            $(".button-add-subproduct").hide();
            $("#tab-add-thumbnali").hide();
            $("#loading").hide();
            $("#tab-add-subproduct").hide();
        });
        $("a#tab-add-product").click(function () {
            $(".button-add-product").show();
            $(".button-add-subproduct").hide();
        });
        $("a#tab-add-subproduct").click(function () {
            $(".button-add-product").hide();
            $(".button-add-subproduct").show();
        })
        $("a#tab-add-thumbnali").click(function () {
            $(".button-add-product").hide();
            $(".button-add-subproduct").hide();
        })
    </script>
    <script>
        {{--$(document).ready(function () {--}}
        {{--    var frm = $('#add-product');--}}
        {{--    var formData = new FormData($("#add-product"));--}}
        {{--    $("#add-product").on('submit', function(e) {--}}
        {{--        // e.preventDefault();--}}
        {{--        // $.ajax({--}}
        {{--        //     type: frm.attr('method'),--}}
        {{--        //     url: frm.attr('action'),--}}
        {{--        //     data: formData,--}}
        {{--        //     success: function (data) {--}}
        {{--        //         $("#tab-add-thumbnali").show();--}}
        {{--        //         $("#tab-validate").hide();--}}
        {{--        //     }--}}
        {{--        // });--}}
        {{--        e.preventDefault(); // Now nothing will happen--}}
        {{--                $("#tab-add-thumbnali").show();--}}
        {{--                $("#tab-validate").hide();--}}
        {{--        --}}{{--// $("a.thumbnail-image").attr('id','tab-add-thumbnali');--}}
        {{--        --}}{{--// $("a.thumbnail-image").attr('href','#vert-tabs-settings2');--}}
        {{--        --}}{{--// $("a.thumbnail-image").attr('aria-controls','#vert-tabs-settings2');--}}
        {{--        $("#add-product").off("submit");--}}
        {{--        $(this).submit();--}}
        {{--    });--}}
        {{--})--}}
    </script>
    <script>
        function addProduct() {
            var frm = $('#add-product');
            var form = $('form#add-product')[0];
            var formData = new FormData(form);
            formData.append('description', CKEDITOR.instances['editor1'].getData());
            $.ajax({
                    method: frm.attr('method'),
                    url: frm.attr('action'),
                    data: formData,
                    mimeType: "multipart/form-data",
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $("#name_parent_product").val(data);
                        $("#tab-add-thumbnali").show();
                        $("#tab-add-subproduct").show();
                        $("#tab-add-product").removeClass('active');
                        $("#tab-add-thumbnali").addClass('active');
                        $("div#vert-tabs-settings2").addClass('fade active show');
                        $("div#vert-tabs-home").removeClass('fade active show');
                        $(".button-add-product").hide();
                        $(".button-add-subproduct").hide();
                        $("#tab-validate").hide();
                        $("#tab-validate2").hide();
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000
                        })
                        Toast.fire({
                            icon: "success",
                            title: "add product success"
                        })
                            }
                        },
                    );
            console.log(data);
            }


    </script>
    <script>
        function addSubProduct() {
            var names = [];
            var attribute = $(".select-att");
            for (var i = 0; i < attribute.length; i++)
            {
                names.push(attribute[i].value);
            }
           var quantity = $("input[name='quantity_sub']").val();
           var price = $("input[name='price_sub']").val();
           var discount = $("input[name='discount_sub']").val();
            $.ajax({
                    url:"{{route('product.ajax.addSubProduct')}}",
                    method:'GET',
                    data: {
                        quantity: quantity,
                        price: price,
                        discount: discount,
                        attribute: names,
                    },
                    success: function (data) {
                        $(".table-subproduct").html(data);
                        console.log(data);
                    },
                    error: function(error) {
                        console.log(error);
                    }
                },
            );

        }
    </script>
    <script>
        function clearSubProductTable() {
            $.ajax({
                    url:"{{route('product.ajax.clearSubProduct')}}",
                    method:'GET',
                    success: function (data) {
                        $(".table-subproduct").html(data);
                        console.log(data);

                    },
                    error: function(error) {
                        console.log(error);
                    }
                },
            );

        }
    </script>
@endpush



