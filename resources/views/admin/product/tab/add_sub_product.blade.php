<form method="post" action="{{route('product.tab.addSubProduct')}}" enctype="multipart/form-data" id="add-subproduct">
    @csrf
<div class="row">
    <div class="col-lg-12">
        <!-- Form Basic -->
        <div class="card mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Nhập thông tin</h6>
            </div>
            <div class="card-body">

                <div class="form-group">
                    <label for="exampleInputEmail1">Tên sản phẩm</label>
                    <input type="text" class="form-control" name="name" id="name_parent_product" aria-describedby="emailHelp" placeholder="" value="{{old('name')}}" readonly>
                </div>

                <div class="form-group">
                    <select class="form-control select-attribute" id="exampleFormControlSelect1">
                        <option selected>--------- Chọn thuộc tính ---------</option>
                        @foreach($attributes as $att)
                                <option value="{{$att->id}}">{{$att->name}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="button" class="btn btn-primary mb-1" id="choose-att" onclick="return getAttribute(this);">
                    Thêm
                </button>

                <div class="add-attribute">

                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Số lượng</label>
                    <input type="text" class="form-control formatPrice" id="soluong" placeholder="nhập tên danh mục" name="quantity_sub" required="">
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Đơn giá</label>
                    <input type="text" class="form-control formatPrice" name="price_sub" placeholder="Giá khuyến mãi" min="1" id="dongia" required="">

                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Giá khuyến mãi</label>
                    <input type="text" class="form-control formatPrice" name="discount_sub" placeholder="Giá khuyến mãi" min="1" id="khuyenmai" required="">

                </div>

            </div>
            <button type="button" class="btn btn-primary mb-1" id="add-to-subproduct" onclick="return addSubProduct(this);">
                quick create
            </button>
        </div>
    </div>
</div>
</form>
@push('scripts')
    <script type="text/javascript">
        function addSubProduct() {
            var soluong = document.getElementById("soluong").value;;
            var dongia = document.getElementById("dongia").value;;
            var khuyenmai = document.getElementById("khuyenmai").value;;
            if(soluong == ""){alert("Vui lòng nhập số lượng sản phẩm");}
            // check đơn giá
            if(dongia == ""){alert("Vui lòng nhập giá cho sản phẩm");}
            //check khuyến mãi
            if(khuyenmai == 0){alert("Vui lòng nhập giá khuyến mãi nếu không thì hãy nhập là 0");}
        }
    </script>
@endpush




