
    <!-- Container Fluid-->

        {{--{{dd(\App\Entity\Category::getSubCategory($categories,0))}}--}}
{{--        <form method="post" action="{{route('product.store')}}" enctype="multipart/form-data">--}}
{{--            {{csrf_field()}}--}}
        <form method="post" action="{{route('product.tab.add')}}" enctype="multipart/form-data" id="add-product">
                @csrf
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Basic -->
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Nhập thông tin</h6>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên sản phẩm</label>
                                <input type="text" class="form-control" name="name" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" value="{{old('name')}}">
                            </div>

                        </div>
                    </div>
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Chọn danh mục </h6>
                        </div>
                        <div class="card-body">
                            <div class="form-group">

                                <input class="easyui-combotree" id="cate-tree" style="width:100%">
                                <input type="hidden" name="category_id" >

                            </div>
                            <div class="form-group line-category">

                            </div>
                        </div>
                    </div>

                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Thêm thông tin</h6>
                        </div>

                        <div class="card-body">
                            <div class="form-group">
                                <label>Mô tả ngắn</label>
                                <textarea name="short_description" class="form-control editor"> {{old('short_description')}}</textarea>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="form-group">
                                <label>Mô tả</label>
                                <textarea name="description" class="form-control editor" id="editor1">{{old('description')}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Chọn Ảnh Sản phẩm</h6>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputFile">Ảnh SP</label>
                                <div class="input-group" style="">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="pick-images" name="image">
                                        <input type="hidden" name="image_avatar" id="image-avatar">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group image-list">

                            </div>
                            <style>
                                .image-pic {
                                    position: relative;
                                    display: inline-block;
                                }
                                .image-pic:hover .remove {
                                    display: block;
                                }

                                .remove {
                                    padding-right: 7px;
                                    position: absolute;
                                    right: 0;
                                    top: 0;
                                    display: none;
                                }
                            </style>
                            <div class="image-pics">

                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </form>
        <!--Row-->

        <!-- Documentation Link -->



    <!---Container Fluid-->




