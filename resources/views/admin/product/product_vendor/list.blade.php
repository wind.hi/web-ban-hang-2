@extends('admin.layout.admin')


@section('title', 'Danh sách sản phẩm')

@section('content')

    <div class="container-fluid" id="container-wrapper">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">

        </div>
        <!-- Row -->
        <div class="row">
            <!-- Datatables -->
            <!-- DataTable with Hover -->
            <div class="col-lg-12">
                <div class="card mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">DataTables with Hover</h6>
                    </div>
                    <div class="table-responsive p-3">
                        <div id="dataTableHover_wrapper" class="dataTables_wrapper dt-bootstrap4">

                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table align-items-center table-flush table-hover dataTable" id="products" role="grid" aria-describedby="dataTableHover_info">
                                        <thead class="thead-light">
                                        <tr role="row">
                                            <th>ID</th>
                                            <th>Sản phẩm</th>
                                            <th>Nhà cung cấp</th>
                                            <th>Hình ảnh</th>
                                            <th>Giá tiền</th>
                                            <th>Trạng thái</th>
                                            <th>Thao tác</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Row-->
        <!-- Documentation Link -->
    </div>
{{--    //modal deny confirm product--}}
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Lý do không duyệt</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" id="denyConfirm" method="post">
                    @csrf
                <div class="modal-body">
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Lý do:</label>
                            <textarea class="form-control" id="message-text" name="reason"></textarea>
                            <input type="hidden" name="product_id">
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Đồng ý</button>
                </div>
                </form>
            </div>
        </div>
    </div>
{{--    end modal--}}
{{--    modal show action--}}
    <div class="modal fade" id="actionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body" id="modal-content">
                    <p>You Content</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(function() {
            $('#products').DataTable({
                pageLength: 6,
                type: 'GET',
                ajax: '{{route('dt.vendor.product')}}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'name_store', name: 'name_store' },
                    { data: 'image', name: 'image', render:function (data) {
                            return '<img src="{{ asset('frontend/img/product/') }}' + '/' + data + '" border="0" width="60" class="img-rounded" align="center" />'
                        } },
                    { data: 'price', name: 'price',render:function (price) {
                            return number_format(price,'0',',','.')
                        } },

                    { data: 'status', name: 'status' , render:function (data, type, row) {
                            if(data == 0){
                                return '<button type="button" class="btn btn-secondary mb-1">chưa duyệt</button>';
                            }
                            if(data == 1){
                                return "<button type='button' class='btn btn-info mb-1' data-productid='"+ row.id +"' onclick='return showAction(this);' data-action='viewConfirm' >đã duyệt</button>";
                            }else{
                            return '<button type="button" class="btn btn-danger mb-1" data-productid="'+ row.id +'" onclick="return showAction(this);" data-action="viewDenyConfirm">không duyệt</button>'
                            }
                        }},
                    { data: 'action', name: 'action', searchable: false, orderable: false }
                ],
                order : [
                    [ 0, 'desc' ]
                ]
            });
        });
    </script>
    <script>
        function showAction(e) {
            var productId = $(e).data('productid');
            var action = $(e).data('action');
            $.ajax({
                url: "{{ route('admin.confirm.product.vendor')}}",
                method: 'GET',
                data: {
                    product_id: productId,
                    action: action,
                },
                success: function(data) {
                    $("#modal-content").empty();
                    $("#modal-content").html(data);
                    $("#actionModal").modal('show');
                    console.log(data);

                },
                error: function(error) {}
            }, );
        }
        function denyConfirm(e) {
            var productId = $(e).data('id');
            var url = $(e).data('url');
            $("input[name='product_id']").val(productId);
            $("form#denyConfirm").attr('action',url);
        }
        function revertStatus(e) {
            var productId = $(e).data('id');
            $.ajax({
                url: "{{ route('revert.status.product')}}",
                method: 'GET',
                data: {
                    product_id: productId,
                },
                success: function(data) {
                   location.reload();
                    console.log(data);
                },
                error: function(error) {}
            }, );
        }
    </script>
@endpush
