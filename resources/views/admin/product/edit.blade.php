@extends('admin.layout.admin')
@section('title', 'Cập nhật sản phẩm')
@section('group', 'Product')
@section('action', 'Add')
@section('content')

    <!-- Container Fluid-->
    <div class="container-fluid" id="container-wrapper">
        {{--        {{print_r(\App\Entity\Category::getParentCategories($categories,$product->category_id))}}--}}
        @if(\App\Entity\Category::getParentCategories($categories,$product->category_id) != 0)
            @php
                $arr = [];
                $arr = \App\Entity\Category::getParentCategories($categories,$product->category_id,$arr)
            @endphp
            @php
                krsort($arr)
            @endphp
        @endif
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{session('success')}}
                &nbsp;<a href="{{route('edit.image.product', $product->id)}}" class="btn btn-info mb-1">cập nhật ảnh sản phẩm</a>
            </div>
        @endif
        <form method="post" action="{{route('product.update', $product->id)}}" enctype="multipart/form-data">
            {{csrf_field()}}
            {{ method_field('PUT') }}
            <div class="row">
                <div class="col-lg-6">
                    <!-- Form Basic -->
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Thông tin</h6>
                        </div>
                        <div class="card-body">
                            <input type="hidden" name="idPro" value="{{$product->id}}">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên sản phẩm</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nhập tên sản phẩm ..." value="{{old('name', $product->name)}}">
                                @if($errors->has('name'))
                                    <span class="error-text">
                                        {{$errors->first('name')}}
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Số lượng</label>
                                <input type="text" class="form-control formatPrice @error('quantity') is-invalid @enderror" id="exampleInputEmail1" placeholder="Nhập số lượng sản phẩm ..." name="quantity" value="{{ old('quantity', $product->quantity)}}">
                                @if($errors->has('quantity'))
                                    <span class="error-text">
                                        {{$errors->first('quantity')}}
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Đơn giá</label>
                                <input type="text" class="form-control formatPrice @error('price') is-invalid @enderror" id="exampleInputEmail1" placeholder="Nhập giá sản phẩm" name="price" value="{{old('price',$product->price)}}">
                                @if($errors->has('price'))
                                    <span class="error-text">
                                        {{$errors->first('price')}}
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Giá khuyến mãi</label>
                                <input type="text" class="form-control formatPrice @error('discount') is-invalid @enderror" id="stepProductCreate4" placeholder="nhập tên danh mục" name="discount" value="{{old('discount',$product->discount)}}">
                                @if($errors->has('discount'))
                                    <span class="error-text">
                                        {{$errors->first('discount')}}
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Danh mục </h6>

                            <button class="btn btn-success btn-sm"  data-toggle="modal" data-target="#editCate" type="button"><i class="fa fa-pencil"></i></button>
                            <input type="hidden" name="category_id" value="{{old('category_id', $product->category_id)}}">

                        </div>
                        <div class="card-body">
                            <div class="form-group show-line-cate">
                                @if(isset($arr))
                                    @foreach($arr as $cate)
                                        <span>{{$cate}} <i class="fas fa-angle-right"></i></span>
                                    @endforeach
                                @endif
                                <span>{{$product->category_title}}</span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-6">
                    <!-- General Element -->
                    <!-- Input Group -->

                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary"></h6>
                        </div>

                        <div class="card-body">
                            <div class="form-group">
                                <label>Mô tả ngắn</label>
                                <textarea name="short_description" class="form-control editor">{{old('short_description',$product->short_description)}}</textarea>
                            </div>
                            @if($errors->has('short_description'))
                                <span class="error-text">
                                    {{$errors->first('short_description')}}
                                </span>
                            @endif
                        </div>

                        <div class="card-body">
                            <div class="form-group">
                                <label>Mô tả</label>
                                <textarea name="description" class="form-control editor" id="editor1">{{old('description',$product->description)}}</textarea>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="row">
                <div class="col-md-10">

                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-success mb-1">Cập nhật</button>

                </div>
            </div>

        </form>



    </div>
    <!---Container Fluid-->
{{--    table sub product--}}
    <div class="col-lg-12 mb-4">
        <!-- Simple Tables -->
        <div class="card">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Danh sách sản phẩm con</h6>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center table-flush" id="tblSubProduct">
                    <thead class="thead-light">
                    <tr>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Discount</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                @foreach($subProducts as $sub)
                    <tr id="{{$sub->id}}">
                        <td><a href="#">{{$sub->name}}</a></td>
                        <td class="qty-subproduct">{{$sub->quantity}}</td>
                        <td class="price-subproduct">{{number_format($sub->price,'0',',','.')}} vnđ</td>
                        <td class="discount-subproduct">{{number_format($sub->discount,'0',',','.')}} vnđ</td>>
                        <td><button class="btn btn-sm btn-primary" data-id="{{$sub->id}}" id="edit-subproduct{{$sub->id}}" onclick="return editSubProduct(this);">Edit</button>
                            <button class="btn btn-sm btn-primary save-subproduct" id="save-subproduct{{$sub->id}}" data-id="{{$sub->id}}" onclick="return updateSubProduct(this);">Save</button>
                            <a href="{{route('delete.subproduct',$sub->id)}}"
                               class="btn btn-sm btn-danger " data-method="DELETE"
                               onclick="return confirm('Delete this sub product?')">Remove
                            </a>
                        </td>
                    </tr>
                @endforeach

                    </tbody>
                </table>
            </div>
            <div class="card-footer"></div>
        </div>
    </div>
{{-- end table --}}
    {{-- modal edit category--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="editCate">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal-content">
                    <div class="form-group line-category">
                        @if(isset($arr))
                            @foreach($arr as $cate)
                                <span>{{$cate}} <i class="fas fa-angle-right"></i></span>
                            @endforeach
                        @endif
                        <span>{{$product->category_title}}</span>
                    </div>
                    <div class="form-group">
                        <input class="easyui-combotree" id="cate-tree" style="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">apply</button>

                    </div>
                </div>

            </div>
        </div>
    </div>
    {{--    end modal--}}


<style>
    input.edit-input {
        border: 0;
        outline: 0;
        background: transparent;
        border-bottom: 1px solid black;
        width: 100px;
    }
</style>
@endsection
@push('scripts')
    <script>
        function editSubProduct(e) {
            var subId = $(e).data('id');
            var qty = $("tr#"+subId).find('td.qty-subproduct').text();
            var price = $("tr#"+subId).find('td.price-subproduct').text();
            var discount = $("tr#"+subId).find('td.discount-subproduct').text();

            $("tr#"+subId).find('td.qty-subproduct').empty();
            $("tr#"+subId).find('td.price-subproduct').empty();
            $("tr#"+subId).find('td.discount-subproduct').empty();

            $("tr#"+subId).find('td.qty-subproduct').append('<input type="text" class=" formatPrice edit-input" value='+qty+'  min="1">');
            $("tr#"+subId).find('td.price-subproduct').append('<input type="text" value='+price+' class="edit-input  formatPrice "  min="1">');
            $("tr#"+subId).find('td.discount-subproduct').append('<input type="text" value='+discount+' class="edit-input  formatPrice" min="1">');

            $("input.edit-input:text:visible:first").focus();
            $("#edit-subproduct"+subId).hide();
            $("#save-subproduct"+subId).show();
            $('.formatPrice').priceFormat({
                prefix: '',
                centsLimit: 0,
                thousandsSeparator: '.'
            });

        }
        function updateSubProduct(e) {
            var subId = $(e).data('id');
            var qty = $("tr#"+subId).find('td.qty-subproduct input.edit-input').val();
            var price = $("tr#"+subId).find('td.price-subproduct input.edit-input').val();
            var discount = $("tr#"+subId).find('td.discount-subproduct input.edit-input').val();

            $.ajax({
                url: "{{ route('update.subproduct') }}",
                method: 'POST',
                data: {
                    _token:"{{csrf_token()}}",
                    product_id: subId,
                    quantity: qty,
                    price: price,
                    discount: discount,
                },
                success: function() {
                    location.reload()
                },
                error: function(error) {}
            }, );
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".save-subproduct").hide();
            $(".amsify-select-operations").hide();
            $('.formatPrice').priceFormat({
                prefix: '',
                centsLimit: 0,
                thousandsSeparator: '.'
            });
            $(".date-sale").hide();
            $("#stepProductCreate4").keyup(function() {
                $(".date-sale").show();
                if ($("#stepProductCreate4").val() == 0) {
                    $(".date-sale").hide();
                }
            });
            $('.image-list').hide();
            $("#pick-images").change(function() {
                $('.image-list').show();
                $('.image-list').empty();
                var names = [];
                for (var i = 0; i < $(this).get(0).files.length; ++i) {
                    names.push($(this).get(0).files[i].name);

                    var reader = new FileReader();
                    reader.onload = function(event) {
                        $($.parseHTML('<img width="85px" height="85px" style="margin-right: 10px" >')).attr('src', event.target.result).appendTo('.image-list');
                    }

                    reader.readAsDataURL(this.files[i]);

                }
                for (i in names) {
                    $('.image-list').append(
                        '<div class="custom-control custom-radio">\n' +
                        '                                    <input type="radio" id="' + names[i] + '" value="' + names[i] + '"name="customRadio" class="custom-control-input" >\n' +
                        '                                    <label class="custom-control-label" for="' + names[i] + '">' + names[i] + '</label>\n' +
                        '                                </div>');
                }
            });
        });
        function readIMG(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })

    </script>
    <script type="text/javascript">
        $("#choose-att").click(function() {
            var attribute_id = $('.select-attribute option:selected').val();

            var label_att = $(".label-att");
            for ($i = 0; $i < label_att.length; $i++) {
                if ($('.select-attribute option:selected').text() == label_att[$i].value) {
                    return;
                }
            }
            $.ajax({
                url: "{{ route('get.att') }}",
                method: 'GET',
                data: {
                    attribute_id: attribute_id
                },
                success: function(data) {
                    $('.add-attribute').append(data);
                    // $("#notifiSuccess").modal('show');
                    $('.select2').select2()

                    //Initialize Select2 Elements
                    $('.select2bs4').select2({
                        theme: 'bootstrap4'
                    })
                },
                error: function(error) {}
            }, );
        });

    </script>

    <script>
        $(document).ready(function () {
            $('#cate-tree').combotree('loadData', {!! json_encode(\App\Entity\Category::getSubCategory($categories,0)) !!});
            $("#cate-tree").combotree({
                onClick: function(){
                    var t =  $("#cate-tree").combotree('tree');	// get the tree object
                    var n = t.tree('getSelected');
                    var id = n.id;
                    var text = n.text;
                    $("input[name='category_id']").val(id);
                    changeCate(id,text);
                },
            });
        });
    </script>
    <script>
        function editCategory(e) {
            var cateId = $(e).data('id');
            $.ajax({
                    url:"{{route('detail.cate')}}",
                    method:'GET',
                    data:{
                        cateId:cateId
                    },
                    success: function (data) {
                        $('#modal-content2').html(data);
                        // $("#notifiSuccess").modal('show');
                    },
                    error: function(error) {
                    }
                },
            );
        }
    </script>
    <script>
        function changeCate(cateId,text) {
            var url = "{{ route('line.cate', "cateId") }}";
            url = url.replace('cateId', cateId);
            $.ajax({
                    url:url,
                    method:'GET',
                    success: function (data) {

                        // $("#notifiSuccess").modal('show');
                        $(".line-category").empty();
                        $(".line-category").html(data);
                        $(".show-line-cate").empty();
                        $(".show-line-cate").html(data);
                        getCateSelected(text);
                        console.log(data);
                    },
                    error: function(error) {
                    }
                },
            );
        }
        function getCateSelected(cate) {
            $(".line-category").append(' <span> '+cate+' </span>');
            $(".show-line-cate").append(' <span> '+cate+' </span>');
        }
        function getAttProduct(e) {
            var product_id = $(e).data('pid');
            var attrId = $(e).data('aid');

            $.ajax({
                    url:'{{route('get.att.product')}}',
                    method:'GET',
                    data:{
                        product_id:product_id,
                        attribute_id:attrId
                    },
                    success: function (data) {
                        $('#modal-content2').html(data);
                        $('.select2').select2()
                        //Initialize Select2 Elements
                        $('.select2bs4').select2({
                            theme: 'bootstrap4'
                        })
                    },
                    error: function(error) {
                    }
                },
            );
        }

        function updateAttProduct(e) {

            var attId = $("#attributes").val();
            var product_id = $(e).data('pid');
            var attribute_id = $("#attribute_id").val();
            console.log(attId);
            $.ajax({
                    url:'{{route('update.att.product')}}',
                    method:'GET',
                    data:{
                        att_id:attId,
                        product_id:product_id,
                        attribute_id:attribute_id
                    },
                    success: function (data) {
                        alert('cập nhật thành công');
                    },
                    error: function(error) {
                    }
                },
            );
        }
    </script>

@endpush
