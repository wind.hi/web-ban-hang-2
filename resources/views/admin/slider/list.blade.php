@extends('admin.layout.admin')
@section('title', 'List Slider')
@section('group', 'List')
@section('action', 'Slider')
@section('content')

<div class="card my-1">
	<form action="" id="addForm" method="post" enctype="multipart/form-data" class="card-body">
		@csrf
		<div class="row">
			<div class="form-group col-4">
				<label for="title">Title</label>
				<input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Title...">
                @if($errors->has('title'))
                    <span class="error-text">
                        {{$errors->first('title')}}
                    </span>
                @endif
			</div>
			<div class="form-group col-4">
				<label for="product">Product</label>
				<select name="product" id="product" class="form-control @error('product') is-invalid @enderror" >
                    <option value=""> ---- Link đến trang sản phẩm ----</option>
					@foreach($products as $product)
					<option value="{{ $product->id }}"> {{ $product->name }}</option>
					@endforeach
				</select>
                @if($errors->has('product'))
                    <span class="error-text">
                        {{$errors->first('product')}}
                    </span>
                @endif
			</div>

			<div class="form-group col-4">
				<label for="slider">Slider Image</label>
				<div class="input-group" style="">
					<div class="custom-file">
						<input type="file" class="custom-file-input @error('slider') is-invalid @enderror" id="slider" name="slider">
						<label class="custom-file-label" for="slider">Choose file</label>
					</div>
				</div>
                @if($errors->has('slider'))
                    <span class="error-text">
                        {{$errors->first('slider')}}
                    </span>
                @endif
			</div>
		</div>
		<div class="form-group">
			<label for="description">Description</label>
			<input type="text" class="form-control  @error('description') is-invalid @enderror" id="description" name="description" placeholder="Description...">
            @if($errors->has('description'))
                <span class="error-text">
                    {{$errors->first('description')}}
                </span>
            @endif
		</div>
	</form>
	<div class="card-footer">
		<button form="addForm" type="submit" class="btn btn-sm btn-success">Submit</button>
		<button form="addForm" type="reset" class="btn btn-sm btn-danger">Reset</button>
	</div>
</div>
<hr>
<div class="card my-1">
	<div class="card-body">
		<div class="table-responsive">
			<table class="table align-items-center table-flush">
				<thead class="thead-light">
					<tr>
						<th>STT</th>
						<th>Ảnh Slider</th>
						<th>Tiêu đề</th>
						<th>Mô Tả Slider</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
                <?php $stt =1; ?>
					@foreach($sliders as $slider)
					<tr>
						<td>{{$stt}}</td>
						<td>
                            <img width="60px" src="{{ asset('frontend/img/slider/'.$slider->image) }}" alt="">
                        </td>
						<td>{{ $slider->title }}</td>
						<td>{{ $slider->description }}</td>
						<td>
							<a href="javascript:void(0)" onclick="removeSlider({{ $slider->id }})" class="btn btn-danger">Delete</a>
						</td>
					</tr>
                        <?php $stt++; ?>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@endsection

@push('scripts')
    <script type="text/javascript">
        function removeSlider(id) {
            var a = 1
            Swal.fire({
                title: 'Bạn có chắc muốn xóa?',
                text: "vui lòng chọn Ok hoặc Cancel!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ok!'
            }).then((result) => {
                if (result.value) {
                    window.location.href = `slider/delete/${id}`;
                }
            })
        }
    </script>
@endpush
