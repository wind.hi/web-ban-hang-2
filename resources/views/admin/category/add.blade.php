@extends('admin.layout.admin')
@section('title', 'Add A Category')
@section('group', 'Categories')
@section('action', 'Add')
@section('content')
<div class="row">
	<div class="col-lg-4 col-md-offset-4">
		<div class="card">
			<div class="card-header">
				<h6 class="m-0 font-weight-bold text-primary">List Categories</h6>
			</div>
			<hr>
			<div class="card-body">
				@php
				\App\Entity\Category::showCategories($categories,0);
				@endphp
			</div>
		</div>
	</div>
	<div class="col-lg-8 mb-4 parent-cate">
		<!-- Simple Tables -->
		<div class="card">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Add Parent Category</h6>
			</div>
			<hr>
			<form action="{{route('add.category')}}" id="addForm" method="post" enctype="multipart/form-data" class="card-body">
				@csrf
				<div class="form-group">
					<label for="title">Title</label>
					<input type="text" class="form-control  @error('title') is-invalid @enderror" id="title" name="title" placeholder="Title...">
                    @if($errors->has('title'))
                        <span class="error-text">
                            {{$errors->first('title')}}
                        </span>
                    @endif
				</div>
				<input type="hidden" class="form-control" name="parent" value="0">
				<div class="card-footer">
					<button form="addForm" type="submit" class="btn btn-sm btn-success">Submit</button>
					<button form="addForm" type="reset" class="btn btn-sm btn-danger">Reset</button>
				</div>
			</form>
		</div>
	</div>

	{{-- form add sub category--}}
	<div class="col-lg-8 mb-4 sub-cate" style="display: none;">
		<!-- Simple Tables -->
		<div class="card">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Add Sub Category
					<code>0</code>
				</h6>
				<button type="button" class="btn btn-sm btn-info" onclick="return addParentCate(this);">Add Parent Category</button>
			</div>

			<form action="{{route('add.category')}}" id="" method="post" enctype="multipart/form-data" class="card-body">
				@csrf
				<div class="form-group">
					<label for="title">Title</label>
					<input type="text" class="form-control" id="title" name="title" placeholder="Title...">
					<input type="hidden" class="form-control" name="parent" value="0">
				</div>
				<div class="card-footer">
					<button type="submit" class="btn btn-sm btn-success">Submit</button>
					<button type="reset" class="btn btn-sm btn-danger">Reset</button>
				</div>
			</form>
		</div>
	</div>
	{{-- end form--}}
</div>
{{-- modal edit category--}}
<div class="modal fade" tabindex="-1" role="dialog" id="editCate">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Update category</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="modal-content">

			</div>

		</div>
	</div>
</div>
{{-- end modal--}}
@endsection
@push('scripts')
<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
<script type="text/javascript">
	jQuery(function($) {
		$("#treeview").shieldTreeView();
	});

</script>
<script>
	$(document).ready(function() {
		$(".sub-cate").hide();


	});

	function addParentCate() {
		$(".parent-cate").show();
		$(".sub-cate").hide();
		$("input[name='parent']").attr('value', 0);
	}

	function addSubCate() {
		$(".parent-cate").hide();
		$(".sub-cate").show();
		var id = $(".sui-treeview-item-selected span.cate-id:first").text();
		$("input[name='parent']").attr('value', id);
		$("code").html(id);
	}
	$('.sui-treeview-item li').click(function() {
		alert('hi');
	});

	function editCate(e) {
		var cateId = $(e).data('id');
		$.ajax({
			url: "{{ route('detail.cate' )}}",
			method: 'GET',
			data: {
				cateId: cateId
			},
			success: function(data) {
				$('#modal-content').html(data);
				// $("#notifiSuccess").modal('show');
			},
			error: function(error) {}
		}, );
	}

</script>
<script>
	$(document).ready(function() {
		$("input[name='checkbox_cate']").click(function() {
			if ($("input[name='checkbox_cate']").is(':checked')) {
				$("input[name='status']").attr('value', '1');
			} else {
				$("input[name='status']").attr('value', '0');
			}

		});
	});

	function addStatus(e) {
		if ($("input[name='checkbox_cate']").is(':checked')) {
			$("input[name='status']").attr('value', '1');
			$(".custom-control-label").find('span').remove();
			$(".custom-control-label").append('<span>show</span>');
		} else {
			$("input[name='status']").attr('value', '0');
			$(".custom-control-label").find('span').remove();
			$(".custom-control-label").append('<span>hide</span>');
		}
	}

	function updateCate(e) {
		var cateId = $("input[name='cate_id']").val();
		var title = $("input[name='title_cate']").val();
		var status = $("input[name='status']").val();

		if(title.trim().length < 1){
            $("#error-title-edit").text('Tên danh mục không được để trống!');
        }else{
            $.ajax({
                url: "{{route( 'category.update' )}}",
                method: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    cateId: cateId,
                    title: title,
                    status: status
                },
                success: function(data) {
                    if(data.error.length > 0)
                    {
                        $("#error-title-edit").text('Tên danh mục đã tồn tại!');
                    }else{
                        $(e).text('updating...');
                        setTimeout(()=>{
                            location.reload();
                        },1200)
                        Toast.fire({
                            icon: 'success',
                            title: "Cập nhật danh mục thành công!",
                            position: 'top-end',
                            timer: 1000,
                        })
                    }
                },
                error: function(error) {}
            }, );
        }
	}

	function removeCate(id) {
        Swal.fire({
            title: 'Bạn có chắc muốn xóa?',
            text: "vui lòng chọn Ok hoặc Cancel!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ok!'
        }).then((result) => {
            if (result.value) {
                window.location.href = `category/delete/${id}`;
            }
        })
    }
</script>

@endpush
