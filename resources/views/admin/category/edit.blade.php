@extends('admin.layout.admin')
@section('title', 'Edit A Category')
@section('group', 'Categories')
@section('action', 'Edit')
@section('content')
<div class="row">
	<div class="col-lg-12 mb-4">
		<!-- Simple Tables -->
		<div class="card">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Edit Category {{ $category->title }}</h6>
				<a href="{{ route('category.list') }}" class="btn btn-sm btn-info">Back to list</a>
			</div>

			<form action="" id="addForm" method="post" enctype="multipart/form-data" class="card-body">
				@csrf

				<div class="form-group">
					<label for="title">Title</label>
					<input type="text" class="form-control" id="title" name="title" value="{{ $category->title }}">
				</div>
				<div class="form-group">
					<label for="parent">Parent Category</label>
					<select class="form-control" id="parent" name="parent">
						<option value="0" @if($category->parent == 0) selected @endif >
							---
						</option>
						@foreach($categories as $category_parent)
						<option value="{{ $category_parent->id }}" @if( $category->parent == $category_parent->id) selected @endif
							>
							{{ $category_parent->title }}
						</option>
						@endforeach
					</select>
				</div>
			</form>
			<div class="card-footer">
				<button form="addForm" type="submit" class="btn btn-sm btn-success">Submit</button>
				<button form="addForm" type="reset" class="btn btn-sm btn-danger">Reset</button>
			</div>
		</div>
	</div>
</div>


@endsection
