@extends('admin.layout.admin')
@section('title', 'List Categories')
@section('group', 'Categories')
@section('action', 'List')
@section('content')
<div class="row">
	<div class="col-lg-12 mb-4">
		<!-- Simple Tables -->
		<div class="card">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">List Categories</h6>
				<a href="{{ route('category.add') }}" class="btn btn-sm btn-info">Add A Category</a>
			</div>
			<div class="table-responsive">
				<table class="table align-items-center table-flush">
					<thead class="thead-light">
						<tr>
							<th>ID</th>
							<th>Title</th>
							<th>Slug</th>
							<th>Parent</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						@foreach($categories as $category)
						<tr>
							<td>{{$category -> id}}</td>
							<td>{{$category -> title}}</td>
							<td>{{$category -> slug}}</td>
							<td>{{$category -> parent}}</td>
							<td>
								<a href="{{ route('category.edit', ['id' => $category -> id]) }}" class="btn btn-sm btn-primary">Edit</a>
								<a href="{{ route('category.delete', ['id' => $category -> id]) }}" class="btn btn-sm btn-danger">Delete</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="card-footer"></div>
		</div>
	</div>
</div>


@endsection
