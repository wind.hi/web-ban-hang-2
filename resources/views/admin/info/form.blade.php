<form method="post" action="{{ isset($info) ? route('info.edit') : route('info.store') }}" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-lg-12">
            <!-- Form Basic -->
            <div class="card mb-4">
                <div class="card-header">
                    @if(isset($info))
                        <h3>Update Info</h3>
                    @else
                        <h3>Create Info</h3>
                    @endif
                </div>
                <div class="card-body row">
                    <div class="form-group col-4">
                        <label for="email">Email</label>
                        <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Enter Email Product" value="{{ old('email', isset($info->email) ? $info->email : '') }}">

                        @if($errors->has('email'))
                            <span class="error-text">
                                {{$errors->first('email')}}
                            </span>
                        @endif

                    </div>
                    <div class="form-group col-4">
                        <label for="phone">Phone</label>
                        <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" placeholder="Enter Email Product" value="{{ old('phone', isset($info->phone) ? $info->phone : '') }}">

                        @if($errors->has('phone'))
                            <span class="error-text">
                                {{$errors->first('phone')}}
                            </span>
                        @endif

                    </div>
                    <div class="form-group col-4">
                        <label for="address">Address</label>
                        <input type="text" class="form-control @error('address') is-invalid @enderror" id="address" name="address" placeholder="Enter Email Product" value="{{ old('address', isset($info->address) ? $info->address : '') }}">

                        @if($errors->has('address'))
                            <span class="error-text">
                                {{$errors->first('address')}}
                            </span>
                        @endif

                    </div>
                    <div class="form-group col-12">
                        <label for="about">About</label>
                        <input type="text" class="form-control @error('about') is-invalid @enderror" id="about" name="about" placeholder="Enter Email Product" value="{{ old('about', isset($info->about) ? $info->about : '') }}">

                        @if($errors->has('about'))
                            <span class="error-text">
                                {{$errors->first('about')}}
                            </span>
                        @endif

                    </div>
                    <div class="form-group col-4">
                        Ảnh logo: <br>
                        <img width="50%" id="output" class="img img-thumbnail" src="{{ isset($info) ? asset('frontend/img/info/'.$info->logo)  : asset('images/no-image.jpg') }}" alt="">
                    </div>
                    <div class="form-group col-4">
                        <label for="slider">Logo Website</label>
                        <div class="input-group" style="">
                            <div class="custom-file">
                                <input type="file" name="logo" class="custom-file-input @error('logo') is-invalid @enderror" id="input" name="slider">
                                <label class="custom-file-label" for="slider">Choose file</label>
                            </div>
                        </div>
                        @if($errors->has('logo'))
                            <span class="error-text">
                                {{$errors->first('logo')}}
                            </span>
                        @endif
                    </div>
                </div>
                <div class="card-footer">
                    @if(isset($info))
                        <button type="submit" class="btn btn-success mb-1 ">Cập nhập</button>
                    @else
                        <button type="submit" class="btn btn-success mb-1 ">Thêm mới</button>
                    @endif
                    <button type="reset" class="btn btn-danger mb-1">Đặt lại</button>
                </div>
            </div>
        </div>
    </div>
</form>

