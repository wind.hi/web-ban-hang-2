@extends('admin.layout.admin')
@section('title', 'Quản lý info')
@section('group', 'quản lý info')
@section('content')
<div class="container-fluid" id="container-wrapper">
	<!--Row-->
        @include('admin.info.form')
	<!-- Documentation Link -->

</div>

@endsection

@section('js')
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#output').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#input").change(function() {
            readURL(this);
        });
    </script>
@endsection

