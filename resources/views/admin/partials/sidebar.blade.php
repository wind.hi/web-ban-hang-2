<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{Route('admin.home')}}">
        <div class="sidebar-brand-icon">
        <img src="{{asset('RuangAdmin-master/img/logo/logo2.png')}}">
        </div>
        <div class="sidebar-brand-text mx-3">Web-ban-hang</div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item active">
        <a class="nav-link" href="{{route('admin.home')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Quản lý Sản phẩm
    </div>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#category"
           aria-expanded="true" aria-controls="category">
            <i class="far fa-laugh-squint "></i>
            <span>Quản lý Danh mục</span>
        </a>
        <div id="category" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Danh mục</h6>
                <a class="collapse-item" href="{{route('category.add')}}">Thêm mới</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#attribute"
           aria-expanded="true" aria-controls="attribute">

            <i class="far fa-kiss-wink-heart fa-5x"></i>
            <span>Quản lý Thuộc tính</span>
        </a>
        <div id="attribute" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Thuộc tính</h6>
                <a class="collapse-item" href="{{route('attribute.list')}}">Danh sách</a>
                <a class="collapse-item" href="{{route('attribute.add')}}">Thêm mới</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#product"
           aria-expanded="true" aria-controls="collapseBootstrap">
            <i class="far fa-angry"></i>
            <span>Quản lý Sản phẩm</span>
        </a>
        <div id="product" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Sản phẩm</h6>
                <a class="collapse-item" href="{{route('product.index')}}{{"?vendor_id=".\Illuminate\Support\Facades\Auth::id()}}">Danh sách</a>
                <a class="collapse-item" href="{{route('product.create')}}">Thêm mới Sản phẩm</a>
{{--                <a class="collapse-item" href="{{route('add.product.option')}}">Thêm mới Sản phẩm con</a>--}}

                <h6 class="collapse-header">Sản phẩm nhà cung cấp</h6>
                <a class="collapse-item" href="{{route('list.vendor.product')}}">Danh sách</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#order"
           aria-expanded="true" aria-controls="collapseBootstrap">
            <i class="far fa-angry"></i>
            <span>Quản lý Hóa Đơn</span>
        </a>
        <div id="order" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Hóa đơn</h6>
                <a class="collapse-item" href="{{route('order.list')}}">Danh sách chưa xử lý</a>
                <a class="collapse-item" href="{{route('Order.list2')}}">Danh sách đã xử lý</a>
            </div>
        </div>
    </li>

    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Quản lý tài khoản
    </div>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#user"
           aria-expanded="true" aria-controls="collapseBootstrap">
            <i class="far fa-grin-stars"></i>            <span>Quản lý tài khoản</span>
        </a>
        <div id="user" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">tài khoản quản trị</h6>

                <a class="collapse-item" href="{{ route('list.user') }}">Danh sách</a>
                <a class="collapse-item" href="{{ route('add.user') }}">Thêm mới tài khoản</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#role"
           aria-expanded="true" aria-controls="collapseBootstrap">
            <i class="far fa-grin-stars"></i>            <span>Quản lý Roles</span>
        </a>
        <div id="role" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Quản lý Roles</h6>

                <a class="collapse-item" href="{{ route('roles.index') }}">Danh sách Roles</a>
                <a class="collapse-item" href="{{ route('roles.create') }}">Thêm mới Roles</a>
            </div>
        </div>
    </li>

    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Quản lý khách hàng
    </div>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#customer"
           aria-expanded="true" aria-controls="collapseBootstrap">
           <i class="fas fa-users"></i>
            <span>Quản lý Khách Hàng</span>
        </a>
        <div id="customer" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Quản Lý Nhà cung cấp</h6>
                <a class="collapse-item" href="{{route('list.vendor')}}">Danh sách vendor</a>

                <h6 class="collapse-header">Quản Lý Khách Hàng</h6>
                <a class="collapse-item" href="{{route('customer.list')}}">Danh sách</a>
                <a class="collapse-item" href="{{route('customer.add')}}">Thêm mới khách hàng</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePage" aria-expanded="false" aria-controls="collapsePage">
            <i class="fas fa-fw fa-columns"></i>
            <span>Quản lý liên hệ</span>
        </a>
        <div id="collapsePage" class="collapse hidden" aria-labelledby="headingPage" data-parent="#accordionSidebar" style="">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Quản lý liên hệ</h6>
                <a class="collapse-item" href="{{ route('contact.list') }}">danh sách</a>
            </div>
        </div>
    </li>

    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Quản lý giao diện
    </div>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('slider.list') }}">
           <i class="fas fa-users"></i>
            <span>Slider</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('info.show') }}">
            <i class="fab fa-fw fa-wpforms"></i>
            <span>Info Website</span>
        </a>
    </li>

    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Quản lý Ads
    </div>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#ads"
           aria-expanded="true" aria-controls="collapseBootstrap">
            <i class="fas fa-users"></i>
            <span>Quản lý quảng cáo</span>
        </a>
        <div id="ads" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Danh sách quảng cáo</h6>
                <a class="collapse-item" href="{{route('ads.list')}}">Danh sách</a>
                <h6 class="collapse-header">Thêm quảng cáo</h6>
                <a class="collapse-item" href="{{route('ads.add')}}">Thêm</a>
            </div>
        </div>
    </li>

</ul>
