<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Bản quyền &copy; <script> document.write(new Date().getFullYear()); </script> - Thiết kế bởi
              <b><a href="#" target="_blank">Chưa có tên</a></b>
            </span>
        </div>
    </div>
</footer>
