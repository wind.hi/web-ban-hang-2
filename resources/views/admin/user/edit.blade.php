@extends('admin.layout.admin')
@section('title', 'Sửa tài khoản Admin')
@section('group', 'Users')
@section('action', 'Edit')
@section('content')

    <!-- Container Fluid-->
    <div class="container-fluid" id="container-wrapper">
        <form  method="post" action="{{ route('post.edit.user',$user->id) }}" enctype="multipart/form-data">
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Basic -->
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên Admin</label>
                                <input type="text" class="form-control" name="nameAdmin" placeholder="Enter Name Product" value ="{{ $user->name  }}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Email</label>
                                <input type="text" class="form-control" name="email" placeholder="Enter Name Product"  value="{{ $user->email }}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input id="password" type="password" class="form-control" name="password"   placeholder="Enter Name Product" value="{{ $user->password }}">
                            </div>
                            <div class="form-group password_confim">
                                <label for="exampleInputPassword1">Repassword</label>
                                <input type="password" class="form-control" name="password_confirmation"   placeholder="Enter Name Product" value="{{ $user->password }}" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <button type="submit" class="btn btn-success mb-1 ">Cập nhập</button>
            </div>

        </form>
        <!--Row-->

        <!-- Documentation Link -->
    </div>
    <!---Container Fluid-->
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function (){

            $(".password_confim").hide();
            $("#password").keyup(function(){
                $(".password_confim").show();
            });
        });
    </script>
@endpush

