@extends('admin.layout.admin')
@section('title', 'Thêm mới Admin')
@section('group', 'Users')
@section('action', 'Add')
@section('content')
    <!-- Container Fluid-->
    <div class="container-fluid" id="container-wrapper">
        <form  method="post" action="" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Basic -->
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Nhập thông tin - trường có dấu (*) là trường bắt buộc nhập</h6>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên Admin (*)</label>
                                <input type="text" class="form-control" name="nameAdmin" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Name Product" >
                                @if($errors->has('nameAdmin'))
                                    <span class="error-text">
                                        {{$errors->first('nameAdmin')}}
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Email (*)</label>
                                <input type="text" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Name Product" >
                                @if($errors->has('email'))
                                    <span class="error-text">
                                        {{$errors->first('email')}}
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password (*)</label>
                                <input type="password" class="form-control" name="password" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Name Product" >
                                @if($errors->has('password'))
                                    <span class="error-text">
                                        {{$errors->first('password')}}
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Repassword (*)</label>
                                <input type="password" class="form-control" name="password_confirmation" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Name Product" >
                                @if($errors->has('password_confirmation'))
                                    <span class="error-text">
                                        {{$errors->first('password_confirmation')}}
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Role (*)</label>

                                <select class="form-control select-attribute" id="exampleFormControlSelect1" name="roles[]">
                                    <option selected>------------------</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role}}">{{$role}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <button type="submit" class="btn btn-success mb-1 ">Thêm khách hàng</button>
            </div>

        </form>
        <!--Row-->

        <!-- Documentation Link -->


    </div>
    <!---Container Fluid-->
@endsection


