@extends('admin.layout.admin')
@section('title', 'Danh sách user')
@section('group', 'Users')
@section('action', 'List')
@section('content')

    <div class="container-fluid" id="container-wrapper">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">

        </div>
        <!-- Row -->
        <div class="row">
            <!-- Datatables -->
            <!-- DataTable with Hover -->

            <div class="col-lg-12">
                @if ( Session::has('status') )
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <strong>{{ Session::get('status') }}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                    </div>
                @endif
                <div class="card mb-4">
                    <div class="table-responsive p-3">
                        <div id="dataTableHover_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table align-items-center table-flush table-hover dataTable" id="users" role="grid" aria-describedby="dataTableHover_info">
                                        <thead class="thead-light">
                                        <tr role="row">
                                            <th>ID</th>
                                            <th>Tên </th>
                                            <th>Email</th>
                                            <th>Role</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Row-->

    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(function() {
            $('#users').DataTable({
                serverSide:true,
                pageLength: 6,
                type: 'GET',
                ajax: '{{route('get.data.user')}}',
                columns: [
                    { data: 'id',       name: 'id' },
                    { data: 'name',     name: 'name' },
                    { data: 'email',    name: 'email' },
                    { data: 'role',     name: 'role'},
                    { data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });
        });
    </script>
@endpush


