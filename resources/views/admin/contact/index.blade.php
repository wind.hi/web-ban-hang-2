@extends('admin.layout.admin')
@section('title', 'Quản lý liên hệ')
@section('group', 'Danh sách liên hệ')
@section('content')
    <div class="container-fluid" id="container-wrapper">
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">DataTables</h6>
                </div>
                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush" id="dataTable">
                        <thead class="thead-light">
                        <tr>
                            <th width="">#</th>
                            <th width="20%">Title</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Message</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(isset($contacts))
                                @foreach($contacts as $key => $contact)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $contact->title }}</td>
                                        <td>{{ $contact->email }}</td>
                                        <td>{{ $contact->phone }}</td>
                                        <td>{{ $contact->message }}</td>
                                        <td>
                                            <a href="{{ route('contact.action',['status', $contact->id]) }}" class="badge {{ $contact->getStatus($contact->status)['class'] }}">{{ $contact->getStatus($contact->status)['name'] }}</a>
                                        </td>
                                        <td>
                                            <button data-id="{{ $contact->id }}"  data-toggle="modal" data-target="#exampleModal" id="btnDeleteProduct" class="btn btn-sm btn-primary relyContact">Reply</button>
                                            <a href="{{ route('contact.action',['delete', $contact->id]) }}" class="btn btn-sm btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Rely Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $('.relyContact').on('click', function(e) {
            var dataId = $('#btnDeleteProduct').attr('data-id');
            $.ajax({
                url: 'contact/reply-contact/'+dataId,
                method: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: dataId,
                },
                success: function(data) {

                    $('.modal-body').html(`
                    <div class="form-group">
                        <label for="title"><strong>Title Heading: </strong> <span> ${data.success.title}</span></label>
                    </div>
                    <div class="form-group">
                        <label for="title"><strong>Email address: </strong> <span> ${data.success.email}</span></label>
                    </div>
                    <div class="form-group">
                        <label for="title"><strong>Message: </strong> <span> ${data.success.message}</span></label>
                    </div>
                     <hr>
                    <div class="form-group">
                        <label for="title"><strong>Title: </label>
                        <input class="form-control" id="titleMessage" type="text">
                        <span id="error-title" class="error-text"></span>
                    </div>
                    <div class="form-group">
                      <label for="exampleFormControlTextarea1">Reply message :</label>
                      <textarea class="form-control" id="replyMessage" id="exampleFormControlTextarea1" rows="3"></textarea>
                        <span id="error-message" class="error-text"></span>
                    </div>
                        `)
                    $('.modal-footer').html(`
                        <a href="javascript:void(0)" onclick="btnSenMail('${data.success.email}')" type="button" class="btn btn-primary">Send</a>
                    `)
                },
                error: function(error) {}
            }, );
        });
        function btnSenMail(email){
            var title = $('#titleMessage').val();
            var content = $('#replyMessage').val();
            if(title.trim().length < 1){
                $('#error-title').text('Yêu cầu nhập tiêu đề !')
            }
            else if(title.trim().length < 5){
                $('#error-title').text('Nhập tối thiểu 5 ký tự !')
            }
            if(content.trim().length < 1){
                $('#error-message').text('Yêu cầu nhập nội dung !')
            }
            else{
                window.location.href = `contact/send-mail/${email}/${title}/${content}`;
            }
        }
    </script>
@endsection
