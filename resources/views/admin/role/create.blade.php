@extends('admin.layout.admin')
@section('title', 'create new role')
@section('action', 'add role')
@section('content')
    <!-- Container Fluid-->
    <div class="container-fluid" id="container-wrapper">
            <div class="row">
                <div class="col-md-3">

                </div>
                <div class="col-md-6">
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Create new role</h6>
                        </div>
                        <div class="card-body">
                            <form method="post" action="{{route('roles.store')}}">
                                @csrf
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Role</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="inputEmail3" placeholder="" name="name" >
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label>Check Permissions</label>
                                    @foreach($permissions as $permission)
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="{{$permission->id}}" name="permission[]" value="{{$permission->id}}">
                                        <label class="custom-control-label" for="{{$permission->id}}">{{$permission->name}}</label>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">create</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
             <div class="col-md-3">

             </div>
            </div>



        <!--Row-->

        <!-- Documentation Link -->


    </div>
    <!---Container Fluid-->
@endsection



