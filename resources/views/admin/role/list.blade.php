@extends('admin.layout.admin')
@section('title', 'create new role')
@section('action', 'add role')
@section('content')
    <!-- Container Fluid-->
    <div class="container-fluid" id="container-wrapper">
        <div class="row">
            <div class="col-lg-12 mb-4">
                <!-- Simple Tables -->
                <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Simple Tables</h6>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($roles as $key=>$role)
                            <tr>
                                <td><a href="#">{{$key+1}}</a></td>
                                <td>{{$role->name}}</td>
                                <td><button class="btn btn-sm btn-primary" data-id="{{$role->id}}" data-toggle="modal" data-target="#exampleModal" onclick="showRole(this);">Detail</button></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
        </div>
    </div>
    <!---Container Fluid-->
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="post" action="" id="update-role">
                @method('PUT')
                @csrf
                <div class="modal-content" id="modal-content">

                </div>
            </form>

        </div>
    </div>

    <style>
        input.edit-input {
            border: 0;
            outline: 0;
            background: transparent;
            border-bottom: 1px solid black;
            width: 70px;
        }
    </style>
@endsection
@push('scripts')
    <script type="text/javascript">
        function showRole(e){
            var role_id = $(e).data('id');
            $.ajax({
                    url:'{{route('show.role')}}',
                    method:'GET',
                    data:{
                        role_id:role_id
                    },
                    success: function (data) {
                        $('#modal-content').html(data);
                        var url = "{{ route('roles.update', 'id') }}";
                        url = url.replace('id', role_id);
                        $("form#update-role").attr('action',url);
                        // $("#notifiSuccess").modal('show');
                    },
                    error: function(error) {
                    }
                },
            );
        }

    </script>
    <script>
        function editNameRole(e){
               var roleName = $("span#name-role").text();
               var roleId = $(e).data('id');
                var url = "{{ route('roles.update', 'id') }}";
                url = url.replace('id', roleId);
               $("form#update-role").attr('action',url);
               $("span#span-role").find("span#name-role").remove();
               $("span#span-role").append('<input type="text" class="edit-input" value="'+roleName+'" name="role_name">');
                $("a#edit-name-role").attr('onclick','').unbind("click");
        }
        $(document).ready(function () {

        })
    </script>
    @endpush


