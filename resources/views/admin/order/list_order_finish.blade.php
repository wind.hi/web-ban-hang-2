@extends('admin.layout.admin')
@section('title', 'Order | Hóa Đơn đã xử lý')
@section('group', 'Hóa đơn')
@section('action', 'List đã xử lý')
@section('title', 'Order | Hóa Đơn Đã sử lý')
@section('group', 'Order')
@section('content')
    <div class="row">
        <div class="col-lg-12 mb-4">
            <!-- Simple Tables -->
            <div class="card">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Danh sách hóa đơn Đã hoàn thành</h6>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                        <tr>
                            <th>Order ID</th>
                            <th>Customer</th>
                            <th>Item</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $stt =1; ?>
                        @forelse ($orders as $order)
                            <tr>
                                <td><a href="#">{{ $stt }}</a></td>
                                <?php $stt++; ?>
                                <td>{{ $order->name }}</td>
                                <td class="row">
                                    @foreach ($order_items as $order_item)
                                        @if ($order_item->order_id == $order->id)
                                            <div class="col-auto m-1"><mark>{{ $order_item->product_name }}</mark></div>
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                    @foreach ($order_items as $order_item)
                                        @if ($order_item->order_id == $order->id)
                                            <div class="col-auto m-1"><mark>{{ $order_item->price }} ₫</mark></div>
                                        @endif
                                    @endforeach
                                </td>

                                <td>
                                    @if($order->status==1)
                                        <a href="{{route('order.update.status',$order->id)}}"><span class="badge badge-secondary status-badge{{ $order->id }}">Chờ xử lý</span></a>
                                    @elseif($order->status==2)
                                        <a href="{{route('order.update.status',$order->id)}}"><span class="badge badge-danger status-badge{{ $order->id }}">Đang đóng góp</span></a>
                                    @elseif($order->status==3)
                                        <a href="{{route('order.update.status',$order->id)}}"><span class="badge badge-warning status-badge{{ $order->id }}">Đang giao hàng</span></a>
                                    @elseif($order->status==4)
                                        <a href="{{route('order.update.status',$order->id)}}"><span class="badge badge-success status-badge{{ $order->id }}">Đã giao hàng</span></a>
                                    @elseif($order->status==5)
                                        <a><span class="badge badge-info status-badge{{ $order->id }}">Đã Hoàn thành</span></a>
                                    @elseif($order->status==6)
                                        <a href="{{route('order.delete',$order->id)}}"><span class="badge badge-danger status-badge{{ $order->id }}">Delete</span></a>
                                        <a><span class="badge badge-danger status-badge{{ $order->id }}">Khách Đã Hủy</span></a>
                                    @endif
                                </td>

                                {{-- <td><a href="#" class="btn btn-sm btn-primary">Detail</a></td> --}}
                                <td>
                                    <a href="{{route('detail.Order',$order->id)}}" class="btn btn-sm btn-primary">Detail</a>
                                </td>
                            </tr>

                        @empty
                            <tr class="">
                                <td colspan="6">Hiện tại không có đơn hàng nào</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <div></div>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
    </div>
@endsection
