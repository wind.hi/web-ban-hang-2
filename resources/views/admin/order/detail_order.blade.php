@extends('admin.layout.admin')
@section('title', 'Order | Chi tiết hóa đơn')
@section('group', 'Hóa đơn')
@section('action', 'List')
@section('title', 'Order | Chi tiết hóa đơn')
@section('group', 'Order')
@section('content')
    <h1 class="page-header">
        Chi tiết Đơn hàng
        <div class="pull-right">
            <button type="button" class="btn btn-link" onclick="window.history.back();">
                <strong style="font-size: 18px"><i class="fa fa-reply"></i> Quay lại</strong>
            </button>
        </div>
    </h1>
    <ul class="list-group">
        <li class="list-group-item">Họ và tên : {{$user->name}}</li>
        <li class="list-group-item">Số điện thoại: {{$orders->phone}}</li>
        <li class="list-group-item"> Email: {{$user->email}}</li>
        <li class="list-group-item">Tên sản phẩm : {{$order_item->product_name}}</li>
        <li class="list-group-item"> Số lượng sản phẩm : {{$order_item->quantity}}</li>
        <li class="list-group-item"> Địa chỉ nhận hàng : {{$address->address}}</li>
        <li class="list-group-item"> Ngày Đặt hàng :  {{$orders->created_at->format('H:i:s d/m/Y')}}</li>
        <li class="list-group-item"> Tình trạng đơn hàng : @if($orders->status==0)
            <a href="{{route('order.update.status', $orders->id)}}"><span class="badge badge-secondary status-badge{{ $orders->id }}">Đang chờ xủ lý</span></a>
            @elseif ($orders->status == 1)
                <a href="{{route('order.update.status', $orders->id)}}"><span class="badge badge-primary status-badge{{ $orders->id }}">Được chấp nhận</span>
                    @elseif ($orders->status == 2)
                        <a href="{{route('order.update.status', $orders->id)}}"><span class="badge badge-success status-badge{{ $orders->id }}">giao hàng</span></a>

                    @elseif ($orders->status == 3)
                        <a href="{{route('order.update.status', $orders->id)}}"><span class="badge badge-warning status-badge{{ $orders->id }}">Đã giao hàng</span></a>
                    @elseif ($orders->status == 4)
                        <a href="{{route('order.update.status', $orders->id)}}"><span class="badge badge-info status-badge{{ $orders->id }}">xác nhận hoàn thành</span></a>
            @endif
        </li>
        <li class="list-group-item"> Tổng Thanh toán : {{$orders->total_price}} ₫</li>
    </ul>

    <div class="jumbotron">
        <h3><u>Ảnh Minh họa</u></h3>
        <div>
            <img src="{{ asset('') }}frontend/img/product/{{$image->image}}">
        </div>
    </div>
@endsection
