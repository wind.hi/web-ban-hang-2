<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="{{asset('RuangAdmin-master/img/logo/logo.png')}}" rel="icon">
    <title>@yield('title')</title>
    <link href="{{asset('RuangAdmin-master/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('RuangAdmin-master/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('RuangAdmin-master/css/ruang-admin.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('RuangAdmin-master/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('RuangAdmin-master/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="{{asset('dropdown/css/amsify.select.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('combotree/easyui.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('combotree/icon.css')}}">
{{--    <link rel="stylesheet" type="text/css" href="{{asset('combotree/demo.css')}}">--}}
{{--    bootstrap file input--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
{{--    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">--}}

    <style>
        .error-text{
            color: red;
        }
    </style>
</head>
<body id="page-top">
	<div id="wrapper">
		<!-- Sidebar -->
		@include('admin.partials.sidebar')
		<!-- Sidebar -->
		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content">
				<!-- TopBar -->
				@include('admin.partials.nav')
				<!-- Topbar -->
				<!-- Container Fluid-->
				<div class="container-fluid" id="container-wrapper">
					<div class="d-sm-flex align-items-center justify-content-between mb-4">
						<h1 class="h3 mb-0 text-gray-800">@yield('title')</h1>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
							<li class="breadcrumb-item">@yield('group')</li>
							<li class="breadcrumb-item active" aria-current="page">@yield('action')</li>
						</ol>
					</div>
					@yield('content')
				</div>
				<!---Container Fluid-->
			</div>
			<!-- Footer -->
			@include('admin.partials.footer')
			<!-- Footer -->
		</div>
	</div>

	<!-- Scroll to top -->
	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fas fa-angle-up"></i>
	</a>
<script src="{{asset('RuangAdmin-master/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('RuangAdmin-master/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('RuangAdmin-master/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
<script src="{{asset('RuangAdmin-master/js/ruang-admin.min.js')}}"></script>
<script src="{{asset('RuangAdmin-master/js/Dashboard-admin.js')}}"></script>
<script src="{{asset('RuangAdmin-master/vendor/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('RuangAdmin-master/js/demo/chart-area-demo.js')}}"></script>
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('jquery.priceformat.js') }}"></script>
<script src="{{asset('RuangAdmin-master/select2/js/select2.full.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type='text/javascript' src='https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js'></script>
{{--    <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.slim.min.js"></script>--}}
    <script type='text/javascript' src="{{ asset('dropdown/js/jquery.amsifyselect.js') }}"></script>

{{--    <script type="text/javascript" src="{{asset('combotree/jquery.min.js')}}"></script>--}}
    <script type="text/javascript" src="{{asset('combotree/jquery.easyui.min.js')}}"></script>

    <script>
    CKEDITOR.replace('editor1',{
        filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
        filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash')}}',
        filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files')}}',
        filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}' ,
        filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
    })
    const Toast = Swal.mixin({
        toast: true,
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
    @if(Session::has('message'))
    Toast.fire({
        icon: 'success',
        title: "{{ Session::get('message') }}",
        position: 'top-end'
    })
    {{ Session::put('message', null) }}
    @endif
</script>
    @stack('scripts')
    @yield('js')
</body>

</html>
