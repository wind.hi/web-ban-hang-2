@extends('admin.layout.admin')
@section('title', 'List Categories')
@section('group', 'Categories')
@section('action', 'List')
@section('content')

<div class="row">
	<div class="col-lg-12 mb-4">
		<!-- Simple Tables -->
		<div class="card">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">List Atrributes</h6>
				<a href="{{ route('attribute.add') }}" class="btn btn-sm btn-info">Add A Attribute</a>
			</div>
			<div class="table-responsive">
				<table class="table align-items-center table-flush">
					<thead class="thead-light">
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Value</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						@foreach($attributes as $attribute)
						<tr>
							<td>{{$attribute -> id}}</td>
							<td>{{$attribute -> name}}</td>
							<td>
								@foreach($values as $value)
								@if($value->attribute_id == $attribute->id)
								<span class="badge badge-light">{{ $value->value }}</span>
								@endif
								@endforeach
							</td>
							<td>
								<a href="{{route('attribute.edit',$attribute->id)}}" class="btn btn-sm btn-primary">Edit</a>
								<a href="javascript:void(0)" onclick="remove({{$attribute->id}})" class="btn btn-sm btn-danger">Delete</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="card-footer"></div>
		</div>
	</div>
</div>


@endsection
@section('js')
    <script type="text/javascript">
        function remove(id) {
            Swal.fire({
                title: 'Bạn có chắc muốn xóa?',
                text: "vui lòng chọn Ok hoặc Cancel!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ok!'
            }).then((result) => {
                if (result.value) {
                    window.location.href = `attribute/delete/${id}`;
                }
            })
        }
    </script>
@endsection
