@extends('admin.layout.admin')
@section('title', 'Edit Attribute')
@section('group', 'Attribute')
@section('action', 'Edit')
@section('content')

<div class="row">
	<div class="col-lg-8 mb-4">
		<!-- Simple Tables -->
		<div class="card">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Chỉnh Sửa Thuộc Tính {{ $attribute->attribute_name }}</h6>
				<a href="{{ route('attribute.list') }}" class="btn btn-sm btn-info">Quay lại danh sách</a>
			</div>

			<form action="" id="addForm" method="post" enctype="multipart/form-data" class="card-body">
				@csrf

				<div class="form-group">
					<label for="name">Tên Thuộc Tính</label>
					<input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ $attribute->name }}">
                    @if($errors->has('name'))
                        <span class="error-text">
                            {{$errors->first('name')}}
                        </span>
                    @endif
				</div>

				<div class="form-group">
					<label for="value">Thêm Giá Trị Thuộc Tính Mới</label>
					<input type="text" class="form-control" id="value" name="value" placeholder="Giá trị thuộc tính...">
                    <small>Phân cách các giá trị bằng dấu ';' (Ví dụ: Đỏ;Xanh;Vàng)</small> <br>
				</div>

			</form>
			<div class="card-footer">
				<button form="addForm" type="submit" class="btn btn-sm btn-success">Lưu</button>
				<button form="addForm" type="reset" class="btn btn-sm btn-danger">Đặt Lại</button>
			</div>
		</div>
	</div>
	<div class="col-lg-4 mb-4">
		<!-- Simple Tables -->
		<div class="card">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Giá trị thuộc tính đã có</h6>
			</div>
			<div class="table-responsive">
				<table class="table align-items-center table-flush">
					<thead class="thead-light">
						<tr>
							<th>ID</th>
							<th>Giá trị</th>
							<th>Hành Động</th>
						</tr>
					</thead>
					<tbody>
						@foreach($values as $value)
						<tr>
							<td>{{$value -> id }}</td>
							<td>{{$value -> value }}</td>
							<td>
								<a href="{{ route('value.delete', ['id' => $value -> id]) }}" class="btn btn-sm btn-danger">Xóa</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="card-footer"></div>
		</div>
	</div>
</div>
@endsection
