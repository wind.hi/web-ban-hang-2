@extends('admin.layout.admin')
@section('title', 'Add Attribute')
@section('group', 'Attribute')
@section('action', 'Add')
@section('content')
<!--Row-->
<div class="row">
	<div class="col-lg-12 mb-4">
		<!-- Simple Tables -->
		<div class="card">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Add A Attribute</h6>
				<a href="{{ route('attribute.list') }}" class="btn btn-sm btn-info">Back to list</a>
			</div>

			<form action="" id="addForm" method="post" enctype="multipart/form-data" class="card-body">
				@csrf

				<div class="form-group">
					<label for="name">Attribute Name</label>
					<input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Attribute Name...">
                    @if($errors->has('name'))
                        <span class="error-text">
                            {{$errors->first('name')}}
                        </span>
                    @endif
				</div>

				<div class="form-group">
					<label for="value">Attribute Value</label>
					<input type="text" class="form-control @error('value') is-invalid @enderror" id="value" name="value" placeholder="Attribute Value...">
					<small>Phân cách các giá trị bằng dấu ';' (Ví dụ: Đỏ;Xanh;Vàng)</small>
				</div>
                @if($errors->has('value'))
                    <span class="error-text">
                            {{$errors->first('value')}}
                        </span>
                @endif
			</form>
			<div class="card-footer">
				<button form="addForm" type="submit" class="btn btn-sm btn-success">Submit</button>
				<button form="addForm" type="reset" class="btn btn-sm btn-danger">Reset</button>
			</div>
		</div>
	</div>
</div>
<!--Row-->

@endsection
