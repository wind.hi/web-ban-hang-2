@extends('admin.layout.admin')
@section('title', 'Thêm mới Ads')
@section('group', 'Ads')
@section('action', 'Add')
@section('content')
<!-- Container Fluid-->
<div class="container-fluid" id="container-wrapper">
    <form  method="post" action="{{ route('dt.ads.add') }}" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Basic -->
                <div class="card mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Nhập thông tin - trường có dấu (*) là trường bắt buộc nhập</h6>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Tên quảng cáo (*)</label>
                            <input type="text" class="form-control" name="adsName">
                            @if($errors->has('adsName'))
                                <span class="error-text">
                                    {{$errors->first('adsName')}}
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label >Tên công ty vendor (*)</label>
                            <input type="text" class="form-control" name="adsVendor">
                            @if($errors->has('adsVendor'))
                                <span class="error-text">
                                    {{$errors->first('adsVendor')}}
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label >Ảnh quảng cáo (*)</label>
                            <input type="file" name="file" id="file" class=" form-control" />
                            @if($errors->has('file'))
                                <span class="error-text">
                                    {{$errors->first('file')}}
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label >Link quảng cáo (*)</label>
                            <input type="text" class="form-control" name="link">
                            @if($errors->has('link'))
                                <span class="error-text">
                                    {{$errors->first('link')}}
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Phone (*)</label>
                            <input type="phone" class="form-control" name="phone">
                            @if($errors->has('phone'))
                                <span class="error-text">
                                    {{$errors->first('phone')}}
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="status">Status (*)</label>
                            <select class="form-control select-attribute" id="status" name="status">
                                <option value="0">Home</option>
                                <option value="1">Detail</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <button type="submit" class="btn btn-success mb-1">Thêm quảng cáo</button>
        </div>
    </form>
    <!--Row-->
</div>
<!---Container Fluid-->
@endsection





