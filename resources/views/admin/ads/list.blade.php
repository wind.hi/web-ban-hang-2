@extends('admin.layout.admin')
@section('title', 'Danh sách Ads')
@section('group', 'Ads')
@section('action', 'List')
@section('content')
    <div class="container-fluid" id="container-wrapper">
        <div class="d-sm-flex align-items-center justify-content-between mb-4"></div>
        <!-- Row -->
        <div class="row">
            <!-- Datatables -->
            <!-- DataTable with Hover -->
            <div class="col-lg-12">

                <div class="card mb-4">
                    <div class="table-responsive p-3">
                        <div id="dataTableHover_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table align-items-center table-flush table-hover dataTable" id="advertisement" role="grid" aria-describedby="dataTableHover_info">
                                        <thead class="thead-light">
                                            <tr role="row">
                                                <th>ID</th>
                                                <th>Tên Ads </th>
                                                <th>Tên vendor </th>
                                                <th>image</th>
                                                <th>Link</th>
                                                <th>Phone</th>
                                                <th>status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Row-->
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(function() {
            $('#advertisement').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 6,
                type: 'GET',
                ajax: '{{route('dt.ads.list')}}',
                columns: [
                    { data: 'id',               name: 'id' },
                    { data: 'ads_name',         name: 'ads_name' },
                    { data: 'vendor_name',      name: 'vendor_name' },
                    { data: 'image', name: 'image', render:function (data) {
                            return '<img src="{{ asset('frontend/img/product/') }}' + '/' + data + '" border="0" width="60" class="img-rounded" align="center" />'
                        } },
                    { data: 'link',             name: 'link'},
                    { data: 'phone',            name: 'phone'},
                    { data: 'status',  render: function (data) {
                        return (data == 0) ? 'Home' : 'Detail';
                    },           name: 'status'},
                    { data: 'action',           name: 'action', orderable: false, searchable: false}
                ],
            });
        });
    </script>
@endpush


