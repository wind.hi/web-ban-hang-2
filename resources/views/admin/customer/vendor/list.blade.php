@extends('admin.layout.admin')
@section('title', 'Danh sách vendor')
@section('group', 'Users')
@section('action', 'List')
@section('content')

    <div class="container-fluid" id="container-wrapper">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">

        </div>
        <!-- Row -->
        <div class="row">
            <!-- Datatables -->
            <!-- DataTable with Hover -->

            <div class="col-lg-12">
                @if ( Session::has('status') )
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <strong>{{ Session::get('status') }}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                    </div>
                @endif
                <div class="card mb-4">
                    <div class="table-responsive p-3">
                        <div id="dataTableHover_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-flush table-hover dataTable" id="vendor" role="grid" aria-describedby="dataTableHover_info" >
                                        <thead class="thead-light">
                                        <tr role="row">
                                            <th >ID</th>
                                            <th >Store Name</th>
                                            <th >Representative</th>
                                            <th >Email contact</th>
                                            <th>Phone</th>

                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Row-->

    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(function() {
            $('#vendor').DataTable({
                serverSide:true,
                pageLength: 6,
                type: 'GET',
                ajax: '{{route('dt.vendor')}}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name_store', name: 'name_store' },
                    { data: 'name_representative', name: 'name_representative' },
                    { data: 'email', name: 'email' },
                    { data: 'address',name: 'address'},
                    { data: 'status', name: 'status', render:function (data) {
                            if(data == 0){
                                return 'chưa duyệt';
                            }
                            else{
                                return 'đã duyệt'
                            }
                        }},
                    { data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });
        });
    </script>
@endpush


