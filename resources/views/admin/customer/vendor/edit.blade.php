@extends('admin.layout.admin')
@section('title', 'Info Vendor')
@section('group', 'Vendor')
@section('action', 'Information')
@section('content')
    <div class="row">

        <div class="col-md-2">

        </div>

        <div class="col-lg-8 mb-4 parent-cate">
            <!-- Simple Tables -->
            <div class="card">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Vendor {{$vendor->name_representative}}</h6>

                        @if($vendor->status == 0)
                            <button type="button" class="btn btn-secondary mb-1" id="activate-vendor" data-id="{{$vendor->id}}" data-action="active" onclick="return activeVendor(this);">confirm</button>
                        @else
                            <button  type="button" class="btn btn-primary mb-1" id="deactivate-vendor" data-id="{{$vendor->id}}" data-action="deactivate" onclick="return activeVendor(this);">confirmed</button>
                        @endif

                </div>
                <hr>
                <form action="" id="addForm" method="post" enctype="multipart/form-data" class="card-body">

                    <div class="form-group">
                        <label for="title">Representative</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="" value="{{$vendor->name_representative}}"readonly>
                    </div>
                    <div class="form-group">
                        <label for="title">Store Name</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="" value="{{$vendor->name_store}}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="title">Email contact</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="" value="{{$vendor->email}}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="title">Phone</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="" value="{{$vendor->phone}}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="title">Address store</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="" value="{{$vendor->address}}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="title">Store Image</label>
                        <img width="30%" id="output" class="img img-thumbnail" src="{{asset('images/no-image.jpg') }}" alt="">
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="button" class="btn btn-danger mb-1"> deny activate</button>
                        </div>
                    </div>
                    <div class="card-footer">
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-2">

        </div>
        {{-- form add sub category--}}

        {{-- end form--}}
    </div>
    {{-- modal edit category--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="editCate">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal-content">

                </div>

            </div>
        </div>
    </div>
    {{-- end modal--}}
@endsection
@push('scripts')
    <script>
        function activeVendor(e) {
            var vendorId = $(e).data('id');
            var action = $(e).data('action');
            alert("Vui lòng chờ trong giây lát");
            $.ajax({
                url: "{{ route('confirm.vendor')}}",
                method: 'GET',
                data: {
                    vendor_id: vendorId,
                    action: action
                },
                success: function(data) {
                    if(action === 'active'){
                        $("div.card-header").find('button#activate-vendor').remove();
                        $("div.card-header").append('<button type="button" class="btn btn-primary mb-1" id="deactivate-vendor" data-id="'+vendorId+'" data-action="deactivate" onclick="return activeVendor(this);">confirmed</button>');
                    }
                    else{
                        $("div.card-header").find('button#deactivate-vendor').remove();
                        $("div.card-header").append('<button type="button" class="btn btn-secondary mb-1" id="activate-vendor" data-id="'+vendorId+'" data-action="active" onclick="return activeVendor(this);">confirm</button>');
                    }

                },
                error: function(error) {}
            }, );
        }
    </script>
    @endpush
