@extends('admin.layout.admin')
@section('title', 'Danh sách khách hàng bị khóa tài khoản')
@section('group', 'Khách hàng')
@section('action', 'Lock')
@section('content')
<div class="col-lg-12">
    <div class="card mb-4">
      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Tài khoản bị khóa</h6>
      </div>
      <div class="table-responsive p-3">
        <table class="table align-items-center table-flush" id="dataTable">
          <thead class="thead-light">
            <tr>
              <th>ID</th>
              <th>Tên</th>
              <th>Email</th>
              <th>Phone</th>
              <th>address</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($userlock  as $us )
                <tr>
                    <td>{{$us->id}}</td>
                    <td>{{$us->name}}</td>
                    <td>{{$us->email}}</td>
                    <td>{{$us->phone}}</td>
                    <td>{{$us->address}}</td>
                    <td>
                      <a href="{{route('lock.customer',$us->id)}}"><i style="font-size:25px;color:#d63031;" class="fas fa-user-lock"></i></a>
                    </td>
                </tr>
              @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection


