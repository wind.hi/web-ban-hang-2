@extends('admin.layout.admin')
@section('title', 'Danh sách khách hàng')
@section('group', 'Khách hàng')
@section('action', 'List')
@section('content')
<div class="col-lg-12">
    <div class="card mb-4">
      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Danh sách khách hàng</h6>
      </div>
      <div class="table-responsive p-3">
        <table class="table align-items-center table-flush" id="dataTable">
          <thead class="thead-light">
            <tr>
              <th>ID</th>
              <th>Tên</th>
              <th>Email</th>
              <th>Phone</th>
              <th>address</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($users  as $key=>$user )
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone}}</td>
                    <td>{{$user->address}}</td>
                    <td>
                      <a href="{{route('lock.customer',$user->id)}}"><i style="font-size:25px;color:#d63031;" class="fas fa-user-lock"></i></a>
                    </td>
                </tr>
              @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection


