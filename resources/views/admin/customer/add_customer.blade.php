@extends('admin.layout.admin')
@section('title', 'Thêm mới khách hàng')
@section('group', 'Customer')
@section('action', 'Tạo mới')
@section('content')

    <!-- Container Fluid-->
    <div class="container-fluid" id="container-wrapper">
        <form  method="post" action="">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Basic -->
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Nhập thông tin - trường có dấu (*) là trường bắt buộc nhập</h6>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên Admin (*)</label>
                                <input type="text" class="form-control" name="name" id="exampleInputEmail1" value="{{ old('name') }}" aria-describedby="emailHelp" placeholder="Enter Name Product" >
                                @if($errors->has('name'))
                                    <span class="error-text">
                                        {{$errors->first('name')}}
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Địa chỉ Email (*)</label>
                                <input type="text" class="form-control" name="email" id="email" value="{{ old('email') }}" aria-describedby="emailHelp" placeholder="Enter Email" >
                                @if($errors->has('email'))
                                    <span class="error-text">
                                        {{$errors->first('email')}}
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password (*)</label>
                                <input type="password" class="form-control" name="password" value="{{ old('password') }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Name Product" >
                                @if($errors->has('password'))
                                    <span class="error-text">
                                        {{$errors->first('password')}}
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Repassword (*)</label>
                                <input type="password" class="form-control" name="password_confirmation" value="{{ old('password_confirmation') }}" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Name Product" >
                                @if($errors->has('password_confirmation'))
                                    <span class="error-text">
                                        {{$errors->first('password_confirmation')}}
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Address (*)</label>
                                <input type="text" class="form-control" name="address" value="{{ old('address') }}" id="address" aria-describedby="emailHelp" placeholder="Enter Address" >
                                @if($errors->has('address'))
                                    <span class="error-text">
                                        {{$errors->first('address')}}
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số Điện Thoại (*)</label>
                                <input type="number" class="form-control" name="phone" value="{{ old('phone') }}" id="phone" aria-describedby="emailHelp" placeholder="Enter Phone" >
                                @if($errors->has('phone'))
                                    <span class="error-text">
                                        {{$errors->first('phone')}}
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <button type="submit" class="btn btn-success mb-1 ">Đăng Ký</button>
            </div>

        </form>
        <!--Row-->

        <!-- Documentation Link -->


    </div>
    <!---Container Fluid-->
@endsection

