<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('name');
            $table->text('slug');
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->bigInteger('view');
            $table->text('description');
            $table->text('short_description');
            $table->float('price', 11, 2);
            $table->float('discount', 11, 2);
            $table->bigInteger('quantity');
			$table->boolean('type');
			$table->boolean('status');
			$table->bigInteger('user_id');
			$table->string('reason_deny');
			$table->bigInteger('is_admin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
