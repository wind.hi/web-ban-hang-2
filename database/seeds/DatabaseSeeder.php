<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AttributePermisson::class);
        $this->call(CategoryPermission::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(CustomerPermission::class);
        $this->call(RoleTableSeeder::class);
        $this->call(UserPermisson::class);
        $this->call(CreateAdminUserSeeder::class);
    }
}
