<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
class CustomerPermission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'vendor-list',
            'vendor-detail',
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }
}
