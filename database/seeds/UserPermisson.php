<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class UserPermisson extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'user-list',
            'user-edit',
            'user-create',
            'user-delete',
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }
}
