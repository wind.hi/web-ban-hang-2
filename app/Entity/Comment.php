<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public static function getCountComment($productId){
        return self::where('product_id',$productId)->get();
        //  Tương đương viết ở view: Comment::where('product_id',$rev->product_id)->get();
    }
}
