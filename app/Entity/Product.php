<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;


class Product extends Model
{
    public static function findProductById($id){
        $product = static::join('categories','categories.id','products.category_id')
                   ->leftJoin('images','images.product_id','products.id')
                   ->leftJoin('sub_products','sub_products.product_id','products.id')
            ->select(
                'products.id',
                'products.name',
                'products.price',
                'products.quantity',
                'products.discount',
                'images.image',
                'sub_products.quantity',
                'categories.title as category'
            )->where('products.id',$id)->where('images.status',1)->first();
        if (!empty($product)){
            return $product;
        }
        return null;
    }

}
