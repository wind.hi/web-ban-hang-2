<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Contact extends Model
{
    protected $table = 'contacts';
    protected $guarded = ['*'];

    const STATUS_PUBLIC = 1;
    const STATUS_PRIVATE = 2;

    protected $active = [
        1 => [
            'name' => 'Chưa xử lý',
            'class' => 'badge-secondary'
        ],
        2 => [
            'name' => 'Đã xử lý',
            'class' => 'badge-success'
        ]
    ];

    public function getStatus()
    {
        return Arr::get($this->active, $this->status, '[N\A]');
    }

}
