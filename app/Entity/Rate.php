<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    public static function getAvgRate($productId){
        return self::where('product_id',$productId)->avg('point_star');
        // tương đương với viết ở view: Rate::where('product_id',$rate->product_id)->avg('point_star')
    }
}
