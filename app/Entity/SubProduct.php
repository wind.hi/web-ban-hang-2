<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class SubProduct extends Model
{

protected $table = 'sub_products';
protected $fillable = [ 'id', 'product_id', 'price', 'discount','quantity', 'created_at', 'updated_at' ];

}
