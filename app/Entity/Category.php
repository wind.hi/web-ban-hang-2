<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Category extends Model
{
    public function products(){
        return $this->hasMany('App\Entity\Product','category_id', 'id');
    }


    public function getCategory(){
        try {
            $categories = $this->where('parent', 0)
                ->orderBy('id')->get();
        } catch (\Exception $e) {
            $categories = array();
            Log::error('Entity->Category->getCategory: Lấy danh mục ra bị lỗi: '.$e->getMessage());
        } finally {
            return $categories;
        }
    }
    public static function showCategories($categories, $parent_id = 0)
    {
        // BƯỚC 2.1: LẤY DANH SÁCH CATE CON
        $cate_child = array();
        foreach ($categories as $item)
        {
            // Nếu là chuyên mục con thì hiển thị
            if ($item->parent == $parent_id)
            {
                $cate_child[] = $item;
//                unset($categories[$key]);
            }
        }
//         BƯỚC 2.2: HIỂN THỊ DANH SÁCH CHUYÊN MỤC CON NẾU CÓ
        $string = '';
        if ($cate_child)
        {

            echo '<ul id="treeview" class="tree-view">';
            foreach ($cate_child as  $item)
            {

                // Hiển thị tiêu đề chuyên mục
                echo '
				<li data-icon-cls="fa fa-inbox" data-expanded="false">
                    <span style="display: none" class="cate-id">'. $item->id.' </span>'
                    .$item->title.
					'(<span>'.self::countSubCate($item->id).'</span>)'.
					'<a href="#" data-toggle="modal" data-target="#editCate" onclick="return editCate(this);" data-id="'.$item->id.'">
						<i class="fa fa-pencil" aria-hidden="true"></i>
					</a>'.
					'<a onclick="removeCate('.$item->id.')" href="javascript:void(0)">
						<i class="far fa-trash-alt" aria-hidden="true"></i>
					</a>
					<a id="nav-profile-tab" data-toggle="tab" href="#sub-tab" role="tab" aria-controls="nav-profile" aria-selected="false" onclick="return addSubCate(this);">
						<i class="fas fa-plus"></i>
					</a>';

                // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
               self::showCategories($categories, $item->id);
               echo  '</li>';
            }
            echo '</ul>';
        }
    }
    private static function countSubCate($cateId){
        return self::where('parent',$cateId)->count();
    }
    //
    public function childCategories(){
        return $this->hasMany('App\Entity\Category','parent');
    }
    public static function getSubCategory($categories, $parent = 0){
        $result = array();
        $items = array();
        foreach ($categories as $cate){
            if($cate->parent == $parent){
                $items['id']=$cate->id;
                $items['text']=$cate->title;
//                $items['state']= "closed";
                $items['children']= (self::getSubCategory($categories, $items['id']));
                array_push($result,$items);
            }
        }

         return $result;
    }
    public static function getParentCategories($categories, $cate_id = null, &$result=array()){

        $arr = array();
//        $result = array();
        $category = self::where('id',$cate_id)->first();
        if($category->parent == 0){
            return 0;
        }
        foreach ($categories as $cate){

                if($cate->id == $category->parent) {
//                    array_push($arr, $cate->title);
                    $result[] = $cate->title;
//                    array_merge($arr,self::getParentCategories($categories, $cate->id));
                    self::getParentCategories($categories, $cate->id, $result);
                    array_push($arr,$result);
                }

        }
        return current($arr);
    }
}
