<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailRegister extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.EmailRegister')
        ->from('ngoquoctranhieu.hust@gmail.com', 'B Store')
        ->subject('Hello & Welcome!')
        ->replyTo('ngoquoctranhieu.hust@gmail.com', 'B store');
    }
}
