<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class checkLoginAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        if(Auth::check() && Auth::user()->role == 3){
//            return $next($request);
//        }
//       return redirect('admin/login');

        if(!Auth::check()){
          return redirect()->route('admin.login');
        }
        elseif(Auth::check() && Auth::user()->hasRole('Customer')){
            return redirect()->route('admin.login');
        }else{
            return $next($request);
        }
    }
}
