<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Product;
use App\Entity\User;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\HelpersClass\date;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Entity\Roles;
class AdminController extends Controller
{
    public function home(){

		$count_products	= Product::count();
		$count_users 	= User::count();
		$count_orders 	= Order::count();
		$orders 		= Order::leftJoin('users', 'orders.user_id', '=', 'users.id')
								->select('orders.*', 'users.name')
								->Where('status',0)->get();
		$order_items	= OrderItem::get();
		//Doanh thu theo tháng
        $listDay = date::getlistDayInMonth();
        $RevenueOrderMonth = Order::Where('status',5)
            ->WhereMonth('orders.created_at',date('m'))
            ->select(\DB::raw('sum(orders.total_price)'), \DB::raw('DATE(orders.created_at) day'))
            ->groupBy('day')->get()->toArray();
        $arrRevenueOrderDay = [];
        $total = 0;
        foreach ($listDay as $day){
            foreach ($RevenueOrderMonth as $key => $Revenue){
                if($Revenue['day'] == $day){
                    $total = $Revenue['total_price'];
                    break;
                }
            }
            $arrRevenueOrderDay[$day] = $total;
        }
        return view('admin.home.index',[
			'orders'			=> $orders,
			'order_items'		=> $order_items,
			'count_products'	=> $count_products,
			'count_users'		=> $count_users,
			'count_orders'		=> $count_orders,
            'listDay'           => json_encode($listDay),
            'arrRevenueOrderDay'=> json_encode($arrRevenueOrderDay)
		]);
    }

}
