<?php

namespace App\Http\Controllers\Admin;

//Entity
use App\Entity\Attribute;
use App\Entity\AttributeValue;
use App\Entity\Category;
use App\Entity\Product;
use App\Entity\Image;
use App\Entity\ProductImage;
use App\Entity\ProductValue;
use App\Entity\SubProduct;
use App\Entity\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use Validator;
use Yajra\DataTables\DataTables;
use Intervention\Image\ImageManagerStatic as Images;
class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:product-list|product-create|product-delete', ['only' => ['index','show']]);
        $this->middleware('permission:product-create', ['only' => ['create','store']]);
        $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:product-delete', ['only' => ['destroy']]);
        $this->middleware('permission:product-vendor-list', ['only' => ['getListProductVendor']]);
    }
    public function index()
    {
        return view('admin.product.list');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $attributes = Attribute::get();
        $attribute_values = AttributeValue::get();
        $categories = Category::get();
        $list =  $this->listCategory($categories, 0 );

        return view('admin.product.add', [
			'attributes' 	=> $attributes,
			'values' 		=> $attribute_values,
			'categories' 	=> $categories,
            'list'          => $list
		]);
    }
    public function listCategory($categories, $parent = 0, $lever = 0){
        $result = [];
        foreach ($categories as $item) {
            if($item['parent'] == $parent) {
                $item['lever'] = $lever;
                $result[] = $item;
                $child = $this->listCategory($categories, $item['id'], $lever + 1);
                $result = array_merge($result, $child);
            }
        }
        return $result;
    }

    public function getAttValue(Request $request){
        $attValues = Attribute::Join('attribute_values', 'attributes.id', 'attribute_values.attribute_id')
                ->where('attributes.id', $request->input('attribute_id'))->get();

        $att = '';

		foreach ($attValues as $attValue){
            $att .= '
				<option value="'.$attValue->id.'">'.$attValue->value.'
				</option>
				';
        }
        $string = '
			<div class="form-group">
				<label>'.$attValue->name.'</label>
				<select class="select2 select-att" name="attribute" data-placeholder=""  style="width: 100%;">
					'.$att.'
				</select>
				<input type="hidden" value="'.$attValue->name.'" class="label-att">
			</div>';
        return $string;
    }


    public function store(Request $request)
    {
        // add product
         $id = $this->addProduct($request,0);
         // add extends att product
         $this->addAttributeProduct($request,$id);
         // add product list image
        $this->addListImageProduct($request,$id);
        return redirect()->route('product.index');
    }
    private function addProduct(Request $request,$type){
        $status = 0;
        if(Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Manager Product')){
            $status = 1;
        }
        else{
            $status = 0;
        }
        if($type == 0){
            $product = new Product();
            $productId = $product->insertGetId([
                "name"				=> $request->input("name"),
                "quantity"			=> $request->input('quantity'),
                "slug"				=> Str::slug($request->name, '-'),
                "price"				=>  str_replace('.', '', $request->input('price')),
                "discount"			=>  str_replace('.', '', $request->input('discount')),
                "category_id"		=> $request->input('category_id') ,
                "description"		=> $request->input("description") ? $request->input("description"):'',
                "short_description"	=> $request->input("short_description"),
                "type"	=> $type,
                "user_id"	=> Auth::id(),
                "status"	=> $status,
                "created_at"	=> new \DateTime(),
                "updated_at"	=> new \DateTime(),
            ]);
            return $productId;
        }
        $product = new Product();
        $productId = $product->insertGetId([
            "name"				=> $request->input("name"),
            "quantity"			=> 0,
            "slug"				=> Str::slug($request->name, '-'),
            "price"				=> 0,
            "discount"			=> 0,
            "category_id"		=> $request->input('category_id') ,
            "description"		=> $request->input("description") ? $request->input("description"):'',
            "short_description"	=> $request->input("short_description"),
            "type"	=> $type,
            "user_id"	=> Auth::id(),
            "status"	=> $status,
            "created_at"	=> new \DateTime(),
            "updated_at"	=> new \DateTime(),
        ]);
        return $productId;
    }

    private function addListImageProduct($request,$productId){
        // add image array
        $images = $request->file('image_list');
        foreach($images as $image){
			if($request->input('customRadio') == $image->getClientOriginalName()){
				$status = 1;
			}else{
				$status = 0;
			}
			$name = uniqid().'-'.$image->getClientOriginalName();

			$image_resize = Images::make($image->getRealPath());
			$image_resize->resize(445, 445);

			$image_resize->save(public_path('frontend/img/product/'.$name) );
            $productImage = new Image();
			$productImage->insert(
                [
					"product_id" 	=> $productId,
					"image"			=> $name,
					"status"		=> $status,
                    "created_at"	=> new \DateTime(),
                    "updated_at"	=> new \DateTime(),
				]);
			}

        }
        public function addImagesProduct(Request $request){
            $fileName =  uniqid() . '-' . $request->file->getClientOriginalName();
            $request->file->move(public_path('frontend/img/product/'), $fileName);
            return response()->json(["uploaded"=>'/frontend/img/product/'.$fileName]);
        }
    private function addAttributeProduct(Request $request,$productId){
        if($request->has('attribute')){
            foreach ($request->input('attribute') as $att){
				$productValue = new ProductValue();
                $productValue->insert([
                    "product_id"			=> $productId,
                    "attribute_value_id"	=> $att
                ]);

            }
        }
    }
    public function getChildCategory(Request $request){
        $childCate = Category::where('parent',$request->input('cateID'))->get();
        return $childCate;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Đã xóa 2 bảng
        Image::where('product_id', $id)->delete();
        Product::destroy($id);
        return redirect()->back();
    }
    private function getParentCate($cateId){
        $parentCate = Category::where('id', $cateId)->first()->parent;
        return Category::where('id', $parentCate)->first();
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::get();
        $product = Product::Join('categories','categories.id','products.category_id')
                            ->select('products.id',
                                'products.name',
                                'products.price',
                                'products.discount',
                                'products.quantity',
                                'products.description',
                                'products.short_description',
                                'categories.id as category_id',
                                'categories.title as category_title')
            ->where('products.id',$id)->first();
//        $attributesProduct =  Product::leftJoin('product_values','products.id','product_values.product_id')
//                            ->join('attribute_values','product_values.attribute_value_id','attribute_values.id')
//                            ->join('attributes','attribute_values.attribute_id','attributes.id')
//                            ->select('attributes.name',
//                                'attributes.id',
//                                'attribute_values.attribute_id as attribute_id',
//                                DB::raw('count(*) as count'))
//                            ->where('products.id',$id)
//                            ->groupBy('attribute_values.attribute_id',
//                                'attributes.name',
//                                'attributes.id'
//                                )->get();
        $subProducts = Product::Join('sub_products','products.id','sub_products.product_id')
                            ->where('products.id',$id)->get();
        return view('admin.product.edit',compact( 'product','categories','subProducts'));

    }
    public function getAttributeValueProduct(Request $request){
        $attribute_id = $request->get('attribute_id');
        $attributeSelected =  Product::leftJoin('product_values','products.id','product_values.product_id')
            ->join('attribute_values','product_values.attribute_value_id','attribute_values.id')
            ->select('attribute_values.value',
                'attribute_values.id')
            ->where('products.id',$request->get('product_id'))
            ->where('attribute_values.attribute_id',$request->get('attribute_id'))
            ->get();
        $attributeSelectedId =  Product::leftJoin('product_values','products.id','product_values.product_id')
            ->join('attribute_values','product_values.attribute_value_id','attribute_values.id')
            ->select(
                'attribute_values.id')
            ->where('products.id',$request->get('product_id'))
            ->where('attribute_values.attribute_id',$request->get('attribute_id'))
            ->get();
        $attValues = Attribute::Join('attribute_values', 'attributes.id', 'attribute_values.attribute_id')
            ->where('attributes.id', $request->get('attribute_id'))->whereNotIn('attribute_values.id',$attributeSelectedId)->get();
        return $this->renderHtml($attributeSelected,$attValues,$attribute_id);
    }
    private function renderHtml($attributeSelected,$attValues,$attribute_id){
        $string = '';
        $attSelected = '';
        $att= '';
        foreach ($attributeSelected as $attValue){
            $attSelected .= '
				<option value="'.$attValue->id.'" selected>'.$attValue->value.'
				</option>
				';
        }
        foreach ($attValues as $attValue){
            $att .= '
				<option value="'.$attValue->id.'" >'.$attValue->value.'
				</option>
				';
        }
        $string .= '  <div class="form-group">
				<select class="select2" name="attribute" id="attributes" data-placeholder="" style="width: 100%;">
                     '.$attSelected.'
                     '.$att.'
				</select>
				<input type="hidden" id="attribute_id" value="'.$attribute_id.'">
			</div>
			';
        return $string;

    }


    public function checkName($name){

    }


    public function updateAttProduct(Request $request){
        $attributeSelected =  Product::leftJoin('product_values','products.id','product_values.product_id')
            ->join('attribute_values','product_values.attribute_value_id','attribute_values.id')
            ->select('product_values.attribute_value_id')
            ->where('products.id',$request->get('product_id'))
            ->where('attribute_values.attribute_id',$request->get('attribute_id'))
            ->get();
        ProductValue::whereIn('attribute_value_id',$attributeSelected)->delete();
            foreach ($request->get('att_id') as $att){
                ProductValue::join('attribute_values','product_values.attribute_value_id','attribute_values.id')
                    ->join('attributes','attribute_values.attribute_id','attributes.id')
                    ->where('product_id',$request->get('product_id'))
                    ->where('attribute_values.attribute_id',$request->get('attribute_id'))
                    ->insert([
                    "product_values.product_id"=>$request->get('product_id'),
                    "product_values.attribute_value_id"=>$att,
                    "product_values.created_at"=>new \DateTime(),
                    "product_values.updated_at"=>new \DateTime()
                ]);
            }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $idPro = $request->idPro;
        $checkPrice = $request->price - 1 ;
        $request->validate([
            'name' => 'required|unique:products,name,'.$idPro,
            'quantity' => 'required|min:1|numeric',
            'price' => 'required|min:1|numeric',
            'discount' => 'max:'. $checkPrice .'|numeric',
            'short_description' => 'required',
            'category_id' => 'required',
        ],[
            'name.required' => 'Trường này không được để trống!',
            'name.unique' => 'Tên sản phẩm đã tồn tại!',
            'quantity.required' => 'Trường này không được để trống!',
            'quantity.min' => 'Trường này không được để trống!',
            'discount.max' => 'Giá khuyến mãi phải nhỏ hơn giá!',
            'price.required' => 'Trường này không được để trống!',
            'price.min' => 'Trường này không được để trống!',
            'category_id.required' => 'Trường này không được để trống!',
            'short_description.required' => 'Trường này không được để trống!',
        ]);
        $productOld =  Product::where('id',$product->id)->first()->name;
        Product::where('id',$product->id)->update([
            "name"=>$request->input('name'),
            "quantity"=> str_replace('.', '', $request->input('quantity')),
            "slug" => Str::slug($request->name, '-'),
            "price"	=> str_replace('.', '', $request->input('price')),
            "category_id"=> $request->input('category_id') ,
            "discount"=> str_replace('.', '', $request->input('discount')),
            "description"=> $request->input("description"),
            "short_description"=> $request->input("short_description"),
            "created_at"=> new \DateTime(),
            "updated_at"=> new \DateTime(),
        ]);
        $productName = Product::where('id',$product->id)->first()->name;
        foreach (SubProduct::where('product_id',$product->id)->get() as $sub){
            SubProduct::where('id',$sub->id)->update([
                "name"=>str_replace($productOld,$productName,$sub->name)
            ]);
        }
        return redirect()->back()->with('success', 'Cập nhật thông tin sản phẩm thành công!');
    }


    /**-------------
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return 'abc';
    }
    public function anyDatatable(Request $request){
        try{
                $products = Product::Join('categories','categories.id','products.category_id')
                    ->leftJoin('images','images.product_id','products.id')
                    ->where('products.user_id',$request->input('vendor_id'))
                    ->select(
                        'products.id',
                        'products.name',
                        'products.price',
                        'products.discount',
                        'products.quantity',
                        'products.status',
                        'categories.title as category'
                    )
                    ->distinct();
//        if($request->has('vendor_id')){
//            $products = $products->where('user_id',$request->input('vendor_id'));
//        }
                return Datatables::of($products)
                    ->addColumn('action', function ($product){
                        $string = '<a href="">
                                <a href="'.route('product.edit',$product->id).'" class="btn btn-sm btn-primary">Edit</a>
								<a href="'.route('product.destroy',$product->id).'" class="btn btn-sm btn-danger">Delete</a>
                           </a>';
                        return $string;
                    })
                    ->addColumn('image', function ($product){
                        $image = Image::where('product_id',$product->id)->where('status',1)->first();
                        return $image->image;
                    })
                    ->orderColumn('products.id', 'products.id desc')
                    ->make(true);
        }catch (Exception $exception){
            return redirect('admin/product?vendor_id='.Auth::id());
        }

    }

    public function editImageProduct($productId){
        $product = Product::where('id',$productId)->first();
        $productImages = Image::join('products','products.id','images.product_id')
                        ->where('images.product_id',$productId)
                        ->select('images.id as image_id',
                            'products.id',
                            'products.name as product_name',
                            'images.image',
                            'images.status'
                            )->get();
        return view('admin.product.edit_image_product',compact('productImages','product'));
    }
    public function posteditImageProduct(Request $request){
        $image = $request->file('avatar');
        $name = uniqid().''.$image->getClientOriginalName();
            $image_resize = Images::make($image->getRealPath());
            $image_resize->resize(445, 445);
            $image_resize->save(public_path('frontend/img/product/'.$name) );
            $productImage = new Image();
            $productImage->insert(
                [
                    "product_id" 	=> $request->product_id,
                    "image"			=> $name,
                    "status"		=> 0,
                    "created_at"	=> new \DateTime(),
                    "updated_at"	=> new \DateTime(),
                ]);
            return redirect()->back();
    }
    public function getDetailImageProduct(Request $request){
        $productImages = Image::where('id',$request->input('image_id'))->first();
        if($productImages->status == 1){
            $option = '<option value="1" selected>avatar image</option>
                    <option value="0" >thumbnal image</option>';
        }
        if($productImages->status == 0){
            $option = '<option value="1" >avatar image</option>
                    <option value="0" selected>thumbnal image</option>';
        }
        $string = '';
        $string .= '  <div class="form-group">
                        <img src="'.asset('image_upload/'.$productImages->image).'" width="120px" height="120px">
                        <p>'.$productImages->image.'</p>
                        <p>Status</p>
                        <select class="form-control form-control-sm mb-3" name="status_image">
                        '.$option.'
                  </select>
                  <input type="hidden" id="" name="image_id" value="'.$productImages->id.'">
                  <input type="hidden" id="" name="old_image" value="'.$productImages->image.'">
                    </div>

                    ';
        return $string;
    }
    public function updateImageProduct(Request $request)
    {
            if($request->hasFile('image_product')){
                $imageFile = Image::where('id', $request->input('image_id'))->first()->image;
                $path = public_path()."/image_upload/".$imageFile;
                unlink($path);
                $image = $request->file('image_product');
                $name = uniqid() . '-' . $image->getClientOriginalName();
                $image->move(public_path('image_upload'), $name);
                Image::where('id', $request->input('image_id'))->update([
                        "image" => $name,
                        "status" => $request->input('status_image'),
                    ]
                );
                return redirect()->back();
            }
        Image::where('id', $request->input('image_id'))->update([
                "image" => $request->input('old_image'),
                "status" => $request->input('status_image'),
            ]
        );
        return redirect()->back();
    }
    public function deleteImageProduct($imageId)
    {
        Image::where('id', $imageId)->delete();
        return redirect()->back();
    }
    public function addSimpleProduct(Request $request){
        $attributes = Attribute::get();
        $attribute_values = AttributeValue::get();
        $categories = Category::get();

        return view('admin.product.add_simple_product',compact('categories','attributes','attribute_values'));
    }
    public function tabAddProduct(Request $request){
        $attributes = Attribute::get();
        $attribute_values = AttributeValue::get();
        $categories = Category::get();

        return view('admin.product.tab.tab',compact('categories','attributes','attribute_values'));
    }
    public function addProductTab(Request $request){
//        if($_POST['action'] == 1){
            // add product
            $productId = $this->addProduct($request,1);
            // add product list image
            $this->addAvatarImageProduct($request,$productId);
            $productName = Product::where('id',$productId)->first()->name;
            return $productName;
        }
////             add product
//            $productId = $this->addProduct($request,1);
//            // add product list image
//            $this->addAvatarImageProduct($request,$productId);
//            return redirect()->route('product.index');
//    }

    public function addSubProductTab(Request $request){
        $product = Product::orderBy('id','desc')->first();
        $total = 0;
            foreach ( Session::get('sub_products') as $item){
                $subProduct = SubProduct::insertGetId([
                    "product_id"=>$product->id,
                    "name"=>$item['name'],
                    "price"=>$item['price'],
                    "discount"=>$item['discount'],
                    "quantity"=>$item['quantity'],
                ]);
                $productValue = new ProductValue();
                foreach ($item['att_id'] as $att){
                    $productValue->insert([
                        "product_id"			=> $subProduct,
                        "attribute_value_id"	=> $att
                    ]);
                }
                $total += $item['quantity'];
            }
            Product::where('id',$product->id)->update(["quantity"=>$total]);

        $request->session()->forget('sub_products');
        return redirect()->route('product.edit',$product->id);
    }
    public function addSubProductToTable(Request $request){
        $product = Product::orderBy('id','desc')->first();
        $valueAttributeId = array();
        $name = array();
        $idValue = array();
        if($request->has('attribute')){
            foreach ($request->input('attribute') as $att){
                $valueAttributeId[] = $att;
            }
        }
        $valueAttribute = AttributeValue::whereIn('id',$valueAttributeId)->get(['value','id']);
        foreach ($valueAttribute as $atts){
            $name[] = $atts->value;
            $idValue[] = $atts->id;
        }
        if (!empty(Session::get('sub_products'))){
            $sub_products = Session::get('sub_products');
            $item = array();
            $item['name'] = implode('-',$name);
            $item['quantity'] = $request->input('quantity');
            $item['price'] = str_replace('.', '', $request->input('price'));
            $item['discount'] = str_replace('.', '', $request->input('discount'));
            $item['product_id'] = $product->id;
            $item['att_id'] = $idValue;
            $sub_products[] = $item;

            Session::put('sub_products', $sub_products);
        }else{
            $item = array();
            $item['name'] = implode('-',$name);
            $item['quantity'] = $request->input('quantity');
            $item['price'] = $request->input('price');
            $item['discount'] = $request->input('discount');
            $item['product_id'] = $product->id;
            $item['att_id'] = $idValue;
            $items = array();
            $items[] = $item;
            Session::put('sub_products', $items);
        }
        return $this->renderTableSubProduct($request);
    }
    public function renderTableSubProduct ($request){
        if ($request->session()->has('sub_products')){
            $string = '';
            foreach ( Session::get('sub_products') as $item){
                $string .= '  <tr>
                                    <td>'.$item['name'].'</td>
                                    <td>'.$item['quantity'].'</td>
                                    <td>'.$item['price'].'</td>
                                    <td>'.$item['discount'].'</td>
                                </tr>';
            }
            $html = '';
            $html .= '     <div class="col-lg-12 mb-4">
                    <!-- Simple Tables -->
                    <div class="card">

                    <div class="row">
                        <div class="col-md-4">

                        </div>
                           <div class="col-md-5">

                        </div>
                           <div class="col-md-3">
                            <a class="btn btn-warning btn-icon-split btn-sm" onclick="return clearSubProductTable(this);">
                            <span class="icon text-white-50">
                                <i class="fas fa-eraser"></i>
                            </span>
                         <span class="text">Reset</span>
                    </a>
                        </div>
                    </div>


                        <div class="table-responsive">
                            <table id="list-image" class="table align-items-center table-flush">
                                <thead class="thead-light">
                                <tr>
                                    <th>Name</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Discount</th>
                                </tr>
                                </thead>
                                <tbody>
                                    '.$string.'
                                </tbody>
                            </table>
                        </div>



                    </div>
                </div>';
            return $html;
        }
        return  $html = '    <div class="col-lg-12 mb-4">
                    <!-- Simple Tables -->
                    <div class="card">

                    <div class="row">
                        <div class="col-md-4">

                        </div>
                           <div class="col-md-5">

                        </div>
                           <div class="col-md-3">
                            <button type="button" class="btn btn-warning btn-icon-split btn-sm" onclick="return clearSubProductTable(this);">
                            <span class="icon text-white-50">
                                <i class="fas fa-eraser"></i>
                            </span>
                         <span class="text">Reset</span>
                    </button>
                        </div>
                    </div>


                        <div class="table-responsive">
                            <table id="list-image" class="table align-items-center table-flush">
                                <thead class="thead-light">
                                <tr>
                                    <th>Name</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Discount</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>



                    </div>
                </div>';
    }
    public function tabAddSimpleProduct(Request $request){
        $attributes = Attribute::get();
        $attribute_values = AttributeValue::get();
        $categories = Category::get();
        return view('admin.product.tab_simple_product.tab',compact('categories','attributes','attribute_values'));
    }
    public function addSimpleProductTab(Request $request){
        // add product
        $productId = $this->addProduct($request,0);
        // add product list image
        $this->addAvatarImageProduct($request,$productId);
        return redirect()->back()->withInput();
//             add product
    }

    private function addAvatarImageProduct($request,$productId){

        $image = $request->file('image');
        $name = uniqid().'-'.$image->getClientOriginalName();
            $image_resize = Images::make($image->getRealPath());
            $image_resize->resize(445, 445);
            $image_resize->save(public_path('frontend/img/product/'.$name) );
            $productImage = new Image();
            $productImage->insert(
                [
                    "product_id" 	=> $productId,
                    "image"			=> $name,
                    "status"		=> 1,
                    "created_at"	=> new \DateTime(),
                    "updated_at"	=> new \DateTime(),
                ]);

        }
    public function addThumbnailProduct(Request $request){
        $product = Product::orderBy('id','desc')->first();
        $fileName =  uniqid() . '-' . $request->file->getClientOriginalName();

        $image_resize = Images::make($request->file->getRealPath());
        $image_resize->resize(445, 445);
        $image_resize->save(public_path('frontend/img/product/'.$fileName) );
        $productImage = new Image();
        $productImage->insert(
            [
                "product_id" 	=> $product->id,
                "image"			=> $fileName,
                "status"		=> 0,
                "created_at"	=> new \DateTime(),
                "updated_at"	=> new \DateTime(),
            ]);

        return response()->json(["uploaded"=>'/frontend/img/product/'.$fileName]);

    }
    public function clearSubProductFromTable(Request $request){
        $request->session()->forget('sub_products');
        return $this->renderTableSubProduct($request);
    }
    public function updateSubProduct(Request $request){
        SubProduct::where('id',$request->input('product_id'))->update([
            "quantity"=>$request->input('quantity'),
            "price"=>str_replace('.', '', $request->input('price')),
            "discount"=>str_replace('.', '', $request->input('discount')),
        ]);
    }
    public function deleteSubProduct($producId){
        SubProduct::where('id',$producId)->delete();
        return redirect()->back();
    }
    public function getListProductVendor(Request $request){
        return view('admin.product.product_vendor.list');
    }
    public function anyDataTableProductVendor(){
        try{
            $products = Product::Join('categories','categories.id','products.category_id')
                ->join('model_has_roles','model_has_roles.model_id','products.user_id')
                ->join('roles','roles.id','model_has_roles.role_id')
                ->join('vendors','vendors.user_id','products.user_id')
                ->leftJoin('images','images.product_id','products.id')
                ->whereIn('roles.name',['Vendor'])
                ->select(
                    'products.id',
                    'products.name',
                    'products.price',
                    'products.discount',
                    'products.quantity',
                    'products.status',
                    'categories.title as category',
                    'vendors.name_store'
                )
                ->distinct();
//        if($request->has('vendor_id')){
//            $products = $products->where('user_id',$request->input('vendor_id'));
//        }
            return Datatables::of($products)
                ->addColumn('action', function ($product){
                    if($product->status == 2){
                        $string = '
                                <a href="'.route('edit.vendor.product',$product->id).'" class="btn btn-sm btn-primary">Xem</a>
								<a href="" class="btn btn-sm btn-warning "  data-id= '.$product->id.' onclick="return revertStatus(this);">Hủy không duyệt</a>
                           </a>';
                        return $string;
                    }
                    $string = '<a href="">
                                <a href="'.route('edit.vendor.product',$product->id).'" class="btn btn-sm btn-primary">Xem</a>
								<a href="" class="btn btn-sm btn-danger" data-toggle="modal" data-id= '.$product->id.' data-url= '.route('deny.confirm.product').' data-target="#exampleModal" onclick="return denyConfirm(this);">Không duyệt</a>
                           </a>';
                    return $string;
                })
                ->addColumn('image', function ($product){
                    $image = Image::where('product_id',$product->id)->where('status',1)->first();
                    return $image->image;
                })
                ->addColumn('status', function ($product){
                    return $product->status;
                })
                ->orderColumn('products.id', 'products.id desc')
                ->make(true);
        }catch (Exception $exception){
            return redirect('admin/product?vendor_id='.Auth::id());
        }
    }
    public function editProductVendor($id)
    {
        $categories = Category::get();
        $product = Product::Join('categories','categories.id','products.category_id')
            ->select('products.id',
                'products.name',
                'products.price',
                'products.discount',
                'products.quantity',
                'products.status',
                'products.description',
                'products.short_description',
                'categories.id as category_id',
                'categories.title as category_title')
            ->where('products.id',$id)->first();
//        $attributesProduct =  Product::leftJoin('product_values','products.id','product_values.product_id')
//                            ->join('attribute_values','product_values.attribute_value_id','attribute_values.id')
//                            ->join('attributes','attribute_values.attribute_id','attributes.id')
//                            ->select('attributes.name',
//                                'attributes.id',
//                                'attribute_values.attribute_id as attribute_id',
//                                DB::raw('count(*) as count'))
//                            ->where('products.id',$id)
//                            ->groupBy('attribute_values.attribute_id',
//                                'attributes.name',
//                                'attributes.id'
//                                )->get();
        $subProducts = Product::Join('sub_products','products.id','sub_products.product_id')
            ->where('products.id',$id)->get();
        return view('admin.product.product_vendor.edit',compact( 'product','categories','subProducts'));

    }
    public function confirmProductVendor(Request $request){
        if($request->get('action') == 'active'){
            Product::where('id',$request->get('product_id'))->update([
                "status"=>1,
                "is_admin"=>Auth::id(),
                "updated_at"=>new \DateTime()
            ]);
        }
        if($request->get('action') == 'deactivate'){
            Product::where('id',$request->get('product_id'))->update([
                "status"=>0,
                "is_admin"=>Auth::id(),
                "updated_at"=>new \DateTime()
            ]);

        }
    }
    public function getAdminConfirmProduct(Request $request){
        if($request->get('action') == 'viewConfirm'){
            $adminId = Product::where('id',$request->get('product_id'))->first();
            $admin = User::join('model_has_roles','model_has_roles.model_id','users.id')
                ->join('roles','roles.id','model_has_roles.role_id')
                ->where('users.id',$adminId->is_admin)->select('users.name','users.email','roles.name as role')->first();
            return ' <p>Tài khoản: '.$admin->name.'</p>
                    <p> Chức vụ: '.$admin->role.'</p>
                    <p>Hành động: duyệt sản phẩm</p>
                    <p> Thời gian: '.$adminId->updated_at.'</p>';
        }
        $adminId = Product::where('id',$request->get('product_id'))->first();
        $admin = User::join('model_has_roles','model_has_roles.model_id','users.id')
            ->join('roles','roles.id','model_has_roles.role_id')
            ->where('users.id',$adminId->is_admin)->select('users.name','users.email','roles.name as role')->first();
        return ' <p>Tài khoản: '.$admin->name.'</p>
                 <p> Chức vụ: '.$admin->role.'</p>
                 <p>Hành động: không duyệt sản phẩm</p>
                 <p> Thời gian: '.$adminId->updated_at.'</p>';

    }
    public function denyConfirmProduct(Request $request){
        Product::where('id',$request->input('product_id'))->update([
            "status"=>2,
            "is_admin"=>Auth::id(),
            "reason_deny"=>$request->input('reason'),
            "updated_at"=>new \DateTime()
        ]);
//       gửi mail thông báo đến vendor
        return redirect()->back();
    }
    public function revertStatusProduct(Request $request){
        Product::where('id',$request->input('product_id'))->update([
            "status"=>0,
            "is_admin"=>Auth::id(),
            "updated_at"=>new \DateTime()
        ]);
//       gửi mail thông báo đến vendor
    }

}
