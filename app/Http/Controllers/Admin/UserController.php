<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Entity\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Yajra\DataTables\DataTables;

use Illuminate\Http\Request;
use App\Http\Requests\{EditUserRequest,AddUserRequest};

use Spatie\Permission\Models\Role;
class UserController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:user-list|user-edit|user-create|user-delete', ['only' => ['showList']]);
        $this->middleware('permission:user-create', ['only' => ['getAddUser','postAddUser']]);
        $this->middleware('permission:user-edit', ['only' => ['getEditUser','postEditUser']]);
        $this->middleware('permission:user-delete', ['only' => ['getDeleteUser']]);
    }
    // show danh sách
    public function showList(){
        return view('admin.user.list');
    }
    public function getAll(){
        $users = User::join('model_has_roles','users.id','model_has_roles.model_id')
            ->join('roles','roles.id','model_has_roles.role_id')
            ->whereNotIn('roles.name',['Customer','Vendor'])->select('users.id','users.name','users.email','roles.name as role' );

        return Datatables::of($users)->addColumn('action',function ($user){
            $string = '<a href="">
                                <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                           </a>';
            $string .= '<a href="" class="btn btn-danger btnDelete" data-toggle="modal" data-target="#myModalDelete" onclick="">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
            return $string;
        })->make(true);
    }

    // thêm tài khoản
    public function getAddUser(){
        $roles = Role::pluck('name','name')->all();
        return view('admin.user.add',compact('roles'));
    }

    public function postAddUser(Request $request){

//        $user = new User();
////
//        $user->insert([
//            'name'      => $request->input('nameAdmin'),
//            'email'     => $request->input('email'),
//            'password'  => bcrypt($request['password']),
//            'role'      => '3',
//        ]);
        $user = User::create([
            'name'=> $request->input('nameAdmin'),
            'email'     => $request->input('email'),
            'password'  => bcrypt($request->input('password'))
            ]);
//        $users = User::permission('role-create')->get();
//        dd(Auth::user()->hasPermissionTo('role-create', 'web'));

//        $users = User::role('Admin')->get();
//        var_dump($users);
        $user->assignRole($request->input('roles'));
//        return redirect('admin/user/list')->with('message','Tạo tài  khoản thành công');

    }

    // xóa tài khoản
    public function getDeleteUser($id){
        $del = User::find($id);
        $del ->delete();
        return redirect('admin/user/list')->with('message','Xóa tài khoản thành công.');
    }

    // sửa tài khoản
    public function getEditUser(){
        $user = User::where('id', Auth::id())->first();
        return view('admin.user.edit',compact('user'));
    }

    public function postEditUser(EditUserRequest $request, $id){
        $user =User::where('id',$id);
        $user->update([
            'name'      =>$request->input('nameAdmin'),
            'email'     =>$request->input('email'),
            'password'  => bcrypt($request['password']),

        ]);
        return redirect('admin/user/list')->with('message','Cập nhập tài khoản thành công.');
    }

}
