<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\AttributeValue;

class ValueController extends Controller
{
    //
	public function delete($id){
		
		$value = AttributeValue::findOrFail($id)->delete();
		
		return back();
	}
}
