<?php

namespace App\Http\Controllers\Admin;

use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Yajra\Datatables\Datatables;
use App\Http\Requests\LoginAdminRequest;
class LoginController extends Controller
{
    public function showLoginForm(){
        return view('admin.login.login');
        
    }

    public function login(LoginAdminRequest $request){
        $data = [
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ];

        if (Auth::attempt($data)) {
            return redirect()->route('admin.home');
        } else {
            //return redirect()->back()->with('status', 'Email hoặc Password không chính xác');
			return 'Email hoặc Password không chính xác';
        }
    }
    public function logOut(){
        Auth::logout();
        return redirect()->route('admin.login');
    }
}
