<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Address;
use App\Entity\Image;
use App\Entity\OrderItem;
use App\Entity\Product;
use App\Entity\SubProduct;
use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Order;

class OrderController extends Controller
{
    //
    public function listorder(){
        $orders 		= Order::leftJoin('users', 'orders.user_id', '=', 'users.id')
            ->select('orders.*', 'users.name')
            ->Where('status','<',5)->paginate(5);
        $order_items	= OrderItem::get();
        return view('admin.order.list_order',compact('orders','order_items'));
    }
    public function listorderfinish(){
        $orders 		= Order::leftJoin('users', 'orders.user_id', '=', 'users.id')
            ->select('orders.*', 'users.name')
            ->Where('status','>=',5)->paginate(5);
        $order_items	= OrderItem::get();
        return view('admin.order.list_order_finish',compact('orders','order_items'));
    }

	public function updateStatus($id) {

		$order = Order::where('id', $id)->firstOrFail();
        $order_items = OrderItem::Where('order_id',$order->id)->firstOrFail();
        $product = Product::Where('id',$order_items->product_id)->firstOrFail();

		if($order->status < 3){
			$order->status = $order->status + 1;
			$order->save();
		}
        elseif($order->status =4){
		    if($product->type == 0){
                $product->quantity = $product->quantity-$order_items->quantity;
                $product->save();
            }
            $Subproduct = SubProduct::where('product_id', $product->id)->firstOrFail();
            $Subproduct->quantity = $Subproduct->quantity-$order_items->quantity;
            $order->status = $order->status + 1;
            $order->save();
            $Subproduct->save();
        }
        return redirect('admin/order/list_order');
		return json_encode($order);
	}

	public function revertUpdate($id) {
		$order = Order::where('id', $id)->firstOrFail();

		if($order->status > 0){
			$order->status = $order->status - 1;
			$order->save();
		}

		return json_encode($order);
	}
	public function deleteOrder($id){
        $faq = Order::where('id', $id)->firstOrFail()->delete();

        return redirect('admin')->with('success' ,'Xóa câu hỏi thành công!');
    }
//    Chi tiết Order
    public function detailOrder(Request $request,$id){
        $orders    = Order::where('id',$id)->first();
        $order_item = OrderItem::Where('order_id',$id)->first();
        $user = User::Where('id',$orders->user_id)->first();
        $address = Address::where('id', $orders->address_bill_id)->first();
        $image = Image::Where('product_id',$order_item->product_id)->Where('status',1)->first();
        return view('admin.order.detail_order',compact('orders','order_item','user','address','image'));
    }
}
