<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Entity\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Session;

class CategoryController extends Controller
{
    //
    function __construct()
    {
        $this->middleware('permission:category-add', ['only' => ['formAdd']]);
    }
	public function getAll(){
		$categories = Category::get();

		return view('admin.category.list', [
			'categories' => $categories
		]);
	}

	public function formAdd(){
//        $categories = Category::where('parent', '0')->get();
        $categories = Category::get();
//        $cate = $this->showCategories($categories, 0);
		return view('admin.category.add', [
			'categories' => $categories
		]);
	}

    public function showCategories($categories, $parent_id )
    {
        //BƯỚC 2.1: LẤY DANH SÁCH CATE CON
        $categories_child = array();
        foreach ($categories as $item)
        {
            // Nếu là chuyên mục con thì hiển thị
            if ($item->parent == $parent_id)
            {
                $categories_child[] = $item;
//                unset($categories[$key]);
            }
        }
		//BƯỚC 2.2: HIỂN THỊ DANH SÁCH CHUYÊN MỤC CON NẾU CÓ
        $string = '';
        if ($cate_child)
        {
            echo '<ul id="treeview" class="tree-view">';
            foreach ($cate_child as  $item)
            {

                // Hiển thị tiêu đề chuyên mục
                echo '<li data-icon-cls="fa fa-inbox" data-expanded="true"> <span style="display: none" class="cate-id">'. $item->id.' </span>'.$item->title;

                // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
                $this->showCategories($categories, $item->id);
                echo  '</li>';
            }
            echo '</ul>';
        }
    }
	public function submitAdd(CategoryRequest $request){

		//To-do: Validation
		//	required
		//
		$category = new Category;

		$slug	= Str::slug($request->title, '-');
		$title	= Str::title($request->title);
		$category->title	= $title;
		$category->slug		= $slug;
		$category->parent	= $request->parent;
		$category->status	= 1;
		$category->save();
        Session::put('message', 'Thêm mới danh mục thành công !');
		return redirect()->route('category.add');
	}

	public function delete($id){
//		$category = Category::findOrFail($id);
//
//		$category->delete();
		Category::destroy($id);
        Session::put('message','Xóa danh mục thành công!');
		return back();
	}

	public function formEdit($id){
		$categories = Category::where('parent', '0')->get();
		$category = Category::findOrFail($id);

		return view('admin.category.edit', [
			'categories' => $categories
		]);
	}

	public function submitEdit(Request $request, $id){

		//To-do: Validation
		//	required, parent !== $id
		//

		$category = Category::findOrFail($id);

		$slug	= Str::slug($request->title, '-');
		$title	= Str::title($request->title);

		$category->title	= $title;
		$category->slug		= $slug;
		$category->parent	= $request->parent;

		$category->save();

		return redirect()->route('category.list');
	}
	public function detailCategory(Request $request){
	    $cate = Category::where('id',$request->get('cateId'))->first();
	    $str = '';
	    if($cate->status == 1){
	        $str .= '<input type="checkbox" class="custom-control-input" id="customSwitch'.$cate->id.'" name="checkbox_cate" onclick="return addStatus(this)" checked>
                        <label class="custom-control-label" for="customSwitch'.$cate->id.'" ><span>show</span></label>
                        <input type="hidden" class="form-control" name="status" value="1">
                        ';
        }else{
            $str .= '<input type="checkbox" class="custom-control-input" id="customSwitch'.$cate->id.'" name="checkbox_cate" onclick="return addStatus(this)" >
                        <label class="custom-control-label" for="customSwitch'.$cate->id.'" ><span>hide</span></label>
                           <input type="hidden" class="form-control" name="status" value="0">';
        }
	    $string = '';

	    return $string .= '
			<div class="form-group">
				<label for="title">Title</label>
				<input type="text" class="form-control" id="title" name="title_cate" value="'.$cate->title.'">
				<input type="hidden" class="form-control" name="cate_id" value="'.$cate->id.'">
				<span id="error-title-edit" class="error-text"></span>

			</div>
            <div class="form-group">
            	<label>Status</label>
				<div class="custom-control custom-switch">
				'.$str.'
            	</div>
            	<input type="hidden" class="form-control" name="status" value="">
			</div>
			<div>
			<button type="button" class="btn btn-primary"  onclick="return updateCate(this)">Save changes</button>
            	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>';
    }
    public function updateCategory (Request $request){
	    $category = Category::find($request->cateId);
        if($category->title != $request->title)
        {
            $error = Category::where('title',$request->title)->get();
            return response()->json( ['error' => $error] );
        }
        Category::where('id',$request->get('cateId'))
                ->update([
                    "title"=>$request->input('title'),
                    "status"=>$request->input('status')
                ]);

        return response()->json(['error' => []]);
    }

    public function changeLineCate($cateId){
        $result = array();
        $string = '';
        $categories = Category::get();
        $category = Category::where('id',$cateId)->first();
//        if($category->parent == 0){
//            $string = '';
//            $string .= ' <span>'.$category->title.' <i class="fas fa-angle-right"></i></span>';
//            return $string;
//        }
        foreach ($categories as $cate){
            if($cate->id == $category->parent) {
                $result[] = $cate->title;
//                $result[] = $category->title;
                $item = $this->changeLineCate($cate->id);
                array_merge($result,(array)$item);
            }
        }
        foreach ($result as $r){
            echo $string .= ' <span>'.$r.' <i class="fas fa-angle-right"></i></span>';
        }

//        echo $string. '<span>'.$category->title.'</span>';
    }

}
