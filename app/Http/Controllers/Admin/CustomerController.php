<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Vendor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\User;
use App\Http\Requests\{CustomerRequest};
use Yajra\DataTables\DataTables;
use App\Entity\Attribute;
use App\Entity\AttributeValue;
use App\Entity\Category;
use App\Entity\Product;
use App\Entity\Image;
use App\Entity\ProductImage;
use App\Entity\ProductValue;
use App\Entity\SubProduct;
use Illuminate\Support\Facades\DB;
use App\Mail\ConfirmVendorEmail;
use App\Mail\DeleteConfirmVendorEmail;
use Illuminate\Support\Facades\Mail;
//
/*
	Show danh sách customer
	Chặn/bỏ chặn
*/
class CustomerController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:vendor-list|vendor-detail|', ['only' => ['showListVendor']]);
        $this->middleware('permission:vendor-detail', ['only' => ['showDetailVendor']]);
       
        
    }
    //Danh sách tài khoản khách hàng
    public function listcustomer(){

        $users =  User::join('model_has_roles','users.id','model_has_roles.model_id')
            ->join('roles','roles.id','model_has_roles.role_id')
            ->whereIn('roles.name',['Customer'])
            ->select('users.id','users.name','users.email','users.phone','users.address' )->get();
		return view('admin.customer.list_customer',[
			'users' => $users
		]);
        //add tài khoản khách hàng
    }
    public function getaddcustomer(){
        return view('admin.customer.add_customer');
    }
    public function postaddcustomer(CustomerRequest $req){

            $user = new User();
            $user ->name 		= $req->name;
            $user ->email 		= $req->email;
            $user ->password 	= bcrypt($req['password']);
            $user ->phone 		= $req->phone;
            $user ->address 	= $req->address;
            $user ->role 		= 1;
			$user ->status		= 1;
            $user -> save();
            return redirect()->back()->with('message','Tạo tài khoản thành công');
    }
    //Khóa tài khoản khách hàng
    public function locklickcustomer(){
        $userlock = User::all()->where('role',4);
        return view('admin.customer.lock_customer',compact('userlock'));
    }
    //Lock Customer
    public function lockCustomer($id){
        $edit_Customer = User::find($id);
        $edit_Customer->role = '4';
        $edit_Customer->save();
        return redirect('admin/lock_list_customer');
    }
    // get list vendor
    public function showListVendor(){
        return view('admin.customer.vendor.list');
    }
    public function dataTableVendor(){
        $vendors = Vendor::select('id','user_id','name_representative','name_store','email','phone','address','status');

        return Datatables::of($vendors)->addColumn('status',function ($vendor){
            return $vendor->status;
        })->addColumn('action',function ($vendor){
            $string = '<a href="'.route('detail.vendor',$vendor->id).'">
                                <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                           </a>';
            $string .= '<a href="'.route('delete.vendor', $vendor->id).'" class="btn btn-danger btnDelete" >
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
            return $string;
        })
            ->make(true);
    }
    public function showDetailVendor($vendorId){
        $vendor = Vendor::where('id',$vendorId)->first();
        return view('admin.customer.vendor.edit',compact('vendor'));
    }
    public function confirmVendor(Request $request){
        $vendor = Vendor::where('id',$request->get('vendor_id'))->first();

        if($request->get('action') == 'active'){
            Vendor::where('id',$request->get('vendor_id'))->update([
                "status"=>1
            ]);
            $user = User::find($vendor->user_id);
            DB::table('model_has_roles')->where('model_id',$vendor->user_id)->delete();
            $user->assignRole('Vendor');
            Mail::to($vendor->email)->send(new ConfirmVendorEmail);
            
        }
        if($request->get('action') == 'deactivate'){
            Vendor::where('id',$request->get('vendor_id'))->update([
                "status"=>0
            ]);
            $user = User::find($vendor->user_id);
            DB::table('model_has_roles')->where('model_id',$vendor->user_id)->delete();
            $user->assignRole('Customer');
            Mail::to($vendor->email)->send(new DeleteConfirmVendorEmail);
        }
        
    }

    public function deleteVendor($vendorId){
        $deleteVendor = Vendor::where('id', $vendorId);
        $deleteVendor->delete();
        return redirect()->back();
    }
}
