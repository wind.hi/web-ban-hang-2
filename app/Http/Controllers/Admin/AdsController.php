<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Ads;
use Yajra\DataTables\DataTables;
use Intervention\Image\ImageManagerStatic as Images;


class AdsController extends Controller
{
    public function addAds(){
        return view('admin.ads.add');
    }
    public function dtAddAds(Request $request){
        $addAds = new Ads();
        $addAds->ads_name       = $request->input('adsName');
        $addAds->vendor_name    = $request->input('adsVendor');
        $addAds->link           = $request->input('link');
        $addAds->phone          = $request->input('phone');
        $addAds->status         = $request->input('status');

        if ($request->hasFile('file')){
            $fileName = $request->file('file')->getClientOriginalName();
            $image_resize = Images::make($request->file('file')->getRealPath());
            $image_resize->resize(270, 432);
            $image_resize->save(public_path('image_upload'.$fileName));
//            $request->file('file')->move('image_upload',$fileName);
            $addAds->image = $fileName;
        }
        $addAds->save();
        return redirect()->back();
    }
    public function editAds($id){
        $editAds = Ads::where('id',$id)->first();
        return view('admin.ads.edit',[
            'editAds'  =>  $editAds,
        ]);
    }
    public function updateAds(Request $request,$id){
        $updateAds =Ads::where('id',$id);
        $fileName = $updateAds->first()->image;

        if($request->hasFile('file')){
            $path = public_path().'/image_upload'.$fileName;
            unlink($path);

            $fileName = $request->file('file')->getClientOriginalName();
            $image_resize = Images::make($request->file('file')->getRealPath());
            $image_resize->resize(270, 432);
            $image_resize->save(public_path('image_upload'.$fileName));
        }
            

        $updateAds->update([
            'ads_name'      =>$request->input('adsName'),
            'vendor_name'     =>$request->input('adsVendor'),
            'image'  => $fileName,
            'link'  => $request->input('link'),
            'phone'  => $request->input('phone'),
            'status' => $request->input('status')
        ]);

        return redirect()->back();
    }
    public function deleteAds($id){
        $deleteAds = Ads::where('id', $id);
        $deleteAds->delete();
        return redirect()->back();
    }
    public function listAds(){
        return view('admin.ads.list');
    }
    public function dtListAds(){
        $ads = Ads::select( 'id','ads_name','vendor_name','image','link','phone','status');
        return Datatables::of($ads)
            ->addColumn('action', function ($row){
                return  '<a href="'.route('ads.edit',$row->id).'" class="btn btn-sm btn-primary">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                         </a>
                         <a href="'.route('ads.delete',$row->id).'" class="btn btn-sm btn-danger">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                         </a>';
            })
            ->make(true);
    }
    // show ADS trang chủ
}
