<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Entity\Attribute;
use App\Entity\AttributeValue;
use App\Http\Requests\AttributeRequest;
use App\Http\Requests\AttributeEditRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Session;

class AttributeController extends Controller
{
    //
    function __construct()
    {
        $this->middleware('permission:attribute-list|attribute-edit|attribute-create|attribute-delete', ['only' => ['getAll']]);
        $this->middleware('permission:attribute-create', ['only' => ['formAdd','submitAdd']]);
        $this->middleware('permission:attribute-edit', ['only' => ['formEdit','submitEdit']]);
        $this->middleware('permission:attribute-delete', ['only' => ['delete']]);
    }
	public function getAll(){
		$attributes = Attribute::get();
		$attribute_values = AttributeValue::get();

		return view('admin.attribute.list', ['attributes' => $attributes, 'values' => $attribute_values]);
	}
	public function formAdd(){
		return view('admin.attribute.add');
	}

	public function submitAdd(AttributeRequest $request){
		$attribute = new Attribute;
		$attribute -> name = $request->name;
		$attribute -> save();

		$values = explode(';', $request->value);

		foreach($values as $value){
			$attribute_value = new AttributeValue;

			$attribute_value -> attribute_id	= $attribute->id;
			$attribute_value -> value			= $value;

			$attribute_value -> save();
		}
        Session::put('message','Thêm mới thuộc tính thành công !');
		return redirect()->route('attribute.list');
	}

	public function delete($id){

		$atrribute = Attribute::findOrFail($id)->delete();

		$values = AttributeValue::where('attribute_id', $id)->delete();
        Session::put('message','Xóa thành công!');
		return back();
	}

	public function formEdit($id){

		$attribute = Attribute::findOrFail($id);
		$attribute_values = AttributeValue::where('attribute_id', $id)->get();
		return view('admin.attribute.edit', ['attribute' => $attribute, 'values' => $attribute_values]);
	}

	public function submitEdit($id,AttributeEditRequest $request){
	    $attribute = Attribute::find($id);

	    if($attribute->name != $request->name)
	    {
            $request->validate([
                'name' => 'unique:attributes,name'
            ],[
                'name.unique' => 'Tên thuộc tính đã tồn tại!'
            ]);
        }
		$attribute = Attribute::findOrFail($id);

		$attribute -> name = $request->name;
		$attribute -> save();

		if($request->value){
			$values = explode(';', $request->value);

			foreach($values as $value){
				$attribute_value = new AttributeValue;

				$attribute_value -> attribute_id = $id;
				$attribute_value -> value		 = $value;
				$attribute_value -> save();
			}
		}
        Session::put('message','Cập nhật thuộc tính thành công !');
		return back();
	}
}
