<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Info;
use App\Http\Controllers\Controller;
use App\Http\Requests\InfoRequest;
use App\Http\Requests\InfoUpdateRequest;
use http\Env\Request;
use Session;

class InfoController extends Controller
{
	public function show(){
		$info = Info::get()->first();
		return view('admin.info.list', ['info'=>$info]);

	}

	public function store(InfoRequest $infoRequest)
    {
        $data = $infoRequest->except('_token');
        $data['created_at'] = $data['updated_at'] = now();
        Info::insert($data);
        Session::put('message', 'Thêm mới thành công !');
        return redirect()->route('info.show');
    }

	public function edit(InfoUpdateRequest $request){
		$info = Info::get()->first();

		$info -> phone 		= $request->phone;
		$info -> email 		= $request->email;
		$info -> address 	= $request->address;
		$info -> about 		= $request->about;
		if($request->hasFile('logo')){
            $file = $request->file('logo');
            $logo = uniqid()."-".$file->getClientOriginalName();
            $file->move('../public/frontend/img/info', $logo);
            $info -> logo 		= $logo;
        }

		$info -> save();
        Session::put('message', 'Cập nhật thành công !');
		return back();
	}
}
