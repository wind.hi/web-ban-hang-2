<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Slider;
use App\Entity\Product;
use Illuminate\Support\Facades\Storage;
use Session;

class SliderController extends Controller
{
    //

	public function getAll(){
		$sliders 	= Slider::get();
		$products 	= Product::get();

		return view('admin.slider.list',compact('sliders','products'));
	}

	public function submitAdd(Request $request){

	    $request->validate([
	        'title' => 'required|unique:sliders,title',
            'product' => 'required',
            'slider' => 'required',
            'description' => 'required'
        ],[
            'title.required' => 'Trường này không được để trống',
            'title.unique' => 'Tên slider đã tồn tại !',
            'product.required' => 'Trường này không được để trống',
            'slider.required' => 'Trường này không được để trống',
            'description.required' => 'Trường này không được để trống',
        ]);

		$file = $request->file('slider');
		$name = uniqid()."-".$file->getClientOriginalName();

        $file->move('../public/frontend/img/slider', $name);

		$slider = new Slider;
		$slider -> image 		= $name;
		$slider -> id	= $request->product;
		$slider -> title		= $request->title;
		$slider -> description	= $request->description;
		$slider -> save();
        Session::put('message','Thêm mới thành công!');
		return back();
	}

	public function delete($id){
		$slider = Slider::findOrFail($id)->delete();
		Session::put('message','Xóa thành công!');
		return back();
	}
}
