<?php

namespace App\Http\Controllers\Admin;

use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use DB;
use Spatie\Permission\Models\Permission;
class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index']]);
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }
    public function index(Request $request)
    {
//        dd(Auth::user()->hasPermissionTo('role-edit', 'web'));
        $roles = Role::orderBy('id','DESC')->get();
        return view('admin.role.list',compact('roles'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::get();
        return view('admin.role.create',compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = Role::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));
//        return redirect()->route('roles.index')
//            ->with('success','Role created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return 'edit';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::find($id);
        if($request->has('role_name')){
            $role->name = $request->input('role_name');
        }
        $role->save();
        $role->syncPermissions($request->input('permission'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function showRole(Request $request){
        $role = Role::find($request->input('role_id'));
        $permissions = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$request->input('role_id'))
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();
        $string2 = '';
        foreach($permissions as $permission){
           $string2 .= '<div class="custom-control custom-checkbox">
                             <input type="checkbox" class="custom-control-input" id="'.$permission->id.'" name="permission[]" value="'.$permission->id.'" '.$this->checkPermissionInRole($rolePermissions,$permission->id).'>
                              <label class="custom-control-label" for="'.$permission->id.'">'.$permission->name.'</label>
                                    </div>' ;
        }

        $string = '';
        return $string .= '

            <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Permissions '.$role->name.' </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="row" style="margin-top: 10px">
                <div class="col-md-5">
                     <div style="margin-left: 10px">
                          <span id="span-role">Role: <span id="name-role">'.$role->name.'</span> </span> &nbsp;<a id="edit-name-role" data-id="'.$role->id.'" onclick="return editNameRole(this);"><i class="fa fa-pencil-square-o fa-1x"  aria-hidden="true" ></i></a>
                        </div>
                </div>
                  <div class="col-md-5">
                </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                       <div>
                                <span style="margin-left: 10px">Menu</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div>
                         <span>Permissions</span>
                        </div>
                        '.$string2.'
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>

                ';
    }
    private function checkPermissionInRole($rolePermissions,$id){
        if(in_array($id,$rolePermissions)){
          return 'checked';
        }
    }
}
