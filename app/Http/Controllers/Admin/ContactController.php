<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Entity\Contact;
use Illuminate\Http\Request;
use Session;
use Mail;

class ContactController extends Controller
{
    public function index()
    {
        $contacts = Contact::all();

        return view('admin.contact.index',compact('contacts'));
    }

    public function action($action,$id)
    {
        if($id)
        {
            $contacts = Contact::find($id);
            switch ($action)
            {
                case 'delete':
                    $contacts->delete();
                    Session::put('message', 'Xóa liên hệ thành công!');
                    break;
                case 'status':
                    if($contacts->status == 1){
                        $contacts->status = 2;
                    }else{
                        $contacts->status = 1;
                    }
                    $contacts->save();
                    Session::put('message', 'Đã cập nhật trạng thái liên hệ!');
                    break;
            }
        }
        return redirect()->back();
    }

    public function replyContact($id)
    {
        $contact = Contact::find($id);
        return response()->json(['success' => $contact]);
    }

    public function sendMailReplyContact($email,$title,$content)
    {

        $data =[
            'content' => $content
        ];
        Mail::send('admin.contact.mail',$data,function ($message) use ($email,$title) {

            $message->to($email);
            $message->subject($title);

        });
        Session::put('message','Trả lời liên hệ thành công !');
        return redirect()->back();
    }
}
