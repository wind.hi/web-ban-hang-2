<?php

namespace App\Http\Controllers\Email;

use App\User;
use App\Mail\WelcomeEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Auth;


class WelcomeEmailController extends Controller
{
    public function sendWelcomeEmail()
    {
        $user = Auth::user()->email;
        Mail::to($user)->send(new WelcomeEmail);
        return view('email.WelcomeEmail');
    }
}
