<?php

namespace App\Http\Controllers\Email;

use Illuminate\Http\Request;
use App\User;
use App\Mail\EmailRegister;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Auth;
use App\Entity\Email;




class EmailRegisterController extends Controller
{
    //

    public function EmailRegister(Request $request)
    {
        
        Mail::to($request['email'])->send(new EmailRegister);
        $data=Email::where("email", $request->email);
        if (empty($request))
             {
              $email = new Email();
              $email->email=$request['email'];
              $email->save();
             }
        return redirect()->back();
        
    }
}
