<?php

namespace App\Http\Controllers\Site;

use App\Entity\WishList;
use App\Entity\Product;
use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class WishlistController extends Controller
{
    //

	public function list(){
		$user_id = Auth::id();
		$wish_lists = WishList::where('wishlists.user_id', $user_id)
                    ->join('products', 'wishlists.product_id', '=', 'products.id')
                    ->join('images', 'images.product_id', '=', 'products.id')
					->where('images.status', 1)
                    ->select('wishlists.*', 'products.name', 'images.image')
                    ->get();
		return view('site.user.wishlist', [
			'wish_lists' => $wish_lists
		]);
	}
	public function toggle($product_id){
		$user_id = Auth::id();
		if($user_id == null){
			return "Unauth";
			die;
		}

		if($user_id == null){
			return "Unauth";
			die;
		}

		$wishList = WishList::where('product_id', $product_id)
								->where('user_id', $user_id)
								->exists();

		if($wishList){
			WishList::where('product_id', $product_id)
								->where('user_id', $user_id)
								->delete();

			return "Deleted";
		}else{
			$wishList = new WishList;

			$wishList -> product_id	= $product_id;
			$wishList -> user_id	= $user_id;

			$wishList -> save();

			return "Added";
		}
	}

    public function deleteWishlist($id) {
	    $wishlist =  WishList::destroy($id);
        return response()->json(['success' => "Sản phẩm đã xóa khỏi danh sách yêu thích"]);
    }
}
