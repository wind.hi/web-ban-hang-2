<?php
namespace App\Http\Controllers\Site;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Image;
use App\Entity\Product;
use App\Entity\Rate;
use App\Entity\Slider;
use App\Entity\Ads;
use App\Entity\Wishlist;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends FrontendController
{
    public function home(){
        $productImages = Image::where('status','1')->get();
        $sliders = Slider::get();
        $newProducts = Product::orderBy('id','desc')->get();
        $saleProducts = Product::where('discount','>','0')->get();
        $topView = Product::orderBy('view','desc')->get();
        $reviews = Comment::get();
        $rating = Rate::get();
        $ads = Ads::where('status', '0')->limit(1)->get();
        $productLove = Product::join('wishlists','wishlists.product_id','products.id')->select(
                    'products.id',
                    'products.name',
                    'products.slug',
                    'products.price',
                    'products.type'
                )->get();
        return view('site.home.index',
           [
                'sliders'       =>  $sliders,
                'newProducts'   =>  $newProducts,
                'saleProducts'  =>  $saleProducts,
                'productImages' =>  $productImages,
                'topView'       =>  $topView,
                'rating'        => $rating,
                'reviews'       => $reviews,
                'ads'           => $ads,
                'productLove'  => $productLove

            ]);
    }



    // Phần tìm kiếm theo danh mục sản phâm
    public function getSearch(){
        return view('site.search.search');
    }

    public function postSearch(Request $request){
        $images = Image::where('status','1')->get();
        $catesId = $this->getParent($request->catsearch);
        $products = Product::where('name','like','%'.$request->keysearch.'%')
            ->whereIn('category_id',$catesId)->get();
        return view('site.search.search',compact('images','products'));

    }

    // đệ quy tìm tất cả các danh mục con trong danh mục cha
    public function getParent($cateId ){
        $arr = [];
        $categories = Category::get();
        foreach ($categories as $cate){
            if($cate['parent'] == $cateId){
                $arr[] = $cate['id'];
                $item = $this->getParent($cate['id']);
                array_merge($arr,[$item]);
            }
        }
        return ($arr);
    }
}
