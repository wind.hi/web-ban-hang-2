<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Category;
use App\Entity\Image;
use App\Entity\Product;
use App\Entity\Slider;
class ProductTypeCotroller extends Controller
{
    public function getproductType($id){
        $category = category::all();
        $productImages = Image::where('status','1')->get();
        $loai_sp = Product::where('category_id',$id)->paginate(12);
        return view('site.product.product_type',compact('loai_sp','productImages'));
    }
}
