<?php

namespace App\Http\Controllers\Site;

use App\Entity\Info;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class FrontendController extends Controller
{
    public function __construct()
    {
        $info = Info::first();
        View::share('info', $info);
    }
}
