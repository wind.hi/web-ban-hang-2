<?php

namespace App\Http\Controllers\Site;

use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;
use App\Entity\SubProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Gloudemans\Shoppingcart\Facades\Cart;

class OrderController extends Controller
{
    public function createOrder(Request $request){
        $orderId = $this->addOrder($request);
        $this->addOrderItem($request,$orderId);
        Cart::destroy();
    }
    private function addOrder(Request $request){
            return $orderID = Order::insertGetId([
                "user_id"=>Auth::id(),
                "address_deli_id"=>$request->input('address_deli_id'),
                "address_bill_id"=>$request->input('address_bill_id'),
                "method"=>$request->input('method'),
                "phone"=>$request->input('phone'),
                "total_price"=>$request->input('total_price'),
                "status"=>0,
                'created_at'=>new \DateTime(),
                'updated_at'=>new \DateTime()
            ]);

    }
    private function addOrderItem(Request $request,$orderId){
        foreach(\Gloudemans\Shoppingcart\Facades\Cart::content() as $item){
                OrderItem::insert([
                    "order_id"=>$orderId,
                    "product_id"=>$item->id,
                    "product_name"=>$item->name,
                    "product_image"=>$item->options->image,
                    "price"=>$item->price,
                    "quantity"=>$item->qty,
                    'created_at'=>new \DateTime(),
                    'updated_at'=>new \DateTime()
                ]);
            }
    }

    public function getOrders(Request $request){
        $orders = Order::where('user_id',Auth::id())->get();
        return view('site.order.my_order',compact('orders'));
    }
    public function getOrderItems(Request $request){
        $items = OrderItem::join('orders','order_items.order_id','orders.id')
                            ->where('order_items.order_id',$request->input('order_id'))
                            ->select('orders.id as order_id',
                                'orders.total_price',
                                'orders.status',
                                'order_items.product_name',
                                'order_items.product_id',
                                'order_items.product_image',
                                'order_items.price',
                                'order_items.quantity')
                            ->get();
        $string = '';
        foreach ($items as $item){
            $string .= $this->renderHtmlCart($item);
        }
        return $string;

    }
    public function getTotalPrice(Request $request){
        $order = Order::where('id',$request->input('order_id'))->first();
        return json_encode($order);
    }
    private function renderHtmlCart($item){
        return '  <tr>
                        <td class="cart-product">
                            <a href="#"><img alt="Blouse" src="'.asset('frontend/img/product/'.$item->product_image).'"></a>
                        </td>
                        <td class="cart-description">
                            <p class="product-name"><a href="#">'.$item->product_name.'</a></p>

                        </td>
                        <td class="cart-avail"><span class="label label-success">In stock</span></td>
                        <td class="cart-unit">
                            <ul class="price text-right">
                                <li class="price">'.number_format($item->price,'0','.',',').' vnd</li>
                            </ul>
                        </td>
                        <td class="cart_quantity text-center">
                            <div class="cart-plus-minus-button">
                                <input class="cart-plus-minus" type="text" name="qtybutton" value="'.$item->quantity.'" readonly>
                        </td>

                        <td class="cart-total">
                            <span class="price">'.number_format($item->price * $item->quantity,'0','.',',').' vnd</span>
                        </td>
                    </tr>';
    }
    public function cancelOrder(Request $request){
        Order::where('id',$request->input('order_id'))->update([
            "status"=>6,
            "updated_at"=>new \DateTime()
        ]);
    }
    public function rollbacklOrder(Request $request){
        Order::where('id',$request->input('order_id'))->update([
            "status"=>0,
            "updated_at"=>new \DateTime()
        ]);
    }
}
