<?php

namespace App\Http\Controllers\Site;

use App\Entity\Address;
use App\Entity\Product;
use App\Entity\SubProduct;
use App\Http\Controllers\Controller;
use App\User;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function showCart(Request $request){
        return view('site.cart.cart');
    }
    public function addtoCart(Request $request){
        $subid = $request->input('product_id');
        $subProduct = SubProduct::where('id',$subid)->first();
        if(!empty($subProduct)){
            $price = $subProduct->price;
            if($subProduct->discount > 0){
                $price = $subProduct->discount;
            }
            $quantity = $request->input('quantity');
            $parentProductId = SubProduct::where('id',$subid)->first()->product_id;
            $parentProduct = Product::findProductById($parentProductId);
            // get attribute value sub product
            $attValues = SubProduct::leftJoin('product_values','product_values.product_id','sub_products.id')
                ->join('attribute_values','attribute_values.id','product_values.attribute_value_id')
                ->join('attributes','attribute_values.attribute_id','attributes.id')
                ->where('sub_products.id',$request->input('product_id'))
                ->get(['attribute_values.value','attributes.name']);
            $arrayAtt = array();
            foreach ($attValues as $att){
                $arrayAtt[$att->name] = $att->value;
            }

            Cart::add(array(
                'id'        => $subid,
                'name'      => $parentProduct->name,
                'price'     => $price,
                'weight'    => 0,
                'qty'       => $quantity,
                'options'   => [
                    'attribute' => $arrayAtt,
                    'image'     => $parentProduct->image
                ]));
            return Cart::content();
        }

        $quantity = $request->input('quantity');
        $parentProduct = Product::findProductById($subid);
        $price = $parentProduct->price;
        if($parentProduct->discount > 0){
            $price = $parentProduct->discount;
        }
        Cart::add(array('id' => $subid,'name' => $parentProduct->name,'price' => $price,'weight' => 0,
            'qty' => $quantity, 'options' => ['image'=>$parentProduct->image]));
        return Cart::content();

    }
    public function updateCart(Request $request){
        Cart::update($request->product_id, ['qty' => $request->quantity]);
        $string = '';
       foreach (Cart::content() as $item){
           $string .= $this->renderHtmlCart($item);
       }
       return $string;
    }

    public function removeItemCart(Request $request){
        Cart::remove($request->product_id);
        $string = '';
        foreach (Cart::content() as $item){
            $string .= $this->renderHtmlCart($item);
        }
        return $string;
    }

    public function getTotalPrice(Request $request){
        $total = 0;
        foreach (Cart::content() as $item){
            $total += $item->price * $item->qty;
        }
        return number_format($total,'0','.',',');
    }
    public function getAttProduct($item){
        $attribute = '';
        if(count((array)$item->options->attribute) > 0) {
            foreach ($item->options->attribute as $key => $value) {
                $attribute .= '<small><a href="#">' . $key . ' :' . $value . '</a></small>';
            }
            return $attribute;
        }
    }
    private function renderHtmlCart($item){
        return '  <tr>
                        <td class="cart-product">
                            <a href="#"><img alt="Blouse" src="'.asset('frontend/img/product/'.$item->options->image).'"></a>
                        </td>
                        <td class="cart-description">
                            <p class="product-name"><a href="#">'.$item->name.'</a></p>
                              '.$this->getAttProduct($item).'
                        </td>
                        <td class="cart-avail"><span class="label label-success">In stock</span></td>
                        <td class="cart-unit">
                            <ul class="price text-right">
                                <li class="price">'.number_format($item->price,'0','.',',').' vnd</li>
                            </ul>
                        </td>
                        <td class="cart_quantity text-center">
                            <div class="cart-plus-minus-button">
                                <input class="cart-plus-minus" type="text" name="qtybutton" value="'.$item->qty.'">
								<input type="text" name="value" value="1" hidden>
								<div class="dec qtybutton">-</div>
								<div class="inc qtybutton">+</div>
                        </td>
						<input type="hidden"  class="rowId_hidden"  value="'.$item->rowId.'" >
                        <td class="cart-delete text-center">
											<span>
                                                <button type="button" class="cart_quantity_delete" title="Delete" data-id="'.$item->rowId.'" onclick="return removeItem(this);"> <i class="fa fa-trash-o"></i></button>
                                                <button type="button" class="cart_quantity_delete" title="Update" data-id="'.$item->rowId.'" onclick="return updateItem(this);"> <i class="fa fa-refresh"></i></button>
											</span>

                        </td>
                        <td class="cart-total">
                            <span class="price">'.number_format($item->price * $item->qty,'0','.',',').' vnd</span>
                        </td>
                    </tr>';
    }
    public function checkoutSignin(Request $request){
        if(Auth::check()){
            return redirect(route('checkout.address'));
            die;
        }else{
        return view('site.cart.check-out-signin');
        die;
        }
    }
    public function checkoutAddress(Request $request){
        $address = Address::where('user_id',Auth::id())->get();
        return view('site.cart.check-out-address',compact('address'));
    }
    public function checkoutShipping(Request $request){
        $addressDeli = $request->input('addDeli');
        $addressBill = $request->input('addBill');
        return view('site.cart.check-out-shipping',compact('addressBill','addressDeli'));
    }
    public function checkout(Request $request){
        $addressDeli = Address::where('id', $request->input('addDeli'))->first();
        $addressBill = Address::where('id', $request->input('addBill'))->first();
        $methodShip = $request->input('methodShipping');
        return view('site.cart.checkout',compact('addressBill','addressDeli','methodShip'));
    }

}
