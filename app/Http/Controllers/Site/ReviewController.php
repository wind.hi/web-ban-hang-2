<?php

namespace App\Http\Controllers\Site;

use App\Entity\Comment;
use App\Entity\Rate;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AddCommentRequest;

class ReviewController extends Controller
{
  public function insertRate(Request $request){
        Rate::insert([
            "user_id"       => Auth::id(),
            "product_id"    => $request->product_id,
            "point_star"    => $request->star_index,
            "created_at"    => new \DateTime(),
            "updated_at"    => new \DateTime(),
        ]);

  }
  public function showComments(Request $request){
      $comments = Comment::join('users','comments.user_id','users.id')
             ->select('comments.user_id','comments.id as commentID','comments.content','comments.product_id','users.id','users.name','comments.created_at as time')
             ->where('comments.product_id',$request->productID)
             ->orderBy('comments.id','DESC')
             ->paginate(7);
      return view('site.review.comments',[
          "comments" => $comments,
      ]);
  }
  public function insertComment(AddCommentRequest $request,$productID){
    Comment::insert([
        "user_id"       => Auth::id(),
        "product_id"    => $productID,
        "content"       => $request->input('Textarea1'),
        "created_at"    => new \DateTime(),
        "updated_at"    => new \DateTime(),
    ]);
    return redirect()->back();
  }
  public function updateComment(Request $request,$id){
      $updateComment = Comment::where('id',$id);
      $updateComment->update([
          "content"  => $request->input('textarea2'),
      ]);
      return redirect()->back();
  }
  public function deleteComment($id){
    $del = Comment::find($id);
    $del->delete();
    return redirect()->back();
  }
}
