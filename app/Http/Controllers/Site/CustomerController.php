<?php

namespace App\Http\Controllers\Site;

use App\Entity\Vendor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\User;
use App\Http\Requests\{CustomerRequest,editPersonalInformationRequest};
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Session;
use App\Mail\AddvendorEmail;
use App\Mail\AddvendorReply;
use Illuminate\Support\Facades\Mail;

class CustomerController extends Controller
{
    public function getAddCustomers(){
        return view('site.Customer.Registration');
    }
    public function postAddCustomers(Request $request){

        $user = User::create([
            'name'=> $request->name,
            'email'     => $request->email,
            'password'  => bcrypt($request['password']),
            'address'  =>$request->address,
            'phone'  =>$request->phone,

        ]);
        $user->assignRole('Customer');
        return redirect()->back()->with('status','Tạo tài khoản thành công');

    }
    // Phần My_acconut
    public function indexMyAccount(){
        return view('site.My_Account.my_account');
    }
    public function getpersonal_information($id){
        $edit_customer_user = User::Where('id',$id)->first();
        return view ('site.My_Account. personal_information',compact('edit_customer_user'));
    }
    public function postpersonal_information(editPersonalInformationRequest $request,$id){
        $update = User::find($id);
        $update->name = $request->name;
        $update->address = $request->address;
        $update->phone = $request->phone;
        $update->phone = bcrypt($request['password']);
        $update->save();
        return redirect()->back()->with('status', 'Cập nhật thông tin thành công!');
    }
    public function becomeToVendor(Request $request){
        return view('site.Customer.become_vendor');
    }
    public function addVendor(Request $request){
        Vendor::insert([
            "user_id"=>Auth::id(),
            "name_representative"=>$request->input('name_representative'),
            "name_store"=>$request->input('name_store'),
            "phone"=>$request->input('phone'),
            "email"=>$request->input('email'),
            "address"=>$request->input('address'),
            "created_at"=>new \DateTime(),
            "updated_at"=>new \DateTime(),
            "status"=>0, //chua xac nhan
        ]);
        
          Mail::to($request->input('email'))->send(new AddvendorReply);
          Mail::to(env('MAIL_USERNAME'))->send(new AddvendorEmail);
//        return redirect()->route('My.Account')->with('success','Cảm ơn bạn');
    }

}
