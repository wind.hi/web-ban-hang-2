<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\{LoginSiteRequest};
use Auth;
use Session;
use Socialite;
use App\Entity\User;
class SiteLoginController extends Controller
{
    public function getLoginSite(){
        return view('site.login.login');
    }
    public function postLoginSite(LoginSiteRequest $req){
        if($_POST['action'] == 1){
            $credentials = array('email'=>$req->email,'password'=>$req->password);
            if(Auth::attempt($credentials)){
                return redirect()->route('checkout.address');
            }
            else{
                return redirect()->back()->with('status','Đăng nhập không thành công');
            }
        }
        $credentials = array('email'=>$req->email,'password'=>$req->password);
        if(Auth::attempt($credentials)){
            return redirect()->route('index');
        }
        else{
            return redirect()->back()->with('status','Đăng nhập không thành công');
        }
    }
    public function logoutSite(){
        Auth::logout();
        return redirect()->route('index');
    }
    public function logingoogle(){
        return view('site.login.logingoogle');
    }
    /**
     * Redirect the user to the Google authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

//    Đăng nhập bằng google
//    /**
//     * Obtain the user information from Google.
//     *
//     * @return \Illuminate\Http\Response
//     */
    public function handleProviderCallback(Request $request)
    {
        try {
            $user = Socialite::driver('google')->user();
        } catch (\Exception $e) {
            return redirect('/login');
        }
        // only allow people with @company.com to login
        if(explode("@", $user->email)[1] !== 'company.com'){
            return redirect()->to('/');
        }
        // check if they're an existing user
        $existingUser = User::where('email', $user->email)->first();
        if($existingUser){
            // log them in
            auth()->login($existingUser, true);
        } else {
            // create a new user
            $newUser                  = new User;
            $newUser->name            = $user->name;
            $newUser->email           = $user->email;
            $newUser->google_id       = $user->id;
            $newUser->avatar          = $user->avatar;
            $newUser->avatar_original = $user->avatar_original;
            $newUser->save();
            auth()->login($newUser, true);
        }
        return redirect()->to('/home');
    }
    //facebook
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
    public function callback($provider)
    {
        $getInfo = Socialite::driver($provider)->user();
        $user = $this->createUser($getInfo,$provider);
        auth()->login($user);
        return redirect()->to('/');
        //return redirect()->route('index');
    }
    function createUser($getInfo,$provider){
        $user = User::where('provider_id', $getInfo->id)->first();
        if (!$user) {
            $user = User::create([
                'name'     => $getInfo->name,
                'email'    => $getInfo->email,
                'provider' => $provider,
                'provider_id' => $getInfo->id
            ]);
            $user->assignRole('Admin');
        }
        return $user;
    }
}
