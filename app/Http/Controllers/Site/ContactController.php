<?php

namespace App\Http\Controllers\Site;

use App\Entity\Contact;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\{ContactRequest};
use Session;
class ContactController extends Controller
{
    public function  getlistContact(){
        return view('site.contact.contact');
    }
    public function postlistContact(ContactRequest $request){
        $contacts = new Contact();
        $contacts->user_id = Auth::id() ? Auth::id() : '';
        $contacts->title = $request->title;
        $contacts->email = $request->email;
        $contacts->phone = $request->phone;
        $contacts->message = $request->message;

        $contacts->save();
        Session::put('status', 'Chúng tôi đã nhập được và sẽ trả lời trong thời gian sớm nhất');
        return redirect()->back();
    }
    public function index()
    {
        return view('site.contact.index');
    }
}
