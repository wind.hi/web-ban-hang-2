<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Address;
use App\Entity\User;
use Illuminate\Support\Facades\Auth;

// TODO: Validate data

class AddressController extends Controller{
	public function list () {
    	$addresses = Address::where('user_id', Auth::id())->get();
		return view('site.address.list', [
			'addresses' => $addresses
		]);
	}

    public function formAdd () {
		return view('site.address.add');
    }

    public function submitAdd (Request $request) {
      	$address = new Address;

      	$address -> user_id      	= Auth::id();
      	$address -> address_name	= $request -> address_name;
      	$address -> name			= $request -> name;
		$address -> address			= $request -> address;
		$address -> phone			= $request -> phone;

		$address->save();

		// TODO: Show thông báo thêm thành công
		return redirect(route('user.address'));
    }

	public function remove ($id) {
		$address = Address::where('id', $id)->firstOrFail();

		if ($address->user_id != Auth::id()) {
			//// TODO: Thông báo không đúng tài khoản hoặc chưa đăng nhập
			return back();
			die;
		} else {
			$address -> delete();
		}
		// TODO: Show thông báo xóa thành công
		return back();
	}
	public function formUpdate ($id) {

		$address = Address::where('id', $id)->firstOrFail();

		if ($address->user_id != Auth::id()) {
			//// TODO: Thông báo không đúng tài khoản hoặc chưa đăng nhập
			return back();
			die;
		} else {
			return view('site.address.edit',[
				'address' => $address
			]);
		}
	}
	public function submitUpdate ($id, Request $request) {

		$address = Address::where('id', $id)->firstOrFail();

		if ($address->user_id != Auth::id()) {
			//// TODO: Thông báo không đúng tài khoản hoặc chưa đăng nhập
			return back();
			die;
		} else {
      		$address -> address_name	= $request -> address_name;
      		$address -> name			= $request -> name;
			$address -> address			= $request -> address;
			$address -> phone			= $request -> phone;

			$address->save();
		}
		return redirect(route('user.address'));
	}
    public function getAddress(Request $request){
        $string = '';
        $address = Address::where('id',$request->input('address_id'))->first();
        return $string .= '
			<li><span class="info-address address_name">'.$address->name.'</span></li>
            <li><span class="info-address address_address1">'.$address->address_name.'</span></li>
            <li><span class="info-address address_address2">'.$address->address.'</span></li>
            <li><span class="info-address address_phone">'.$address->phone.'</span></li>
            <li class="info-address update-button">
            	<a href="' .route('user.address.update',$address->id). '">Update<i class="fa fa-chevron-right"></i></a>
            </li> ';
    }
}
