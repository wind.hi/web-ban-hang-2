<?php

namespace App\Http\Controllers\Site;

use App\Entity\Attribute;
use App\Entity\Product;
use App\Entity\Comment;
use App\Entity\Image;
use App\Entity\Category;
use App\Entity\Ads;

use App\Entity\Rate;
use App\Entity\SubProduct;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    //

    public function detailProduct(Request $request){


        $infoProduct = Product::where('id',$request->id)->firstOrFail();

        //Get Category and Images
        $category = Category::where('id', $infoProduct->category_id)->first();
        $images = Image::where('product_id', $request->id)->get();

        //Get New Status
        $now = Carbon::now();
        $date = $infoProduct->created_at;
        $diffDate = $now->diffInDays($date);

        if($diffDate < 3){
            $status = 'new';
        }else{
            $status = null;
        }

        if($infoProduct->type == 0){
            $product = Product::where('id',$request->id)->firstOrFail();
            $options = null;
        }else{
            $product = SubProduct::where('product_id', $request->id)->where('quantity','>' ,0)->first();
            $options = SubProduct::where('product_id', $request->id)->where('quantity','>' ,0)->get();
        }

        // Hiển thị rating với user_id và 1 product_id tương ứng.
        $rates = Rate::where('product_id',$product->id)->where('user_id',Auth::id());
        // Tính rating star trung bình user_id và 1 product_id tương ứng
        $sum_rate = round((Rate::where('product_id',$product->id)->avg('point_star')));
        // sản phẩm liên quan lấy theo category id
        $rale_products = Product::where('category_id',$product->category_id)->get();
        //  tăng view khi click vào chi tiết sản phẩm
//        Product::where('id',$infoProduct->id)->update([
//                "view"=>$infoProduct->view+1
//            ]);

        //image
        $productImages = Image::where('status','1')->get();
        //đếm số lượt review
        $review = count(Comment::where('product_id',$product->id)->get('content'));

        $reviews = Comment::get();

        $rating = Rate::get();

        $ads = Ads::where('status','1')->limit(2)->get();
//        Share link facebook
        $url_canonical =$request->url();
        
        return view('site.product.detail',[
            'info'		        => $infoProduct,
            'product'	        => $product,
            'options' 	        => $options,
            'images'	        => $images,
            'category'	        => $category,
            'status'	        => $status,
            'rates'             => $rates,
            'sum_rate'          => $sum_rate,
            'rale_products'     => $rale_products,
            'productImages'     => $productImages,
            'review'            => $review,
            'rating'            => $rating,
            'reviews'           => $reviews,
            'url_canonical'     => $url_canonical,
            'ads'               => $ads
        ]);
    }
	public function showOptionAttribute($id, $option_id){
		$attributes = SubProduct::where('product_id', $id)->where('id', $option_id)->first();
		return json_encode($attributes);
	}


}
