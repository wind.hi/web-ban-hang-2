<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'quantity' => 'required|min:1|numeric',
            'price' => 'required',
            'short_description' => 'required',
            'category_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Trường này không được để trống!',
            'quantity.required' => 'Trường này không được để trống!',
            'price.required' => 'Trường này không được để trống!',
            'category_id.required' => 'Trường này không được để trống!',
            'short_description.required' => 'Trường này không được để trống!',
        ];
    }
}
