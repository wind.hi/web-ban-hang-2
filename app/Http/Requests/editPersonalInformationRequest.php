<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class editPersonalInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'           => 'required|string|max:255',
            'password'       => 'required|string|min:1|confirmed',
            'address'        => 'required|string|max:255',
            'phone'          => 'required|min:9|max:11',
        ];
    }
    public function messages()
    {
        return [
            'name.required'         => 'Họ và tên là trường bắt buộc',
            'name.max'              => 'Họ và tên không quá 255 ký tự',
            'password.min'          => 'Mật khẩu phải chứa ít nhất 1 ký tự',
            'password.confirmed'    => 'Xác nhận mật khẩu không đúng',
            'password.required'     => 'Password là trường bắt buộc',
            'address.required'      => 'Địa Chỉ là trường bắt buộc phải nhập',
            'address.max'           => 'Địa Chỉ không dài quá 255 kí tự',
            'phone.required'        => 'Số điện thoại không được để trống',
            'phone.min'             => 'Số điện thoại không chính xác',
            'phone.max'             => 'Số điện thoại không chính xác'
        ];
    }
}
