<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InfoRequest extends FormRequest{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
			'phone'		=> 'required',
			'email'		=> 'required',
			'address'	=> 'required',
			'about'		=> 'required',
            'logo'             => 'required'
            //
        ];
    }

	public function messages(){
		return [
			'phone.required'	=> 'Yêu cầu nhập số điện thoại',
			'email.required'	=> 'Yêu cầu nhập email',
			'address.required'	=> 'Yêu cầu nhập địa chỉ',
			'about.required'	=> 'Yêu cầu nhập thông tin cơ bản',
            'logo.required'         => 'Logo website là trường bắt buộc',

        ];
	}
}
