<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InfoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'phone' => 'required|regex:/(0)[0-9]{9}/',
            'email'		=> 'required|email',
            'address'	=> 'required',
            'about'		=> 'required',
        ];
    }

    public function messages(){
        return [
            'phone.required'	=> 'Yêu cầu nhập số điện thoại',
            'phone.regex'	=> 'Trường này phải là số điện thoại',
            'email.required'	=> 'Yêu cầu nhập email',
            'address.required'	=> 'Yêu cầu nhập địa chỉ',
            'about.required'	=> 'Yêu cầu nhập thông tin cơ bản',
        ];
    }
}
