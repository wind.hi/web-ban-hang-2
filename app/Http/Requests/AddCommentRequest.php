<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "Textarea1" => "required|string",
        ];
    }
//    public function messages()
//    {
//        return [
//            'Textarea1.required'    => 'Họ và tên là trường bắt buộc',
//        ];
//    }
}
