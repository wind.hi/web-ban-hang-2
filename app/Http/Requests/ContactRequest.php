<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|',
            'email' => 'required|email',
            'phone' => 'required|min:9|max:11',
            'message' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required'    =>  'Tiêu đề này không được để trống !',
            'email.required'    =>  'Email này không được để trống !',
            'email.email'       =>  'Email không đúng định dạng !',
            'phone.required'    =>  'Số điện thoại không được để trống !',
            'phone.min'         =>  'Số điện thoại không đúng!',
            'phone.max'         =>  'Số điện thoại không đúng!',
            'message.required'  =>  'Trường này không được để trống !',
        ];
    }
}
