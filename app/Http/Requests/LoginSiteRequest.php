<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginSiteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'     =>'required|string|email',
            'password'  =>'required|max:20'

        ];
    }
    public function messages()
    {
        return [
            'email.required'        => 'Bạn chưa nhập email',
            'email.email'           =>'Email không đúng định dạng',
            'password.required'     => 'Bạn chưa nhập password',
            'pasword.max'           => 'Password không được quá 20 kí tự'
        ];
    }
}
