<?php
namespace App\HelpersClass;



class date
{
    public static function getlistDayInMonth()
    {
        $arrayDay = [];
        $month    = date('m');
        $year     = date('Y');
        //Lấy tất cả các ngày trong tháng;
        for($day = 1;$day<=31;$day++)
        {
            $time = mktime(12,0,0,$month,$day,$year);
            if(date('m',$time) == $month)
                $arrayDay[] = date('d-m-Y',$time);
        }
        return $arrayDay;
    }
}
