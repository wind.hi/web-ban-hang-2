<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();
        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes(){
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes(){
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

	////Admin Routes
	//Login
	protected function mapAdminLoginRoutes(){
		Route::prefix('admin')
			->namespace($this->namespace .'\Admin')
			->group(base_path('routes/admin/login.php'));
	}
	//Admin
	protected function mapAdminRoutes(){
		Route::prefix('admin')
			->middleware('admin')
			->namespace($this->namespace .'\Admin')
			->group(base_path('routes/admin/admin.php'));
	}

	//Attribute
	protected function mapAttributeRoutes(){
		Route::prefix('admin/attribute')
			->middleware('admin')
			->namespace($this->namespace .'\Admin')
			->group(base_path('routes/admin/attribute.php'));
	}

	//Category
	protected function mapCategoryRoutes(){
		Route::prefix('admin/category')
			->middleware('admin')
			->namespace($this->namespace .'\Admin')
			->group(base_path('routes/admin/category.php'));
	}

	//Customer
	protected function mapCustomerRoutes(){
		Route::prefix('admin/customer')
			->middleware('admin')
			->namespace($this->namespace .'\Admin')
			->group(base_path('routes/admin/customer.php'));
	}

	//Info
	protected function mapInfoRoutes(){
		Route::prefix('admin/info')
			->middleware('admin')
			->namespace($this->namespace .'\Admin')
			->group(base_path('routes/admin/info.php'));
	}

	//Product
	protected function mapProductRoutes(){
		Route::prefix('admin/product')
			->middleware('admin')
			->namespace($this->namespace .'\Admin')
			->group(base_path('routes/admin/product.php'));
	}

	//Slider
	protected function mapSliderRoutes(){
		Route::prefix('admin/users')
			->middleware('admin')
			->namespace($this->namespace .'\Admin')
			->group(base_path('routes/admin/slider.php'));
	}

	//User
	protected function mapUserRoutes(){
		Route::prefix('admin/users')
			->middleware('admin')
			->namespace($this->namespace .'\Admin')
			->group(base_path('routes/admin/user.php'));
	}

	//Value
	protected function mapValueRoutes(){
		Route::prefix('admin/value')
			->middleware('admin')
			->namespace($this->namespace .'\Admin')
			->group(base_path('routes/admin/value.php'));
	}




}
