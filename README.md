<p align="center">

#Danh sách công việc cần làm

</p>

> Nhận task bằng cách điền tên cuối task và ở giữa 2 dấu *
> Ví dụ *tên*

> Làm xong thì đánh dấu x vào [ ]

## Admin site
- [ ] Validate Form *Quỳnh*
- [ ] Edit Products + Delete Products  
- [ ] Quản lý Comments
  - [ ] Xóa/Ẩn
- [ ] Phân quyền
  - [ ] Thêm/Xóa Role(Chức)
  - [ ] Thêm/Xóa Permission(Quyền)
  - [ ] Gắn/Gỡ Permission vào Role
- [ ] Quản lý Order
  - [ ] Show List Order
  - [ ] Sort/Filter
- [ ] Tùy chỉnh giao diện
  - [ ] Thay đổi logo, sdt, địa chỉ
- ...


## User site
- [ ] Gửi Bình luận/Đánh giá
- [ ] Giỏ hàng
  - [ ] Thêm/bớt sản phẩm
  - [ ] Xóa hết giỏ hàng
- [x] Địa chỉ User *Đạt*
  - [x] Thêm/Sửa/Xóa
- [ ] Thanh toán
- [ ] Show Sản phẩm theo Danh mục
- [ ] Viết seeds(Dữ liệu cho DB mẫu)
  - [ ] TK Admin
  - [ ] TK User
  - ...
