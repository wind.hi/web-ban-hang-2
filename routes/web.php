<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Route;

// route admin login logout
Route::group(['prefix'=>'admin','namespace'=>'Admin'],function (){
    Route::get('/login', 'LoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'LoginController@login')->name('admin.login');
    Route::get('/logout', 'LoginController@logOut')->name('admin.logout');

    //shjhghhjhj
});
// route admin quản lý user
Route::group(['prefix'=>'admin/user','namespace'=>'Admin', 'middleware'=>['admin']],function (){
    // Show List Admin
    Route::get('/list', 'UserController@showList')->name('list.user');
    Route::get('/get-list','UserController@getAll')->name('get.data.user');

    //Add
    Route::get('/add', 'UserController@getAddUser')->name('add.user');
    Route::post('/add', 'UserController@postAddUser');

    //Delete
    Route::get('/delete/{id}', 'UserController@getDeleteUser')->name('delete.user');

    //Edit
    Route::get('/edit/', 'UserController@getEditUser')->name('edit.user');

    Route::post('/edit-user/{id}', 'UserController@postEditUser')->name('post.edit.user');

});
// route admin
Route::group(['prefix'=>'admin', 'namespace'=>'Admin', 'middleware'=>['admin']],function(){
    Route::get('/','AdminController@home')->name('admin.home');
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::get('show-role', 'RoleController@showRole')->name('show.role');

    //Category
    Route::group(['prefix'=>'category'],function(){

        //Add
        Route::get('/', 'CategoryController@formAdd')->name('category.add');
        Route::post('/', 'CategoryController@submitAdd')->name('add.category');

        //Delete
        Route::get('/delete/{id}', 'CategoryController@delete')->name('category.delete');

        //Edit
        Route::get('/edit/{id}', 'CategoryController@formEdit')->name('category.edit');
        Route::post('/edit/{id}', 'CategoryController@submitEdit');

    });

    //Attribute
    Route::group(['prefix'=>'attribute'],function(){
        //Show
        Route::get('/', 'AttributeController@getAll')->name('attribute.list');

        //Add
        Route::get('/add', 'AttributeController@formAdd')->name('attribute.add');
        Route::post('/add', 'AttributeController@submitAdd');

        //Delete
        Route::get('/delete/{id}', 'AttributeController@delete')->name('attribute.delete');

        //Edit
        Route::get('/edit/{id}', 'AttributeController@formEdit')->name('attribute.edit');
        Route::post('/edit/{id}', 'AttributeController@submitEdit');

    });

    //Value
    Route::group(['prefix'=>'value'],function(){
        Route::get('/delete/{id}', 'ValueController@delete')->name('value.delete');
    });

    //Product
    Route::resource('product', 'ProductController');
    // get attribute value product
    Route::get('get-att-value', 'ProductController@getAttValue')->name('get.att');
    //get child category
    Route::get('get-cate-child', 'ProductController@getChildCategory')->name('get.child.cate');
    //datatable product
    Route::get('dt-product', 'ProductController@anyDatatable')->name('dt.product');
    Route::get('list-product', 'ProductController@listProduct')->name('list.product');
    //vendor product
    Route::get('dt-vendor-product', 'ProductController@anyDataTableProductVendor')->name('dt.vendor.product');
    Route::get('list-vendor-product', 'ProductController@getListProductVendor')->name('list.vendor.product');
    Route::get('vendor/product/{productId}', 'ProductController@editProductVendor')->name('edit.vendor.product');
    Route::get('confirm-product-vendor', 'ProductController@confirmProductVendor')->name('confirm.product.vendor');
    Route::get('show-admin-confirm-product', 'ProductController@getAdminConfirmProduct')->name('admin.confirm.product.vendor');

    Route::post('deny-confirm-product', 'ProductController@denyConfirmProduct')->name('deny.confirm.product');
    Route::get('revert-status-product', 'ProductController@revertStatusProduct')->name('revert.status.product');

    // get detail cate
    Route::get('detail-cate', 'CategoryController@detailCategory')->name('detail.cate');
    Route::post('update-cate', 'CategoryController@updateCategory')->name('category.update');
    //ajax line cate
    Route::get('change-line-cate/{cateId}', 'CategoryController@changeLineCate')->name('line.cate');

    Route::get('get-att-product', 'ProductController@getAttributeValueProduct')->name('get.att.product');
    Route::get('update-att-product', 'ProductController@updateAttProduct')->name('update.att.product');

    Route::get('edit-image-product/{productId}', 'ProductController@editImageProduct')->name('edit.image.product');
    Route::post('edit-image-productPost', 'ProductController@posteditImageProduct')->name('POSTedit.image.product');
//    ajax show image product
    Route::get('get-detail-image', 'ProductController@getDetailImageProduct')->name('get.detail.image');
    Route::post('update-image-product', 'ProductController@updateImageProduct')->name('update.image.product');
    Route::get('update-image-product/{imageId}', 'ProductController@deleteImageProduct')->name('delete.image.product');

    //test tab
    Route::get('add-configurable-product','ProductController@tabAddProduct')->name('product.add.exProduct');
    Route::post('add-product','ProductController@addProductTab')->name('product.tab.add');
    Route::post('add-subproduct','ProductController@addSubProductTab')->name('product.tab.addSubProduct');
    // ajax addSubProductToTable
    Route::get('add-subproduct-table','ProductController@addSubProductToTable')->name('product.ajax.addSubProduct');
    Route::get('clear-subproduct-table','ProductController@clearSubProductFromTable')->name('product.ajax.clearSubProduct');
    Route::get('add-simple-product','ProductController@tabAddSimpleProduct')->name('product.add.SimpleProduct');
    Route::post('add-simple-product','ProductController@addSimpleProductTab')->name('product.tab.add.SimpleProduct');
    //end tab
    //test image upload
    Route::get('add-image-product',function (){
        return view('admin.product.tab.add.image');
    });
    Route::post('add-images-product/','ProductController@addThumbnailProduct');
    //end image upload
    // update sub product
    Route::post('update-sub-product/','ProductController@updateSubProduct')->name('update.subproduct');
    // delete sub product
    Route::get('delete-sub-product/{productId}','ProductController@deleteSubProduct')->name('delete.subproduct');
    // add new product Option
    //Route::get('add-product-option','ProductController@addProductOption')->name('add.product.option');
    //Slider
    Route::group(['prefix'=>'slider'], function(){
        Route::get('/', 'SliderController@getAll')->name('slider.list');
        Route::post('/', 'SliderController@submitAdd')->name('slider.add');
        Route::get('delete/{id}', 'SliderController@delete')->name('slider.delete');
    });

    //Info Contact
    Route::group(['prefix'=>'info'], function(){
        Route::get('/', 'InfoController@show')->name('info.show');
        Route::post('/edit', 'InfoController@edit')->name('info.edit');
        Route::post('/store', 'InfoController@store')->name('info.store');
    });
	//Order
	Route::group(['prefix' => 'order'], function(){
		Route::get('update/{id}', 'OrderController@updateStatus')->name('order.update.status');
		Route::get('updateRevert/{id}', 'OrderController@revertUpdate')->name('order.revert.status');
		route::get('deleteOrder/{id}','OrderController@deleteOrder')->name('order.delete');
		// Danh sách order Slider
        Route::get('list_order','OrderController@listorder')->name('order.list');
        Route::get('list_order_finish','OrderController@listorderfinish')->name('Order.list2');
        Route::get('detail-order/{id}', 'OrderController@detailOrder')->name('detail.Order');
	});
    Route::group(['prefix' => 'ads'], function(){
        Route::get('add', 'AdsController@addAds')->name('ads.add');
        Route::post('dt-add','AdsController@dtAddAds')->name('dt.ads.add');

        Route::get('edit/{id}', 'AdsController@editAds')->name('ads.edit');
        Route::post('update/{id}', 'AdsController@updateAds')->name('ads.update');

        Route::get('delete/{id}', 'AdsController@deleteAds')->name('ads.delete');

        Route::get('dt-list', 'AdsController@dtListAds')->name('dt.ads.list');
        Route::get('list', 'AdsController@listAds')->name('ads.list');
        //show
    });

    // vendor
    Route::group(['prefix' => 'vendor'], function(){
        Route::get('list-vendor', 'CustomerController@showListVendor')->name('list.vendor');
        Route::get('dt-vendor', 'CustomerController@dataTableVendor')->name('dt.vendor');
        Route::get('detail-vendor/{vendorId}', 'CustomerController@showDetailVendor')->name('detail.vendor');
        Route::get('confirm-vendor/', 'CustomerController@confirmVendor')->name('confirm.vendor');
        Route::get('delete-vendor/{id}', 'CustomerController@deleteVendor')->name('delete.vendor');
    });
    //Contact
    Route::get('contact','ContactController@index')->name('contact.list');
    Route::get('contact/{action}/{id}','ContactController@action')->name('contact.action');
    Route::post('contact/reply-contact/{id}','ContactController@replyContact');
    Route::get('contact/send-mail/{email}/{title}/{content}','ContactController@sendMailReplyContact')->name('send.mail.contact');


    // Customer
    Route::get('list.customer','CustomerController@listcustomer')->name('customer.list');
    Route::get('lock.list.customer','CustomerController@locklickcustomer')->name('customer.list.lock');
    Route::get('add.customer','CustomerController@getaddcustomer')->name('customer.add');
    Route::post('add.customer','CustomerController@postaddcustomer')->name('customer.add');
    // Edit Lock Customer
    Route::get('lock.customer/{id}','CustomerController@getEditCustomer')->name('lock.customer');
});



//route trang Site
Route::group(['namespace'=>'Site' ],function(){
    Route::get('/', 'HomeController@home')->name('index');

    //Đăng ký tài khoản khách hàng
    Route::get('customer-add-new','CustomerController@getAddCustomers')->name('customer.add.new');
    Route::post('customer-add-new','CustomerController@postAddCustomers')->name('customer.add.new');
    // My_account
    route::get('my_account','CustomerController@indexMyAccount')->name('My.Account');
    route::get('personal_information/{id}','CustomerController@getpersonal_information')->name( 'personal.information');
    route::post('personal_information/{id}','CustomerController@postpersonal_information')->name( 'personal.information');
    // Route phần tìm kiếm
    Route::get('page-search','HomeController@getSearch');
    Route::get('search','HomeController@postSearch')->name('search');

    // check total
    Route::get('check-total','ProductController@checkTotal')->name('check.total');
});
// Customer
Route::group(['prefix'=>'admin', 'namespace'=>'Admin', 'middleware'=>['admin']],function(){
    Route::get('list.customer','CustomerController@listcustomer')->name('customer.list');
    Route::get('lock.list.customer','CustomerController@locklickcustomer')->name('customer.list.lock');
    Route::get('add.customer','CustomerController@getaddcustomer')->name('customer.add');
    Route::post('add.customer','CustomerController@postaddcustomer')->name('customer.add');
    // Edit Lock Customer
    Route::get('lock.customer/{id}','CustomerController@getEditCustomer')->name('lock.customer');
});


    // Đăng nhập Site
//    //login
//    Route::get('login','SiteLoginController@getLoginSite')->name('Login.Site');
//    Route::post('login','SiteLoginController@postLoginSite')->name('Login.Site');
    //Log out
//Route::get('logoutSite', 'SiteLoginController@logoutSite')->name('logoutSite');

// Đăng nhập Site
Route::group(['namespace'=>'Site' ],function(){
    route::get('loginGoogle','SiteLoginController@logingoogle')->name('login.site.google');
	//login
    Route::get('login','SiteLoginController@getLoginSite')->name('Login.Site');
	Route::post('login','SiteLoginController@postLoginSite')->name('Login.Site');
	//Log out
	Route::get('logoutSite', 'SiteLoginController@logoutSite')->name('logoutSite');
    // Login Google
    Route::get('/redirect', 'SiteLoginController@redirectToProvider')->name('login.google');
    Route::get('/callback', 'SiteLoginController@handleProviderCallback');
    //login facebook
    Route::get('/login_facebook/{provider}', 'SiteLoginController@redirect')->name('login.facebook');
    Route::get('/callback/{provider}', 'SiteLoginController@callback');
	// cart
//    Route::get('add-to-cart', 'CartController@addtoCart');
    Route::post('add-to-cart', 'CartController@addtoCart')->name('add_to_cart');
    Route::get('update-cart', 'CartController@updateCart')->name('update_item_cart');
    Route::get('remove-item-cart', 'CartController@removeItemCart')->name('remove_item_cart');
    Route::get('load-total-price', 'CartController@getTotalPrice')->name('reload_total_price');
    Route::get('my-cart', 'CartController@showCart')->name('my.cart');

    Route::get('checkout-signin', 'CartController@checkoutSignin')->name('checkout.signin');
    Route::get('checkout-address', 'CartController@checkoutAddress')->name('checkout.address');
    Route::get('checkout-shipping', 'CartController@checkoutShipping')->name('checkout.shipping');
    Route::get('checkout', 'CartController@checkout')->name('checkout');
    // order
    Route::post('create-order', 'OrderController@createOrder')->name('create.order');
    Route::get('my-order', 'OrderController@getOrders')->name('get.order');
    Route::get('get-order-item', 'OrderController@getOrderItems')->name('get.order.item');
    Route::get('get-total-price-order', 'OrderController@getTotalPrice')->name('get.order.total');
    Route::get('cancel-order', 'OrderController@cancelOrder')->name('cancel.order');
    Route::get('rollback-order', 'OrderController@rollbacklOrder')->name('rollback.order');
// end cart


    //Wish List
    Route::get('wish-list', 'WishlistController@list')->name('wishlist.list');
    Route::get('wishList/{id}', 'WishlistController@toggle')->name('wishlist.toggle');
    Route::post('delete-wish-list/{id}','WishlistController@deleteWishlist')->name('delete.wishlist');

    //Detail
    Route::get('detail/{id}','ProductController@detailProduct')->name('product.detail');
    Route::get('detail/{id}/{option_id}','ProductController@showOptionAttribute')->name('product.option');
    Route::get('testchitiet',function (){
        return view('site.product.chitiet');
    });

    //Address
    Route::get('address', 'AddressController@list')->name('user.address');
    Route::get('address/add', 'AddressController@formAdd')->name('user.address.add');

    Route::post('address/add', 'AddressController@submitAdd');
    Route::get('address/delete/{id}', 'AddressController@remove')->name('user.address.delete');

    Route::get('address/update/{id}', 'AddressController@formUpdate')->name('user.address.update');
    Route::post('address/update/{id}', 'AddressController@submitUpdate');

    // get address
    Route::get('get-address', 'AddressController@getAddress')->name('get.address');

    // Show Sản phẩm theo loại
    Route::get('product_type/{id}','ProductTypeCotroller@getproductType')->name('product.type');

    // đăng ký thành vendor
    Route::get('register-vendor', 'CustomerController@becomeToVendor')->name('become.vendor');

    Route::post('add-vendor', 'CustomerController@addVendor')->name('add.vendor');
});

// route chi tiết product trang site
Route::group(['prefix'=>'product/detail', 'namespace'=>'Site'],function(){
    Route::get('rating','ReviewController@insertRate')->name('rating');
    Route::get('{productID}/show-comment','ReviewController@showComments')->name('show.comment');
    Route::post('{productID}/add-comment','ReviewController@insertComment')->name('insert.comment');
    Route::get('del-comment/{id}','ReviewController@deleteComment')->name('delete.comment');
    Route::get('update-comment/{id}','ReviewController@updateComment')->name('update.comment');
});

//xác nhận Email
Auth::routes(['verify' => true]);
Route::get('home', 'Site\HomeController@home')->middleware('verified');

//Gửi mail
Route::get('email', 'Email\WelcomeEmailController@sendWelcomeEmail')->middleware('verified');
Route::get('notification', 'Email\EmailRegisterController@EmailRegister');
Route::group(['namespace'=>'Site' ],function(){
    // contact trang site
    route::get('ContactUS','ContactController@getlistContact')->name("site.contact");
    route::post('ContactUS','ContactController@postlistContact')->name("site.contact");
});
