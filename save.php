
		<!-- SINGLE-PRODUCT INFO TAB START -->
		<div class="row">
			<div class="col-sm-12">
				<div class="product-more-info-tab">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs more-info-tab">
						<li class="active"><a href="#moreinfo" data-toggle="tab">more info</a></li>
						<li><a href="#datasheet" data-toggle="tab">data sheet</a></li>
						<li><a href="#review" data-toggle="tab">reviews</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="moreinfo">
							<div class="tab-description">
								<p>Fashion has been creating well-designed collections since 2010. The brand offers feminine designs delivering stylish separates and statement dresses which have since evolved into a full ready-to-wear collection in which every item is a vital part of a woman's wardrobe. The result? Cool, easy, chic looks with youthful elegance and unmistakable signature style. All the beautiful pieces are made in Italy and manufactured with the greatest attention. Now Fashion extends to a range of accessories including shoes, hats, belts and more!</p>
							</div>
						</div>
						<div class="tab-pane" id="datasheet">
							<div class="deta-sheet">
								<table class="table-data-sheet">
									<tbody>
										<tr class="odd">
											<td>Compositions</td>
											<td>Cotton</td>
										</tr>
										<tr class="even">
											<td class="td-bg">Styles</td>
											<td class="td-bg">Casual</td>
										</tr>
										<tr class="odd">
											<td>Properties</td>
											<td>Short Sleeve</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="tab-pane" id="review">
							<div class="row tab-review-row">
								<div class="col-xs-5 col-sm-4 col-md-4 col-lg-3 padding-5">
									<div class="tab-rating-box">
										<span>Grade</span>
										<div class="rating-box">
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star-half-empty"></i>
										</div>
										<div class="review-author-info">
											<strong>write A REVIEW</strong>
											<span>06/22/2015</span>
										</div>
									</div>
								</div>
								<div class="col-xs-7 col-sm-8 col-md-8 col-lg-9 padding-5">
									<div class="write-your-review">
										<p><strong>write A REVIEW</strong></p>
										<p>write A REVIEW</p>
										<span class="usefull-comment">Was this comment useful to you? <span>Yes</span><span>No</span></span>
										<a href="#">Report abuse </a>
									</div>
								</div>
								<a href="#" class="write-review-btn">Write your review!</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- SINGLE-PRODUCT INFO TAB END -->
		<!-- RELATED-PRODUCTS-AREA START -->
		<div class="row">
			<div class="col-sm-12">
				<div class="left-title-area">
					<h2 class="left-title">related products</h2>
				</div>
			</div>
			<div class="related-product-area featured-products-area">
				<div class="col-sm-12">
					<div class=" row">
						<!-- RELATED-CAROUSEL START -->
						<div class="related-product">
							<!-- SINGLE-PRODUCT-ITEM START -->
							<div class="item">
								<div class="single-product-item">
									<div class="product-image">
										<a href="#"><img src="{{ asset('') }}frontend/img/product/sale/3.jpg" alt="product-image" /></a>
									</div>
									<div class="product-info">
										<div class="customar-comments-box">
											<div class="rating-box">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star-half-empty"></i>
											</div>
											<div class="review-box">
												<span>1 Review(s)</span>
											</div>
										</div>
										<a href="#">Faded Short T-sh...</a>
										<div class="price-box">
											<span class="price">$16.51</span>
										</div>
									</div>
								</div>
							</div>
							<!-- SINGLE-PRODUCT-ITEM END -->
							<!-- SINGLE-PRODUCT-ITEM START -->
							<div class="item">
								<div class="single-product-item">
									<div class="product-image">
										<a href="#"><img src="{{ asset('') }}frontend/img/product/sale/1.jpg" alt="product-image" /></a>
									</div>
									<div class="product-info">
										<div class="customar-comments-box">
											<div class="rating-box">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
											</div>
											<div class="review-box">
												<span>1 Review(s)</span>
											</div>
										</div>
										<a href="#">Blouse</a>
										<div class="price-box">
											<span class="price">$22.95</span>
											<span class="old-price">$27.00</span>
										</div>
									</div>
								</div>
							</div>
							<!-- SINGLE-PRODUCT-ITEM END -->
							<!-- SINGLE-PRODUCT-ITEM START -->
							<div class="item">
								<div class="single-product-item">
									<div class="product-image">
										<a href="#"><img src="{{ asset('') }}frontend/img/product/sale/9.jpg" alt="product-image" /></a>
									</div>
									<div class="product-info">
										<div class="customar-comments-box">
											<div class="rating-box">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star-half-empty"></i>
												<i class="fa fa-star-half-empty"></i>
											</div>
											<div class="review-box">
												<span>1 Review(s)</span>
											</div>
										</div>
										<a href="#">Printed Dress</a>
										<div class="price-box">
											<span class="price">$23.40</span>
											<span class="old-price">$26.00</span>
										</div>
									</div>
								</div>
							</div>
							<!-- SINGLE-PRODUCT-ITEM END -->
							<!-- SINGLE-PRODUCT-ITEM START -->
							<div class="item">
								<div class="single-product-item">
									<div class="product-image">
										<a href="#"><img src="{{ asset('') }}frontend/img/product/sale/5.jpg" alt="product-image" /></a>
										<div class="overlay-content">
										</div>
									</div>
									<div class="product-info">
										<div class="customar-comments-box">
											<div class="rating-box">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star-half-empty"></i>
											</div>
											<div class="review-box">
												<span>1 Review(s)</span>
											</div>
										</div>
										<a href="#">Printed Dress</a>
										<div class="price-box">
											<span class="price">$50.99</span>
										</div>
									</div>
								</div>
							</div>
							<!-- SINGLE-PRODUCT-ITEM END -->
							<!-- SINGLE-PRODUCT-ITEM START -->
							<div class="item">
								<div class="single-product-item">
									<div class="product-image">
										<a href="#"><img src="{{ asset('') }}frontend/img/product/sale/12.jpg" alt="product-image" /></a>
									</div>
									<div class="product-info">
										<div class="customar-comments-box">
											<div class="rating-box">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star-half-empty"></i>
												<i class="fa fa-star-half-empty"></i>
											</div>
											<div class="review-box">
												<span>1 Review(s)</span>
											</div>
										</div>
										<a href="#">Printed Summer Dr...</a>
										<div class="price-box">
											<span class="price">$28.98</span>
											<span class="old-price">$30.51</span>
										</div>
									</div>
								</div>
							</div>
							<!-- SINGLE-PRODUCT-ITEM END -->
							<!-- SINGLE-PRODUCT-ITEM START -->
							<div class="item">
								<div class="single-product-item">
									<div class="product-image">
										<a href="#"><img src="{{ asset('') }}frontend/img/product/sale/13.jpg" alt="product-image" /></a>
									</div>
									<div class="product-info">
										<div class="customar-comments-box">
											<div class="rating-box">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
											</div>
											<div class="review-box">
												<span>1 Review(s)</span>
											</div>
										</div>
										<a href="#">Printed Summer</a>
										<div class="price-box">
											<span class="price">$30.50</span>
										</div>
									</div>
								</div>
							</div>
							<!-- SINGLE-PRODUCT-ITEM END -->
							<!-- SINGLE-PRODUCT-ITEM START -->
							<div class="item">
								<div class="single-product-item">
									<div class="product-image">
										<a href="#"><img src="{{ asset('') }}frontend/img/product/sale/7.jpg" alt="product-image" /></a>
									</div>
									<div class="product-info">
										<div class="customar-comments-box">
											<div class="rating-box">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star-half-empty"></i>
												<i class="fa fa-star-half-empty"></i>
											</div>
											<div class="review-box">
												<span>1 Review(s)</span>
											</div>
										</div>
										<a href="#">Printed Chiffon Dr...</a>
										<div class="price-box">
											<span class="price">$16.40</span>
											<span class="old-price">$20.50</span>
										</div>
									</div>
								</div>
							</div>
							<!-- SINGLE-PRODUCT-ITEM END -->
							<!-- SINGLE-PRODUCT-ITEM START -->
							<div class="item">
								<div class="single-product-item">
									<div class="product-image">
										<a href="#"><img src="{{ asset('') }}frontend/img/product/sale/11.jpg" alt="product-image" /></a>
									</div>
									<div class="product-info">
										<div class="customar-comments-box">
											<div class="rating-box">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
											</div>
											<div class="review-box">
												<span>1 Review(s)</span>
											</div>
										</div>
										<a href="#">Printed Dress</a>
										<div class="price-box">
											<span class="price">$26.00</span>
										</div>
									</div>
								</div>
							</div>
							<!-- SINGLE-PRODUCT-ITEM END -->
							<!-- SINGLE-PRODUCT-ITEM START -->
							<div class="item">
								<div class="single-product-item">
									<div class="product-image">
										<a href="#"><img src="{{ asset('') }}frontend/img/product/sale/10.jpg" alt="product-image" /></a>
									</div>
									<div class="product-info">
										<div class="customar-comments-box">
											<div class="rating-box">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star-half-empty"></i>
												<i class="fa fa-star-half-empty"></i>
											</div>
											<div class="review-box">
												<span>1 Review(s)</span>
											</div>
										</div>
										<a href="#">Printed Dress</a>
										<div class="price-box">
											<span class="price">$26.00</span>
										</div>
									</div>
								</div>
							</div>
							<!-- SINGLE-PRODUCT-ITEM END -->
							<!-- SINGLE-PRODUCT-ITEM START -->
							<div class="item">
								<div class="single-product-item">
									<div class="product-image">
										<a href="#"><img src="{{ asset('') }}frontend/img/product/sale/2.jpg" alt="product-image" /></a>
									</div>
									<div class="product-info">
										<div class="customar-comments-box">
											<div class="rating-box">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
											</div>
											<div class="review-box">
												<span>1 Review(s)</span>
											</div>
										</div>
										<a href="#">Blouse</a>
										<div class="price-box">
											<span class="price">$27.00</span>
										</div>
									</div>
								</div>
							</div>
							<!-- SINGLE-PRODUCT-ITEM END -->
							<!-- SINGLE-PRODUCT-ITEM START -->
							<div class="item">
								<div class="single-product-item">
									<div class="product-image">
										<a href="#"><img src="{{ asset('') }}frontend/img/product/sale/4.jpg" alt="product-image" /></a>
									</div>
									<div class="product-info">
										<div class="customar-comments-box">
											<div class="rating-box">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star-half-empty"></i>
											</div>
											<div class="review-box">
												<span>1 Review(s)</span>
											</div>
										</div>
										<a href="#">Faded Short Sle...</a>
										<div class="price-box">
											<span class="price">$16.51</span>
										</div>
									</div>
								</div>
							</div>
							<!-- SINGLE-PRODUCT-ITEM END -->
							<!-- SINGLE-PRODUCT-ITEM START -->
							<div class="item">
								<div class="single-product-item">
									<div class="product-image">
										<a href="#"><img src="{{ asset('') }}frontend/img/product/sale/6.jpg" alt="product-image" /></a>
									</div>
									<div class="product-info">
										<div class="customar-comments-box">
											<div class="rating-box">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
											</div>
											<div class="review-box">
												<span>1 Review(s)</span>
											</div>
										</div>
										<a href="#">Printed Chiffon Dr...</a>
										<div class="price-box">
											<span class="price">$16.40</span>
											<span class="old-price">$20.50</span>
										</div>
									</div>
								</div>
							</div>
							<!-- SINGLE-PRODUCT-ITEM END -->
							<!-- SINGLE-PRODUCT-ITEM START -->
							<div class="item">
								<div class="single-product-item">
									<div class="product-image">
										<a href="#"><img src="{{ asset('') }}frontend/img/product/sale/8.jpg" alt="product-image" /></a>
									</div>
									<div class="product-info">
										<div class="customar-comments-box">
											<div class="rating-box">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star-half-empty"></i>
											</div>
											<div class="review-box">
												<span>1 Review(s)</span>
											</div>
										</div>
										<a href="#">Printed Dress</a>
										<div class="price-box">
											<span class="price">$26.00</span>
										</div>
									</div>
								</div>
							</div>
							<!-- SINGLE-PRODUCT-ITEM END -->
							<!-- SINGLE-PRODUCT-ITEM START -->
						</div>
						<!-- RELATED-CAROUSEL END -->
					</div>
				</div>
			</div>
		</div>
		<!-- RELATED-PRODUCTS-AREA END -->
		